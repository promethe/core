/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
#ifndef _RESEAU
#define _RESEAU
/*---------------------------------------------------------------*/
/*      type d'un reseau general pour le robot simule            */
/*      version sept 1992 : rajout du temps de memorisation      */
/*      precedente version dans dir_system_a                     */
/*---------------------------------------------------------------*/

#include <semaphore.h>
#include <math.h>

#define RESEAU_VERSION  8

#define nil 0


#define modulo 2147483646

#define No_Hebb              0
#define No_Winner            1
#define No_Kohonen           2
#define No_Widrow            3
#define No_Special           4
#define No_Ou                5
#define No_Et                6
#define No_Entree            7
#define No_Sortie            8
#define No_PTM               9
#define No_Hebb_Seuil       10
#define No_Winner_Special   11
#define No_Hopfield         12
#define No_Winner_Colonne   13
#define No_Fonction_Algo    14
#define No_But              15

/*-------------------------*/

#define No_Pyramidal        16  /* can be a good model of granular cell     */
#define No_Inter_neurone    17  /* no STM, usefull for inhibition mechanism */

/*-------------------------*/


#define No_Fonction_Algo_mvt 18

/*-------------------------*/

#define No_Granular         19
#define No_Micro_Pyramid    20

/*-------------------------*/
#define No_SV_cor 21            /* to correlate (difference) 2 forms */
#define No_Dyn 22
/*-------------------------*/

#define No_SAW              23

#define No_Sigma_PI	     24
#define No_Winner_Selectif	25

#define No_Macro_Colonne 26     /* defined to allow self organization beween neurons of a given macro column */

#define No_BREAK	27


#define No_LMS		  28        /* Group added by O.Ledoux */

#define No_RTTOKEN	  29        /* Group added by A.Patard: Groupe d'execution temps reel. */

#define No_KO_Discret     30    /* ajout M. Quoy 1/03/2005 */

#define No_KO_Continu     31    /* ajout M. Quoy 1/03/2005 */
#define No_Hebb_Seuil_binaire       32
#define No_Pyramidal_plan 33


#define No_LMS_2	34          /* ajoute par B. Siri le 07/07/05 */
#define No_BCM		35          /* ajoute par B. Siri le 20/05/05 */

#define No_PLG 36               /*ajoute par M.M. mais le groupe est pas encore vraiment fonctionnel... */
#define No_Mean_Mvt 37
#define No_Asso_Cond 38
#define No_Sub_Network 39       /* pour gerer l'inclusion s'un script */

#define No_LMS_delayed 40

#define No_Winner_Macro 41
#define No_CA3_timing 42
#define No_Sutton_Barto 43

#define No_selective_winner_modulated 44
#define No_PTM_2_old 45 /* ancien 23 No_PTM_2 */

#define nbre_type_groupes 46



/*****************************/

#define No_l_1_1_modif     0
#define No_l_1_a           1
#define No_l_1_v           2
#define No_l_1_1_non_modif 3
#define No_l_1_t           4
#define No_l_algorithmique 5
#define No_l_1_1_non_modif_bloqueur 6
#define No_l_1_v_non_modif 7
#define No_l_neuro_mod     8

#define nbre_type_links 9


/*****************************/

#define E0  -0.1
#define E2   0.1
#define EPS 0.0001
#define epsilon 0.00001

#ifndef pi
#define pi 3.14159
#endif

#define Max_voie 5              /* nbre max de voies de liaisons pour 1 neurone */
#define PAS_RT_TOKEN -100

/*---------------------------------------------------------------*/
/*                  TYPES DE DONNEES UTILISEES                   */
/*---------------------------------------------------------------*/


/* description d'un neurone                                      */

typedef struct type_coeff
{
    float val;                  /* valeur du coefficient               */
    float proba;                /* probabilite du poids                */
    float Nbre_ES;              /* pour credit assignement temporel    */
    float Nbre_S;               /* pour Arnaud nombre de fois ou une sortie a ete active */
    float Nbre_E;               /* nb de fois ou un entree a ete activee  */
    int entree;                 /* numero de l'entree associee         */
    int type;                   /* modifiable ou non                   */
    /* 2n   :  ei*wij    non modifiable    */
    /* 2n+1 : |ei - wij |    modifiable    */
    /* n numero de la voie                 */
    int evolution;              /* si le poids peut evoluer ou non     */
    /* si non modif alors evolution=0      */
    float moy;                  /* valeur moyenne de l'entree          */
    float smoy;                 /* valeur integree de la sortie        */
    int gpe_liaison;
    struct type_coeff *s;       /* pointe vers le coefficient suivant  */
} type_coeff;

typedef struct type_neurone
{
    float seuil;                /* seuil du neurone                    */
    float s;                    /* sortie non seuillee                 */
    float s1;                   /* sortie temps t1                     */
    float s2;                   /* sortie temps t2                     */
    int flag;                   /* utilise pour noter les neurones a   */
    /* modifier avec PTM                   */
    /* utilise par les neurones bloqueurs  */
    /* pour donner l'adresse du macro N    */

    float d;                    /* retropagation de l'erreur           */
    /* ou tolerance pour le seuil          */
    /* ou activite de la voie inconditionnelle */

    float last_activation;      /* instant de la derniere activation du neurone */

    float cste;                 /* coeff correspondant au seuil        */
    int groupe;                 /* numero du groupe d'appartenance     */
    int nbre_voie;              /* nbre de voies de liaisons ratachees */
    int nbre_coeff;             /* nombre de coeffs rataches           */

    char max;                   /* indique si le neurone est le max de */
    /* son groupe/ supporter max <1        */
    /* pour Stephane                       */

    float posx;                 /* position du neurone dans l'espace   */
    float posy;
    float posz;
    type_coeff *coeff;          /* pointeur vers liste des coefficients */
    type_coeff *nsor;           /* liste des neurones de sortie        */
} type_neurone;

/* description de la structure du reseau                         */

typedef struct type_noeud
{
    int numero;                 /* numero du neurone associe           */
    struct type_noeud *s;       /* pointe  neurone suivant du groupe   */
} type_noeud;

#define TAILLE_CHAINE 256

typedef struct type_noeud_comment
{
    char chaine[TAILLE_CHAINE];
    struct type_noeud_comment *suiv;
} type_noeud_comment;

typedef struct type_prom_data
{
    long type;
    long size;
    struct type_prom_data *next;
    char prom_data_element[1];
} type_prom_data;


typedef struct arg_thread_grp
{
    int gpe;
    int i;
    int gestion_STM;
    int learn;
    int no_thread;
    int retour;
    int mvt;
	sem_t * p_is_launched;/*semaphore compteur s'assurant que les threads sont bien crees avant de poursuivre*/
} arg_thread_grp;


#ifndef _LIMBIC_NEUROMOD_structs_noeudmod_listemod_H
#define _LIMBIC_NEUROMOD_structs_noeudmod_listemod_H

/* Liste des modulations du meme type arrivant sur le groupe */

typedef struct st_noeud_modulation
{
    float poids;                /* Poids du lien de neuromodulation */
    int num_neurone;            /* Numero du neurone d'entree effectuant la neuromodulation */
    struct st_noeud_modulation *suivant;
} noeud_modulation;

/* Liste de toutes les neuromodulations affectees a un groupe */

typedef struct st_liste_neuromodulations
{
    char *type;                 /* Type de la neuromodulation en chaine de caracteres           */
    noeud_modulation *liste_modulation; /* Liste des modulations du meme type arrivant sur le groupe    */
    struct st_liste_neuromodulations *suivant;
} liste_types_neuromodulations;

#endif

#define MIN_FLOAT_EPS 1e-30
/* pour les tests d'equalite entre 2  flottants */
static inline int isequal(float x, float y)
{
  return (fabs(x-y)<1e-30);
}

/* pour les tests de difference entre 2  flottants */
static inline int isdiff(float x, float y)
{
  return (fabs(x-y)>1e-30);
}

/*--------------------------------------------------------------*/
/*         structure des donnees pour le script                 */
/* pour la creation de reseaux avec cluster                     */
/* systeme interactif de creation de reseaux                    */
/*--------------------------------------------------------------*/

extern const char *stop_if_not_int(const char *chaine); /* defini dans le parser outils_script.c */
extern const char *stop_if_not_float(const char *chaine);   /* defini dans le parser outils_script.c */

/* MY_FloatAffect(x,y) : affecte un flottant y a la variable x (float ou string) */
/* MY_IntAffect(x,y):  affecte un entier y a la variable x (float ou string) */
/* MY_Str2Float(x,y): convertit la string y en float x (ou string x) */

#ifndef SYMBOLIQUE_VERSION
#define my_int(x)   int x
#define my_d       %d
#define my_float(x) float x
#define my_f       %f
#define my_adr     &
#define MY_Int2Int(x) x
#define MY_Float2Float(x) x
#define MY_Data_Copy(x,y) x=y
/* attention fonction dangereuse , pas d'allocation memoire*/
#define MY_Float2Str(x) Float2Str(x)
#define MY_Int2Str(x) Int2Str(x)
/*----------*/
#define Str2MY_Float(x,y) x=atof(stop_if_not_float(y))
#define Str2MY_Int(x,y) x=atoi(stop_if_not_int(y))
/* affecte un float y au float x */
#define MY_FloatAffect(x,y) x=y
#define MY_IntAffect(x,y) x=y
#else
#define my_int(x)   char x[255]
#define my_d       #%s
#define my_float(x) char x[255]
#define my_f       #%s
#define my_adr                  /* rien */
#define MY_Int2Int(x) atoi(x)
#define MY_Float2Float(x) atof(x)
#define MY_Data_Copy(x,y) strcpy(x,y)
#define MY_Float2Str(x) x
#define MY_Int2Str(x) x
#define Str2MY_Float(x,y) strcpy(x,y)
#define Str2MY_Int(x,y) strcpy(x,y)
#define MY_FloatAffect(x,y) strcpy(x,Float2Str(y))
#define MY_IntAffect(x,y) strcpy(x,Int2Str(y))
#endif

#define AFF1(chaine,type)  chaine #type
#define AFF(chaine,type) AFF1(chaine,type)

#define taille_max_tableau_diff_gaussienne 500 /* 30 */

#define SIZE_NO_NAME 1024
typedef struct type_groupe
{
    int no;                     /*  Numero du groupe pour utilisateur  */
    char no_name[SIZE_NO_NAME]; /* numero simbolique utile pour des scripts symboliques complexes */
    int posx;                   /*   position du centre du groupe      */
    int posy;
    int p_posx;                 /*   position du centre du groupe      */
    int p_posy;                 /*  dans le debug de promethe          */
    int reverse;                /* 1: orientation inverse pour dessin  */
    int nbre_voie;              /* nbre de voies de liaisons           */

    int deja_active;            /* pour eviter de le re-etudier, active une fois la mise a jour terminee          */
    int active;                 /* active juste avant la mise a jour, jusqu'a la fin de la maj                    */
    int deja_appris;
    int ech_temps;              /* echelle temporelle pour savoir quand */
    /* l'executer                          */
    int premier_ele;            /* no du premier neurone du groupe     */
    char nom[100];              /* nom du groupe pour entree ou sortie */

    int selected;               /* pour les futures ameliorations de l'IHM : selection multiple de groupes a deplacer... */
    int debug;                  /* 1 si le groupe doit etre affiche    */
    /* lors du debug, 0 sinon              */
    int type;                   /*         type du groupe              */
      my_int(type2);            /*  specifie si c'est une sortie       */
      /*  pour rt_token : numero correspondant a la priorite du token */

      my_int(taillex);          /* nbre de lignes de la carte          */
      my_int(tailley);          /* nbre de colonnes de la carte        */
      my_int(nbre);             /* nbre de neurones dans le groupe     */
      my_float(seuil);          /* seuil pour chaque neurone si diff 0 */
      my_float(tolerance);      /* pour neurones a trois etats, colonne */


      my_int(dvp);
      my_int(dvn);              /* distance voisinage d diff et compet */
      my_float(alpha);          /* coeff pour le renforcement dans PTM */
      my_float(nbre_de_1);      /* estimation normalisation pour PTM   */
      my_float(sigma_f);        /* constante pour les fcts de sortie   */
      my_float(learning_rate);  /* simulation speed and learning rate   */
      my_float(simulation_speed);

      my_float(noise_level);


    float tableau_gaussienne[taille_max_tableau_diff_gaussienne];   /* init diff si carte           */

    type_prom_data *video_ext;  /*  ces champs ont le meme objectif que ext mais ils sont specialises dans le traitement de */
    type_prom_data *tactil_ext; /* donnees particulieres. Nous avons identifie des donnees de type video (image et sequence d'image)    */
    type_prom_data *audio_ext;  /*  sonores et tactiles. Type_prom_data est un liste chainee ou chaque maillon contient une donnee particuliere, */
    /* son type, sa taille, et le maillon suivant. */

    void *ext;                  /* pour permettre de rajouter des trucs particuliers a un groupe */
    /* ce pointeur est initialiser a NULL au debut de l'execution de promethe */
    void *data;                 /* pour stocker les donnees que l'on ne veut pas demander plusieurs fois ... */
    /* ce pointeur est initialiser a NULL au debut de l'execution de promethe */

    liste_types_neuromodulations *liste_types_neuromodulations; /* pointeur vers la liste des neuromodulation associees au groupe. */
    /* Ce pointer est initialise au lancement de promethe lors de      */
    /* l'initialisation des groupes.                                   */
    type_noeud_comment *comment;    /*liste des commentaires associee a un groupe      */
    void (*appel_apprend) (int gpe);
    void (*appel_activite) (int no_neurone, int local_gestion_STM,
                            int local_learn);
    void (*appel_gestion) (int gpe);
    void (*appel_algo) (int gpe);

    /* pour la creation et la destruction de structures dediees a un groupe au debut et a la fin d'une simulation */
    void (*init) (void);
    void (*free) (void);

    /* gestion heritage de fonctionnalites */

    void (*inherit) (struct type_groupe * this, struct type_groupe * parent);

    /* pour la gestion du thread associe a un groupe */

      void *(*create_and_manage) (void);  /* pas encore utilise ... */
      void (*new) (int gpe);      /* fonctions appelee a la fin de create_and_manage() */
      void (*destroy) (int gpe);    
      void (*save) (int gpe); /* fonction utilisee lors de la sauvegarde du reseau */

    pthread_t pthread;

  sem_t sem_lock_fields;      /* semaphore pour eviter de modifier les champs du groupes tant que tout n'est pas fini*/
                              /* important pour les rt_tokens */

    sem_t sem_wake_up;          /* semaphore utilise pour communiquer avec le groupe */
    sem_t sem_ack;              /* quand le groupe a fini le traitement lie a wake_up */
    char message[255];          /* zone utilisee pour passer un message */
    int return_value;           /* valeur de retour lors de l'utilisation du gpe: */
    /*  0 si le  groupe n'a pu s'executer, 1 si OK et 2 si un break intervient  */

    struct type_groupe *s;
    int rttoken;               /*Rend le groupe directement activable. Gestion Execution temps reel */


    void (*appel_maj_STM_entrees) (int gpe);
    void (*appel_maj_STM_sorties) (int gpe);

    my_int(nb_threads);	/*nombre de threads lors du calcul de la mise a jour M.M. 07/02/07*/

} type_groupe;

typedef struct type_coude
{
  float x,y;                      /* coordonnes du coude */
  int relatif;                  /* relatif=0 le coude ne bouge pas a moins d'etre deplace  */
  struct type_coude *first;
  struct type_coude *s;         /* coude suivant */
} type_coude;

typedef struct type_liaison
{
  int deja_active;              /* sert a savoir pour leto si le lien a ete lu d'un script insere (=1 si insere) (=0 en direct) */
  int secondaire;             /* secondaire =1 pas necessaire pour jetons circulant */
  /* secondaire =0 indispensable jeton present en entree */
  int type;                   /*        type de la liaison                          */
  int mode;                   /* si mode = 1 --> distance                           */
  /* si mode = 0 --> produit                            */
  /* si mode = 2 --> distance vers buts                 */
  /* si mode = 3 --> produit vers buts                  */
  /* pour gerer le type de voie                         */
  int posx1;                  /* extremites de la liaison                           */
  int posy1;
  int posx2;
  int posy2;
  
  int style;                  /* type des coudes du lien 0: segments, 1: courbe      */
  type_coude *coude;          /* 1er coude de la liste des coudes                    */
  type_coude *fin_coude;      /* pt vers le dernier (associe au groupe d'arrivee     */
  type_coude *coude_legende;  /* pointeur vers le coude associe a la legende du lien */

  my_float(norme);          /* valeur initiale ou intervale coeff                 */
  int depart;                 /*         groupe de depart                           */
  char depart_name[SIZE_NO_NAME];
  int arrivee;                /*         groupe d'arrivee                           */
  char arrivee_name[SIZE_NO_NAME];
  my_float(temps);          /* temps de mem. pour ces liaisons en entree          */
  my_float(stemps);         /* temps de mem. pour ces liaisons en sortie          */
  my_int(nbre);             /*  nombre d'entrees pour un neurone                  */
  my_float(dv_x);           /* distances pour liaisons vers voisinage             */
  my_float(dv_y);           /* distances pour liaisons vers voisinage             */
  my_float(proba);          /* probabilite de creation de la liaison              */
  char nom[100];              /* nom de la liaison pour traitements algo            */
  type_noeud_comment *comment;    /*liste des commentaires associee a une liaison       */
  struct type_liaison *s;
} type_liaison;


typedef type_neurone *type_tableau; /* tableau contenant le reseau       */
typedef type_liaison *type_tableau_voies;   /* tableau contenant les voies de liaison       */
typedef float **type_matrice;   /* tableau contenant les entrees     */
typedef float *type_vecteur;    /* pour vecteur d'entree ou sortie   */
typedef int *type_vecteur_entier;

typedef type_noeud *type_tableau_groupe;
typedef type_noeud *pointeur_type_noeud;

#define EPSILON 1.e-10
#define FABS(x) ((x>0)?x:-x)
#define FLOAT_NEAR(x,y) (FABS(x-y)<EPSILON)

extern void Str2Int(int *x, char *chaine);


extern void Str2Float(float *x, char *chaine);



/**************************************************************/
/* transforme un entier en chaine de caracteres */
/* attention si l'on ne recopie pas le resultat renvoye par un 1er
appel avant le 2eme appel il sera remplace par le resultat du 2eme appel!
Une seule zone de memoire allouee... */
/* fonctions definies dans Tx_graphic.c ou cc_leto.c */
extern char *Int2Str(int entier);


extern char *Float2Str(float x);


#endif

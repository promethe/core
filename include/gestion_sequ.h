/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
#ifndef GESTION_SEQU_H
#define GESTION_SEQU_H

/*-------------------------------------------------------------------*/
/*   gere le sequencement des actions dans le reseau                 */
/*   utilise uniquement pour la partie algo                          */
/*   la partie RN fonctionne en mode synchrone qd sequencement fini  */
/*                                                                   */
/*   commence toujours par le noeud debut                            */
/*   lorsque un noeud a toute ses entrees actives, il s'active       */
/*   une fois active, il devient deja active                         */
/*   lorsque un groupe a toute ses entrees deja activees il s'active */
/*                                                                   */
/*   peut declencher un groupe si une liaison secondaire n'a pas de  */
/*   jeton en entree, si la liaison n'est pas secondaire: impossible */
/*-------------------------------------------------------------------*/


/*-------------------------------------------------------------------*/
/* fonction recursive servant a mettre a jour les differentes        */
/* echelles temporelles                                              */
/*-------------------------------------------------------------------*/

extern void mise_a_jour_des_sous_echelles(int ech);


extern void mise_a_jour_des_sous_echellesb(int ech);
extern void apprend_groupe(int gpe);
extern void set_tokens(int ech);

extern int echelle_temps_learn; /* echelle de temps a la fin de laquelle */
                /* l'apprentissage du RN est declenche   */
extern int echelle_temps_debug; /* idem mais pour l'affichage dans promethe */
                /* du debug du RN                           */

extern int flag_temps_dynamique;    /* =1 lorsque l'on est dans le 1/2 temps de modif mouv de Arnaud */

extern int etat_step_by_step;

extern sem_t attend_un_rt_token;

#ifdef USE_THREADS
extern pthread_mutex_t mutex_section_critique_sequenceur ;
extern pthread_mutex_t mutex_section_critique_sequenceur_attend ;
#endif

#endif

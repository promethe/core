/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
#if !defined (__CONSTS__)
#define __CONSTS__

#define STR_LIMIT	255

/* Valeurs de retour */
#define RET_CONNECT_OK			 2
#define RET_CONNECT				 1
#define RET_OK					 0
#define RET_ERR					-1
#define RET_NODATA				-2
#define RET_BUSY        		-3
#define RET_DECO				-4
#define RET_CONNECT_REFUSED		-5

/* Types de lien : reception ou transmission */
#define RX						0
#define TX						1

/* Les protocoles */
#define TCP 					0
#define UDP						1
#define IVY						2

/* Temps max d attente pour la reception d un message */
#define TIMEOUT 				30

/* Temps max d attente pour la reception d un accuse */
#define TIMEOUT_ACK 			30

/* Messages d info envoyes pour les connections */
#define CONNECT_REFUSED 		0
#define CONNECT_OK				1
#define CONNECT_ALREADY_EXIST 	2

/* Nom des flags pour les liens */
#define BLOCK 					"block"
#define ACK 					"ack"
#define NOIMG 					"noext"
#define NOEXT					"noext"
#define WITHEXT					"ext"
#define RAZ 					"raz"
#define TIMEOUT_OPT				"timeout"
#define FFT					"fft"
#define NEXT					"next"

/* Nom des flags reseau */
#define MSG						0
#define MSG_ASK_ACK				1
#define MSG_ACK					2
#define MSG_CONNECT				3
#define	MSG_CONNECT_OK			4
#define MSG_CONNECT_REFUSED		5

/* Flag pour la partie donnees des messages reseau */
#define DATA					0
#define DATA_IMG				1
#define DATA_ACK				2
#define DATA_ACK_IMG			3

/* type de donnees */
#define NEURONE					0
#define IMAGE					1


#endif /* __CONSTS__ */

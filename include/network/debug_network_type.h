/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/**
\defgroup debug_network_type debug_network_type.h
\ingroup libcomm

\brief structure of debug network in libcomm

\file
\ingroup debug_network_type
\brief structure of debug network in libcomm
*/

#ifndef __DEBUG_NETWORK_TYPE__
#define __DEBUG_NETWORK_TYPE__


#include <sys/time.h>
#include <sys/types.h>

/** default debug period (ms) */
#define DEBUG_CYCLE 30000

/** handler for the different debug variables */
typedef struct debug_network_s
{
  /** start time stamp */
  struct timeval start_ts;
  /** period for printing debug (-1 is no printing) */
  long debug_cycle;


  /** mean latency before reception */
  float recv_latence_mean;
  /** min latency before reception */
  float recv_latence_min;
  /** max latency before reception */
  float recv_latence_max;
  /** mean period between two reception */
  float recv_period_mean;
  /** min period between two reception */
  float recv_period_min;
  /** max period between two reception */
  float recv_period_max;
  /** mean latency before reading */
  float read_latence_mean;
  /** min latency before reading */
  float read_latence_min;
  /** max latency before reading */
  float read_latence_max;
  /** mean paquet size */
  int paquet_size_mean;
  /** min paquet size */
  int paquet_size_min;
  /** max paquet size */
  int paquet_size_max;
  /** mean miss reading */
  float miss_read_mean;
  /** min miss reading */
  float miss_read_min;
  /** max miss reading */
  float miss_read_max;
  /** nb of recv during debug_cycle */
  long nb_recv;
  /** nb of reading during debug cycle */
  long nb_read;
  /** nb of reading of a new message during debug cycle */
  long nb_read_new_msg;
  /** number of try to read */
  long nb_try_read;  
  /** recv time stamp */
  struct timeval recv_time;
  /** previous recv time stamp */
  struct timeval prev_recv_time;
  /** previous recv time stamp tmp */
  struct timeval prev_recv_time_tmp;
  /** read time stamp */
  struct timeval read_time;

  /** case_ext is 1 if separate management of ext, 0 otherwise */
  int case_ext;
  /** case ext : mean latency before reception */
  float ext_recv_latence_mean;
  /** case ext : min latency before reception */
  float ext_recv_latence_min;
  /** case ext : max latency before reception */
  float ext_recv_latence_max;
  /** case ext : mean period between a reception and ext reception*/
  float ext_recv_ante_period_mean;
  /** case ext : min period between a reception and ext reception */
  float ext_recv_ante_period_min;
  /** case ext : max period between a reception and ext reception */
  float ext_recv_ante_period_max;
  /** case ext : mean period between ext reception and another reception */
  float ext_recv_post_period_mean;
  /** case ext : min period between ext reception and another reception */
  float ext_recv_post_period_min;
  /** case ext : max period between ext reception and another reception */
  float ext_recv_post_period_max;
  /** case ext : mean period between 2 ext receptions */
  float ext_recv_period_mean;
  /** case ext : min period between 2 ext receptions */
  float ext_recv_period_min;
  /** case ext : max period between 2 ext receptions */
  float ext_recv_period_max;
  /** case ext : mean latency before reading */
  float ext_read_latence_mean;
  /** case ext : min latency before reading */
  float ext_read_latence_min;
  /** case ext : max latency before reading */
  float ext_read_latence_max;
  /** case ext : mean paquet size */
  int ext_paquet_size_mean;
  /** case ext : min paquet size */
  int ext_paquet_size_min;
  /** case ext : max paquet size */
  int ext_paquet_size_max;
  /** case ext : mean miss reading */
  float ext_miss_read_mean;
  /** case ext : min miss reading */
  float ext_miss_read_min;
  /** case ext : max miss reading */
  float ext_miss_read_max;
  /** case ext : nb of recv during debug_cycle */
  long ext_nb_recv;
  /** case ext : nb of reading during debug cycle */
  long ext_nb_read;
  /** case ext : nb of reading of a new message during debug cycle */
  long ext_nb_read_new_msg;
  /** case ext : number of try to read */
  long ext_nb_try_read;
  /** case ext : flag last recv is ext */
  int last_recv_ext;
  /** case ext : recv time stamp */
  struct timeval ext_recv_time;
  /** case ext : read time stamp */
  struct timeval ext_read_time;
  /** previous ext recv time stamp */
  struct timeval ext_prev_recv_time;

  /** send time sec */
  long send_t_sec;
  /** send time usec */
  long send_t_usec;

  /** last debug print time stamp */
  struct timeval last_debug_time;
  /** ptr on virtual link name */
  char *name;
} debug_network_t;

#endif /* __DEBUG_NETWORK_TYPE__ */

/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
#ifndef PANDORA_CONNECT_H
#define PANDORA_CONNECT_H

#include <ivy.h>
#include <Ivy/ivyglibloop.h>
#include <semaphore.h>

#define PANDORA_NUMBER_OF_CHANNELS 10 //Nombre de canaux de communication entre Prométhé et Japet
#define NB_SCRIPTS_MAX 30 //Nombre maximal de scripts qu'on peut détecter (et donc afficher) simultanément

enum{
 PANDORA_START = 0,
 PANDORA_STOP,
 PANDORA_SEND_NEURONS_START,
 PANDORA_SEND_NEURONS_STOP,
 PANDORA_SEND_NEURONS_ONE,
 PANDORA_SEND_EXT_START,
 PANDORA_SEND_EXT_STOP,
 PANDORA_SEND_EXT_ONE,
 PANDORA_SEND_PHASES_INFO_START,
 PANDORA_SEND_PHASES_INFO_STOP,
 PANDORA_SEND_NEURO_LINKS_START,
 PANDORA_SEND_NEURO_LINKS_STOP,
 PANDORA_SEND_OK_DEBUG_GRP_MEM,
 PANDORA_SEND_STOP_DEBUG_GRP_MEM
};

typedef struct arg_pandora
{
  char* ivyget;
  int compression;
  int port;
}arg_pandora;


extern int pandora_send_phases_info;
extern int pandora_activated;
extern int pandora_is_activating;
//Implémentée dans ../prom_kernel/src/prom_send_to_pandora.c
void* pandora_connect(void *data);
void pandora_disconnect();


//void quit_pandora(type_connexion_udp* connexion);

//Implémentée dans ../prom_kernel/src/shared/ivy_bus.c
void pandora_callback(IvyClientPtr app, void* data, int argc, char** argv);
void send_neurons_to_pandora(int gpe);
void send_token_pandora(int gpe, int phase, long temps);


#endif

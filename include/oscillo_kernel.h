/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/*
 * oscillo_kernel.h
 *
 *  Modified on: Feb 20, 2012
 *      Author: Arnaud Blanchard
 */

#ifndef OSCILLO_KERNEL_H
#define OSCILLO_KERNEL_H

#include <sys/time.h>
#include <unistd.h>
#include <libx.h>

#ifndef AVEUGLE
#include "prom_tools/include/oscillo_kernel_display.h"
#endif

#include "prom_enet_debug.h"
#include "pandora_connect.h"

#define SIZE_OF_HISTORIC_OF_GROUP_PROFILE 256
#define SIZE_OF_DISPLAY_INFO 256

void init_com_def_groupe(type_com_groupe *com_def_groupe);

#ifndef AVEUGLE
extern type_profiler *profiler;
#define DISPLAY_OSCILLO_KERNEL(gpe, phase) \
if (oscillo_kernel_activated == 1) group_profiler_update_info(profiler, gpe, phase, temps_oscillo);
#else
#define DISPLAY_OSCILLO_KERNEL(gpe,phase)
#endif

#define UPDATE_ENET_OSCILLO_KERNEL(gpe, phase) \
send_token_pandora(gpe, phase, temps_oscillo);\
if(oscillo_dist_activated == 1) send_token_oscillo_kernel(gpe, phase, temps_oscillo);


#ifndef OSCILLO_KERNEL
#define UPDATE_OSCILLO_KERNEL(gpe, phase)
#else
glong temps_oscillo;
#define UPDATE_OSCILLO_KERNEL(gpe, phase) \
temps_oscillo = long_us_time(); \
DISPLAY_OSCILLO_KERNEL(gpe, phase) \
UPDATE_ENET_OSCILLO_KERNEL(gpe, phase)\


extern int oscillo_kernel_activated;
extern int recent_reset_affichage;
extern int hauteur_display_gpe;
extern struct timeval InitTimeTrace;

extern int oscillo_dist_activated; /* pour le control de l'envoie ou non des donnees au dist_prom */

void reset_affichage_oscillo_kernel();
void create_fenetre_oscillo_kernel();
void affiche_oscillo_distant();

void show_oscillo_kernel();

void oscillo_dist_status();

void init_oscillo_window();

void create_window_of_profiler(const char *title);

void create_fenetre_oscillo_kernel();

void add_promethe_oscillo(type_com_groupe *def_groupe, int nb_gpe, const char *name_promethe);

void oscillo_kernel_pressed();

void affiche_oscillo_kernel();

static inline long long_us_time()
{
  long Secondes, MicroSecondes, temps;
  struct timeval CurrentTimeTrace;

  gettimeofday(&CurrentTimeTrace, (void *) NULL);

  Secondes = CurrentTimeTrace.tv_sec - InitTimeTrace.tv_sec;
  MicroSecondes = CurrentTimeTrace.tv_usec - InitTimeTrace.tv_usec;
  temps = Secondes * 1000000 + MicroSecondes;
  return temps;
}

#endif
#endif





/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/*
 * enet_network.h
 *
 *  Created on: Sep 27, 2013
 *      Author: arnablan
 */

#ifndef ENET_NETWORK_H
#define ENET_NETWORK_H
#define ENET_UNLIMITED_BANDWITH 0
#define NET_VARS_MAX 256
#define NET_VAR_TIMEOUT 1



#include <limits.h>
#include <enet/enet.h>
#include <semaphore.h>

typedef void(*type_recv_callback)(char* data, size_t size, void *user_data);
typedef struct net_var type_net_var;
typedef struct connection type_connection;


typedef enum {
  CONNECTION_CONNECTED, CONNECTION_DISCONNECTED, CONNECTION_TRYING
}CONNECTION_TYPE;

/**
 * a connection can be void if nothing has been initialised, waiting if a distant promethe has said it has launch a server,  connected if it is working, server if the local promethe has started a server waiting for a peer.
 */
struct connection {
  char prom_name[NAME_MAX + 1];
  char ip[HOST_NAME_MAX + 1];
  int port;
  ENetHost *host;
  ENetPeer *peer;
  CONNECTION_TYPE type;
  int busy_channels_nb;
  sem_t *semaphore;
  type_net_var *net_vars[NET_VARS_MAX];
};

typedef enum {
  NET_VAR_SEND = 0, NET_VAR_RECV, NET_VAR_AVAILABLE, NET_VAR_WAITING
} NET_VAR_TYPE;

typedef enum {
  NET_VAR_AUTO = 0, NET_VAR_MANUAL
} NET_VAR_MODE;

struct net_var {
  char name[NAME_MAX + 1];
  NET_VAR_TYPE type;
  NET_VAR_MODE mode;
  enet_uint8 channel;
  type_connection *connection;
  char *buffer;
  void *user_data;
  type_recv_callback recv_callback;
  int connected;
};


void enet_network_init();
void enet_network_finish();

void enet_close_connections();
type_net_var *net_var_find(const char *name);
type_net_var* net_var_recv(const char *name, type_recv_callback recv_callback, void *user_data);
type_net_var * net_var_recv_manual(const char *name, const char *ip, int port, type_recv_callback recv_callback, void *user_data);
type_net_var* net_var_send(const char *name, type_recv_callback recv_callback, void *user_data);
type_net_var* net_var_send_manual(const char *name, const char *ip, int port, type_recv_callback recv_callback, void *user_data);
void net_var_waiting(const char *name, const char *prom_name, const char *ip, int port);
void net_var_available(const char *name, const char *prom_name, const char* ip, int port);
void net_var_send_data(type_net_var *net_var, const char *buffer, size_t size);

#endif /* ENET_NETWORK_H_ */

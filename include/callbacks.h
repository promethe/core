/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
#ifndef CALLBACKS_H
#define CALLBACKS_H

#include <gtk/gtk.h>

#ifdef OSCILLO_KERNEL
extern void arrete_oscillo_kernel(GtkWidget * widget);
extern void affiche_oscillo_kernel(GtkWidget * widget, const char*name);
extern void show_oscillo_kernel();
extern gboolean fenetre_oscillo_kernel_expose_event(GtkWidget * widget, GdkEventExpose * event,
					     gpointer data);
extern gboolean fenetre_oscillo_kernel_configure_event(GtkWidget * widget, GdkEventConfigure * event,
						gpointer data);
#endif


extern gboolean kernel_configure_event(GtkWidget * widget,
                                         GdkEventConfigure * event,
                                         gpointer data);
extern gboolean kernel_expose_event(GtkWidget * widget,
                                      GdkEventExpose * event, gpointer data);
extern gboolean fenetre1_configure_event(GtkWidget * widget,
                                         GdkEventConfigure * event,
                                         gpointer data);
extern gboolean fenetre2_configure_event(GtkWidget * widget,
                                         GdkEventConfigure * event,
                                         gpointer data);
extern gboolean image1_configure_event(GtkWidget * widget,
                                       GdkEventConfigure * event,
                                       gpointer data);
extern gboolean image2_configure_event(GtkWidget * widget,
                                       GdkEventConfigure * event,
                                       gpointer data);

extern gboolean fenetre1_expose_event(GtkWidget * widget,
                                      GdkEventExpose * event, gpointer data);
extern gboolean fenetre2_expose_event(GtkWidget * widget,
                                      GdkEventExpose * event, gpointer data);
extern gboolean image1_expose_event(GtkWidget * widget,
                                    GdkEventExpose * event, gpointer data);
extern gboolean image2_expose_event(GtkWidget * widget,
                                    GdkEventExpose * event, gpointer data);
extern gboolean image1_click_event(GtkWidget * widget, GdkEventButton * event,
                                   gpointer data);
extern gboolean image1_release_event(GtkWidget * widget, GdkEventButton * event,
                                   gpointer data);
extern gboolean image1_motion_event(GtkWidget * widget, GdkEventButton * event,
                                   gpointer data);
extern gboolean image2_click_event(GtkWidget * widget, GdkEventButton * event,
                                   gpointer data);
extern gboolean image2_release_event(GtkWidget * widget, GdkEventButton * event,
                                   gpointer data);
extern gboolean image2_motion_event(GtkWidget * widget, GdkEventButton * event,
                                   gpointer data);
extern gint delete_event(GtkWidget * widget, GdkEvent * event, gpointer data);
extern void delete(GtkWidget * widget, GdkEvent * event, gpointer data);
extern void file_quit_cmd_callback(GtkWidget * widget, gpointer data);
extern void about(void);



/*extern void lecture_imageb(GtkWidget *widget, gpointer data);*/

extern void check_step_by_step_cb(gpointer callback_data,
                                  guint callback_action,
                                  GtkWidget * menu_item);
/*extern gboolean step_by_step_pressed(GtkWidget * widget, gpointer data);
extern gboolean continue_pressed(GtkWidget * widget, gpointer data);*/
/* extern gboolean cancel_pressed(GtkWidget * widget, gpointer data); */ /* mis dans promethe3.c pour pouvoir aussi etre apppele par ivy */


extern void rafraichissement_oui(GtkWidget * widget, gpointer data);
extern void rafraichissement_non(GtkWidget * widget, gpointer data);
extern void modif_coeff(GtkWidget * widget, gpointer data);

extern void check_item_demo_callback(gpointer callback_data,
                                     guint callback_action,
                                     GtkWidget * menu_item);
extern void check_item_fast_callback(gpointer callback_data,
                                     guint callback_action,
                                     GtkWidget * menu_item);
extern void check_debug(gpointer callback_data, guint callback_action,
                        GtkWidget * menu_item);

extern void check_inverse_video_callback(gpointer callback_data,
                                         guint callback_action,
                                         GtkWidget * menu_item);

extern void dessine_rnb(GtkWidget * widget, gpointer data);

extern void ecriture_lenab(GtkWidget * widget, gpointer data);
extern void affiche_neuroneb(GtkWidget * widget, gpointer data);

extern void show_fenetre_2(GtkWidget * widget, gpointer data);
extern void show_image_2(GtkWidget * widget, gpointer data);
extern void show_image_1(GtkWidget * widget, gpointer data);




extern void cb_hscale_get_value(GtkAdjustment * adj);
extern gboolean cb_hide_fenetre(GtkWidget * widget, gpointer data);
extern gboolean cb_destroy_fenetre(GtkWidget * widget, gpointer data);
extern void check_debug_link(gpointer callback_data, guint callback_action,GtkWidget * menu_item);

extern void stack_threads(GtkWidget * widget);

#endif

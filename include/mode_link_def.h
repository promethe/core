/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/*--------------------------------------------------*/
/** types and constants definitions for link modes **/
/*--------------------------------------------------*/

/*** ATTENTION les valeurs de mode superieures a 1000 et inferieures a 2000 sont
 * exclusivement reservees pour les liens de neuromodulation neurone
 * par neurone ***/
#define NEUROMOD                     1000
#define NEUROMOD_learn               1001
#define NEUROMOD_decay               1002
#define NEUROMOD_decay2              1003
#define NEUROMOD_decay3              1004
/** param sigma dans gaussienne dans SAW*/
#define NEUROMOD_sigma               1010 


/***
 *  Mode specifiques pour beneficier de la non gestion micro/macro
 *  neurones pour les modes dont les valeurs sont au dessus de
 *  NEUROMOD
 */
#define inhibMotiv		     2001 /* inhibition specifique de
					   * la motivation dans la
					   * carte cognitive*/




/** modes par defauts */

#define No_mode_link_product_compet  0   /* value obtained after compet */
#define No_mode_link_distance_compet 1
#define No_mode_link_product_analog  2   /* direct analog value */
#define No_mode_link_distance_analog 3

/** Mode pour distance euclidienne dans le SAW */
#define No_mode_link_distance_euclidienne 4
#define No_mode_link_distance_expo        5
#define No_mode_link_produit              6


/** mode pour la gestion des micros neurones */

#define MACRO_MC          0
#define MICRO_DIST        1
#define MICRO_SUPERVISOR  7

#define MACRO_LINKS      0        /**< links between micro and macro neurons */
#define RECURRENT_LINKS  1    /* links inside de RRNN : hebbian learning */
#define INCOMING_LINKS   2    /* incoming links : hebbian learning */
#define INPUT_LINKS      3    /* input links : no learning */

#define MACRO_LINKS	       0        /**< links between micro and macro neurons */
#define PROXIMAL_HEBBIAN       1        /**< DG  -> CA3               */
#define DG_CA		       1        /**< DG  -> CA3               */
#define PROXIMAL_NON_HEBBIAN   2        /**< not used !!!             */
#define INTERMEDIATE_HEBBIAN   3        /**< CA3 -> CA3 ou CA3 -> CA1 */
#define REC_CA_CA	       3        /**< CA3 -> CA3 ou CA3 -> CA1 */
#define DISTAL_HEBBIAN         4        /**< EC2 -> CA3 ou EC3 -> CA1 */
#define DISTAL_NON_HEBBIAN     5        /**< EC1 -> CA3 (Sorin)       */
#define EC_CA		       5	/**< EC1 -> CA3 (Sorin)       */
#define INHIBITION	       6
#define TYPE_SIGMA	       7    /*!< type lien pour neurone sommateur dans sigma-pi */


#define MACRO_LINKS		0 /**< links between micro and macro neurons */
#define RECURRENT_HEBBIAN	1
#define MOTIVATIONNAL 		2
#define NEURO_MOD_BUT		3
#define	CA1			-1


/** max number of displayed links is 255 (last one is for ending NULL pointer)*/
#define MAX_NUMBER_LINKS   256

/****************************************/

/** definition des types */

/** shared type - do not modify */
typedef struct type_lien_no_nom
{
      int no;
      char nom[256];
      int editable[256];          /* nombre max d'item que les liens ou les groupes peuvent avoir ... */

} type_lien_no_nom;

typedef struct mode_lien_s
{
      char abbrev[32];
      type_lien_no_nom type_lien;
}mode_lien;

typedef struct group_mode_link {
      int nb_mode;
      mode_lien *mode_tab;
}group_mode_link;



/*---------------------------------------*/


extern void init_group_mode_link_tab(void);
extern group_mode_link group_mode_link_tab[nbre_type_groupes];

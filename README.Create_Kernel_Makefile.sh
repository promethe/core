Usage: Create_Kernel_Makefile.sh [options]
     --enable-debug     Compile libkernel in debug configuration
     --enable-enet      Compile libkernel with enet
     --enable-threads   Compile libkernel with threads
     --enable-blind	Compile libkernel without graphic interface
 -h, --help       	This help

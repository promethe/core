/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/*
 * prom_jpeg_compressor.c
 *
 * A JPEG compressor for prom_images_struct -> prom_images_struct_compressed
 *
 *  Created on: 31 juil. 2014
 *      Author: jfellus
 */


#include <stdio.h>
#include <string.h>

#include <net_message_debug_dist.h>
#include <libx.h>
#include <limits.h>

#include <jpeglib.h>
#include "prom_jpeg_compressor.h"

struct _prom_jpeg_compressor {
	prom_images_struct *images_in;
	prom_images_struct_compressed *images_out;

	struct jpeg_compress_struct cinfo;
	struct jpeg_error_mgr jerr;
};


// << mem_dst_mgr : Custom destination manager for JPEG compressor//


typedef struct memory_destination_mgr_def{
	struct jpeg_destination_mgr super;

	unsigned int buffer_size;
	unsigned char *current_image;
	unsigned int current_image_size;
} mem_dst_mgr;


static mem_dst_mgr * mem_dst_mgr_new(struct jpeg_compress_struct *cinfo, unsigned int jpeg_max_size);
static void mem_dst_mgr_destroy(mem_dst_mgr * mgr);
static void mem_dst_mgr_set_image(prom_jpeg_compressor *my_data, int img_id);
static void mem_dst_mgr_update_image_size(prom_jpeg_compressor *my_data, int img_id);
static void mem_dst_mgr_init(struct jpeg_compress_struct * cinfo);
static int mem_dst_mgr_empty_output_buffer(struct jpeg_compress_struct * cinfo);
static void mem_dst_mgr_term(struct jpeg_compress_struct * cinfo);

// >> end mem_dst_mgr



prom_jpeg_compressor* prom_jpeg_compressor_create(prom_images_struct* images_in, prom_images_struct_compressed* images_out, int quality) {
	int jpeg_max_size, i;
	prom_jpeg_compressor* data = ALLOCATION(prom_jpeg_compressor);
	data->images_in = images_in;

	data->cinfo.err = jpeg_std_error(&data->jerr);
	jpeg_create_compress(&data->cinfo);

	data->cinfo.image_height = data->images_in->sy;
	data->cinfo.image_width = data->images_in->sx;
	data->cinfo.input_components = data->images_in->nb_band;

	switch(data->images_in->nb_band){
	case 1:
		data->cinfo.in_color_space = JCS_GRAYSCALE;
		break;
	case 3:
		data->cinfo.in_color_space = JCS_RGB;
		break;
	}

	jpeg_set_defaults(&data->cinfo);

	printf("JPEG image spec: %i:%ix%i\n",data->images_in->sx,data->images_in->sy,data->images_in->nb_band);

	data->cinfo.dct_method = JDCT_IFAST;
	data->cinfo.do_fancy_downsampling = FALSE;

	jpeg_set_quality(&data->cinfo, quality,TRUE);


	if(!images_out)	images_out = ALLOCATION(prom_images_struct_compressed);
	data->images_out = images_out;
	images_out->super.image_number = data->images_in->image_number;
	images_out->super.sx = data->images_in->sx;
	images_out->super.sy = data->images_in->sy;
	images_out->super.nb_band = data->images_in->nb_band;

	// We suppose that a compressed image won't be larger than the original image (sic)
	jpeg_max_size = data->images_in->sx * data->images_in->sy * data->images_in->nb_band;
	for(i=0;i<images_out->super.image_number;i++){
		images_out->super.images_table[i] = MANY_ALLOCATIONS(jpeg_max_size, unsigned char);
		images_out->images_size[i] = 0;
	}

	mem_dst_mgr_new(&data->cinfo, jpeg_max_size);
	return data;
}

void prom_jpeg_compressor_destroy(prom_jpeg_compressor* c) {
	mem_dst_mgr_destroy((mem_dst_mgr*)c->cinfo.dest);
	jpeg_destroy_compress(&c->cinfo);
	free(c); c = NULL;
}

void prom_jpeg_compressor_compress(prom_jpeg_compressor* data){
	int i, row_stride;
	JSAMPROW row_pointer[1];

	if(!data || !data->images_out || !data->images_in) {PRINT_WARNING("Call prom_jpeg_compressor_init(...) first !"); return;}

	for(i=0; i<data->images_out->super.image_number;i++){

		mem_dst_mgr_set_image(data,i);

		jpeg_start_compress(&data->cinfo, TRUE);

		row_stride = data->images_in->sx * data->images_in->nb_band;
		while(data->cinfo.next_scanline < data->cinfo.image_height){
			row_pointer[0] = & data->images_in->images_table[i][data->cinfo.next_scanline * row_stride];
			jpeg_write_scanlines(&data->cinfo, row_pointer, 1);
		}

		jpeg_finish_compress(&data->cinfo);

		mem_dst_mgr_update_image_size(data,i);
	}
}

prom_images_struct* prom_jpeg_compressor_get_in(prom_jpeg_compressor* c) {return c->images_in;}
prom_images_struct_compressed* prom_jpeg_compressor_get_out(prom_jpeg_compressor* c) {return c->images_out;}



/////////////////
// mem_dst_mgr //
/////////////////

static mem_dst_mgr * mem_dst_mgr_new(struct jpeg_compress_struct *cinfo, unsigned int jpeg_max_size){
	mem_dst_mgr * mgr = malloc(sizeof(mem_dst_mgr));

	mgr->super.init_destination = mem_dst_mgr_init;
	mgr->super.empty_output_buffer = mem_dst_mgr_empty_output_buffer;
	mgr->super.term_destination = mem_dst_mgr_term;

	mgr->buffer_size = jpeg_max_size;

	cinfo->dest = (struct jpeg_destination_mgr *)mgr;

	return mgr;
}

static void mem_dst_mgr_destroy(mem_dst_mgr * mgr){
	free(mgr);
}

static void mem_dst_mgr_set_image(prom_jpeg_compressor *my_data, int img_id){
	mem_dst_mgr * mgr = (mem_dst_mgr*) my_data->cinfo.dest;

	mgr->current_image = my_data->images_out->super.images_table[img_id];
	mgr->current_image_size = 0;

}

static void mem_dst_mgr_update_image_size(prom_jpeg_compressor *my_data, int img_id){
	mem_dst_mgr * mgr = (mem_dst_mgr*) my_data->cinfo.dest;
	my_data->images_out->images_size[img_id] = mgr->current_image_size;
}

static void mem_dst_mgr_init(struct jpeg_compress_struct * cinfo){
	mem_dst_mgr * mgr = (mem_dst_mgr*) cinfo->dest;
	mgr->super.next_output_byte = mgr->current_image;
	mgr->super.free_in_buffer = mgr->buffer_size;
}


static int mem_dst_mgr_empty_output_buffer(struct jpeg_compress_struct * cinfo){
	mem_dst_mgr_init(cinfo);
	return 1;
}
static void mem_dst_mgr_term(struct jpeg_compress_struct * cinfo){
	mem_dst_mgr * mgr = (mem_dst_mgr*) cinfo->dest;
	mgr->current_image_size = mgr->buffer_size - mgr->super.free_in_buffer;
}

/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/*--------------------------------------------------------------------*/
/* Outil pour la lecture et la sauvegarde de la position des groupes. */
/* + dans le futur autres donnees telles que la position des fenetres */
/* Si se fichier est lu apres le script alors on peut laisser les 2   */
/* en prallele et meme regenerer le fichier de config graphique grace */
/* au fichier script.                                                 */
/* gcd pour graphic context details (3 lettres pour compatibilite)    */
/* P. Gaussier fevrier 2006                                           */
/*--------------------------------------------------------------------*/

#include "public.h"
#include "outils_script.h"
#include <gtk/gtk.h>
/* #define DEBUG */

void group_set_local_debug(type_groupe *group, int flags_local_debug)
{
  group->debug |= (flags_local_debug & (FLAG_LOCAL_DEBUG | FLAG_LOCAL_NORMALIZE_DEBUG));
  group->debug &= (flags_local_debug | ~(FLAG_LOCAL_DEBUG | FLAG_LOCAL_NORMALIZE_DEBUG)); /* Seul les bits correspondant aux FLAGS_LOCAL_DEBUG sont changes */
}

void reset_debug()
{
	int j;
	for (j = 0; j < nbre_groupe; j++)
		group_set_local_debug(&def_groupe[j], 0);
}

void active_debug()
{
	int j;
	for (j = 0; j < nbre_groupe; j++)
	  group_set_local_debug(&def_groupe[j], FLAG_LOCAL_DEBUG);
}



void lecture_gcd(char *local_nomfich1)
{
	FILE *local_f;
	int no, largeur, hauteur, presente;
	char ligne[255], no_name[255];
	type_noeud_comment *first_comment = NULL;
	int symbolic_gcd = 0, x, y;
	int local_debug;

	ligne[0] = '\0';

	local_f = fopen(local_nomfich1, "r");

	if (local_f == NULL)
	{
		/*
		 * Erreur lecture gcd (il n'y en a probablement pas).
		 */
		return;
	}

	first_comment = read_line_with_comment(local_f, first_comment, ligne);
	if (first_comment != NULL)
	{
		if (recherche_champs(first_comment->chaine, "SYMBOLIC_NO"))
		{
			symbolic_gcd = 1;
		}
	}
	if (recherche_champs(ligne, "begin") != NULL)
	{
		sscanf(ligne, "begin dimensions\n");
		first_comment = read_line_with_comment(local_f, first_comment, ligne);
		sscanf(ligne, "fenetre1  %d %d %d\n", &largeur, &hauteur, &presente);
		gtk_widget_set_size_request(fenetre1.window, largeur, hauteur);
		first_comment = read_line_with_comment(local_f, first_comment, ligne);
		sscanf(ligne, "fenetre2  %d %d %d\n", &largeur, &hauteur, &presente);
		gtk_widget_set_size_request(fenetre2.window, largeur, hauteur);
		first_comment = read_line_with_comment(local_f, first_comment, ligne);
		sscanf(ligne, "image1  %d %d %d\n", &largeur, &hauteur, &presente);
		gtk_widget_set_size_request(image1.window, largeur, hauteur);
		first_comment = read_line_with_comment(local_f, first_comment, ligne);
		sscanf(ligne, "image2  %d %d %d\n", &largeur, &hauteur, &presente);
		gtk_widget_set_size_request(image2.window, largeur, hauteur);
		first_comment = read_line_with_comment(local_f, first_comment, ligne);
		/*sscanf(ligne,"end dimensions\n"); */
		if (recherche_champs(ligne, "end") == NULL)
		{
			EXIT_ON_ERROR("attention le end de la definition des dimentions n'a ps ete trouve\nUn pb dans votre fichier .gcd : solution le supprimer et laisser promethe le regenerer \n");
		}
	}

	while (feof(local_f) == 0)
	{
		first_comment = read_line_with_comment(local_f, first_comment, ligne);
		if (recherche_champs(ligne, "groupe") != NULL)
		{
			sscanf(ligne, "groupe = %s \n", no_name);
#ifdef DEBUG
			printf("lecture groupe %s\n", no_name);
#endif
			if (symbolic_gcd == 0) no = atoi(no_name);
			else no = find_no_associated_to_symbolic_name(no_name, NULL);
			if (no > nbre_groupe)
			{
				printf("Echec de la lecture du gcd pour le groupe %d > nbre_groupes = %d\n", no, nbre_groupe);
				exit(1);
			}
			else if (no >= 0)
			{
				first_comment = read_line_with_comment(local_f, first_comment, ligne);
				if (recherche_champs(ligne, "p_posx") != NULL) sscanf(ligne, "p_posx = %d , p_posy = %d\n", &def_groupe[no].p_posx, &def_groupe[no].p_posy);
				first_comment = read_line_with_comment(local_f, first_comment, ligne);
				if (recherche_champs(ligne, "debug") != NULL) sscanf(ligne, "debug = %d\n", &local_debug);
				group_set_local_debug(&def_groupe[no], local_debug);
			}
			else
			{
				printf("WARNING: Le No symbolique %s n'existe pas (groupe ignore du gcd) \n", no_name);
			}
		}
		else
		{
			if (recherche_champs(ligne, "RESET_DEBUG") != NULL) reset_debug();
			else if (recherche_champs(ligne, "DEBUG_ALL") != NULL) active_debug();

			else if (recherche_champs(ligne, "Begin") != NULL)
			{
				sscanf(ligne, "Begin positions\n");
				first_comment = read_line_with_comment(local_f, first_comment, ligne);
				sscanf(ligne, "fenetre1  %d %d \n", &x, &y);
				printf("set windows  position: %d %d\n", x, y);
				gtk_window_move((GtkWindow *) fenetre1.window, x, y);
				first_comment = read_line_with_comment(local_f, first_comment, ligne);
				sscanf(ligne, "fenetre2  %d %d \n", &x, &y);
				printf("set windows  position: %d %d\n", x, y);
				gtk_window_move((GtkWindow *) fenetre2.window, x, y);
				first_comment = read_line_with_comment(local_f, first_comment, ligne);
				sscanf(ligne, "image1  %d %d \n", &x, &y);
				printf("set windows  position: %d %d\n", x, y);
				gtk_window_move((GtkWindow *) image1.window, x, y);
				first_comment = read_line_with_comment(local_f, first_comment, ligne);
				sscanf(ligne, "image2  %d %d \n", &x, &y);
				printf("set windows  position: %d %d\n", x, y);
				gtk_window_move((GtkWindow *) image2.window, x, y);
				first_comment = read_line_with_comment(local_f, first_comment, ligne);
				if (recherche_champs(ligne, "End") == NULL)
				{
					printf("attention le End de la definition des positions n'a ps ete trouve\n");
					printf("un pb dans votre fichier .gcd : solution le supprimer et laisser promethe le regenerer \n");
					exit(1);
				}
			}
		}
	}
	fclose(local_f);
}

void ecriture_gcd(char *local_nomfich1)
{
	FILE *local_f;
	int i, x, y, presente;

	local_f = fopen(local_nomfich1, "w");
	if (local_f == NULL)
	{
		printf("\n Erreur lors de l'ouverture du fichier contenant le gcd : %s \n", local_nomfich1);
		return;
	}
#ifdef DEBUG
	printf("--- ecriture du gcd : %s\n", local_nomfich1);
#endif
	fprintf(local_f, "%%SYMBOLIC_NO\n");

	presente = 0; /* pas encore gerer , test si icone */
	fprintf(local_f, "begin dimensions\n");
	fprintf(local_f, "fenetre1  %d %d %d \n", fenetre1.window->allocation.width, fenetre1.window->allocation.height, presente);
	fprintf(local_f, "fenetre2  %d %d %d \n", fenetre2.window->allocation.width, fenetre2.window->allocation.height, presente);
	fprintf(local_f, "image1  %d %d %d \n", image1.window->allocation.width, image1.window->allocation.height, presente);
	fprintf(local_f, "image2  %d %d %d \n", image2.window->allocation.width, image2.window->allocation.height, presente);
	fprintf(local_f, "end dimensions\n");

	for (i = 0; i < nbre_groupe; i++)
	{
		fprintf(local_f, "groupe = %s \n", def_groupe[i].no_name);
		fprintf(local_f, "p_posx = %d , p_posy = %d\n", def_groupe[i].p_posx, def_groupe[i].p_posy);
		fprintf(local_f, "debug = %d\n", def_groupe[i].debug);
	}
	fprintf(local_f, "Begin positions\n");
	gtk_window_get_position((GtkWindow *) fenetre1.window, &x, &y);
	fprintf(local_f, "fenetre1  %d %d \n", x, y);
	gtk_window_get_position((GtkWindow *) fenetre2.window, &x, &y);
	fprintf(local_f, "fenetre2  %d %d \n", x, y);
	gtk_window_get_position((GtkWindow *) image1.window, &x, &y);
	fprintf(local_f, "image1  %d %d \n", x, y);
	gtk_window_get_position((GtkWindow *) image2.window, &x, &y);
	fprintf(local_f, "image2  %d %d \n", x, y);
	gtk_window_get_position((GtkWindow *) fenetre1.window, &x, &y);
	fprintf(local_f, "End positions\n");
	fclose(local_f);
#ifdef DEBUG
	printf("Sauvegarde du gcd terminee. \n");
#endif
}

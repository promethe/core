/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <pthread.h>
#include <sys/time.h>
#include <sys/types.h>
#include <semaphore.h>

#include "blc.h"

#include "global.h"
#include "consts.h"
#include "libcomm_types.h"
#include "tcp_link.h"
#include "network_message.h"
#include "virtual_link.h"
#include "network_link.h"
#include "debug_network.h"

#include "prom_tools/include/basic_tools.h"

/** Initialisation d un lien reseau */
void network_link_init(network_link_t *netlink, char* name, char* ip, int port, unsigned short protocol)
{

	memset(netlink->name, 0, STR_LIMIT);

	/** Verification sur la taille des chaines de caracteres */
	if (strlen(name) < STR_LIMIT)
	{
		memset(netlink->name, 0, STR_LIMIT);
		memcpy(netlink->name, name, strlen(name) + 1);
	}
	else EXIT_ON_ERROR("Nom d'hote superieur à %d caracteres!\n", STR_LIMIT);

	memset(netlink->ip, 0, STR_LIMIT);
	if (strlen(ip) < STR_LIMIT)
	{
		memset(netlink->ip, 0, STR_LIMIT);
		memcpy(netlink->ip, ip, strlen(ip) + 1);
	}
	else EXIT_ON_ERROR("Nom d'hote superieur à %d caracteres!\n", STR_LIMIT);

	/** Initialisation info lien */
	netlink->port = port;
	netlink->protocol = protocol;
	netlink->connected = 0;
	netlink->rx = 0;

	/** Initialisation thread de reception */
	CLEAR(netlink->thread);

	/** Initialisation pour envoi */
	netlink->id_tx = 0;
	netlink->size_max_msg_tx = 0;
	netlink->message_tx = NULL;

	/** Initialisation pour reception */
	netlink->id_rx = 0;
	netlink->size_max_msg_rx = 0;
	netlink->message_rx = NULL;

	/** Taille partie reseau   ---  size  ---         ---  flag  ---    */
	netlink->size_netmsg = sizeof(unsigned int) + sizeof(unsigned int) +
	/**  --- id  ---            ---  timestamp  ---     ---  nom lien  ---     */
	sizeof(unsigned int) + (2 * sizeof(long_64_t)) + (STR_LIMIT * sizeof(char));

	/** mutex pour l envoi des donnees */
	if (pthread_mutex_init(&netlink->mut_send, NULL)) EXIT_ON_SYSTEM_ERROR("pthread_mutex_init()");

	/** initialisation chaine virtual links */
	netlink->vl_chaine = NULL;
}

void network_link_close(network_link_t *netlink)
{

	/* fermeture thread */
	if (netlink->connected)
	{
		if ((errno = pthread_cancel(netlink->thread))) EXIT_ON_SYSTEM_ERROR("libcomm : network_link_close() : pthread_cancel()");
	}

	/* fermeture mutex */
	if ((errno = pthread_mutex_destroy(&netlink->mut_send))) EXIT_ON_SYSTEM_ERROR("libcomm : network_link_close() : pthread_mutex_destroy()");

	/* fermeture socket */
	if (netlink->connected)
	{
			tcp_link_close(netlink->socket);
			kprints("libcomm : network_link_close() : %s close\n", netlink->name);
	}

	/** free buffer */
	if (netlink->message_tx) free(netlink->message_tx);
	if (netlink->message_rx) free(netlink->message_rx);

}

int network_link_find(char name[STR_LIMIT])
{
	unsigned int i;

	for (i = 0; i < nb_netlink; i++)
	{
		if (!strcmp(name, netlinks[i].name)) return (i);
	}
	return (-1);
}

/** GESTION DES ENVOIS DE MESSAGES */
int network_link_send(network_link_t* netlink, unsigned int flag, virtual_link_t *vlink)
{
	int retval;

	if (netlink->connected == 0)
	{
		if (!(vlink->error_flag & NETLINK_NOT_CONNECTED))
		{
			kprints("libcomm : network_link_send() : netlink not connected : %s\n", netlink->name);
			vlink->error_flag |= NETLINK_NOT_CONNECTED; /* Ajoute l'erreur */
		}
		return (RET_ERR);
	}
	else
	{
		if (vlink != NULL)
		{
			if (vlink->error_flag & NETLINK_NOT_CONNECTED)
			{
				kprints("libcomm : network_link_send() : netlink %s connected: OK\n", netlink->name);
				vlink->error_flag &= !NETLINK_NOT_CONNECTED; /* Supprime l'erreur mais pas les autres.*/
			}
		}
	}

	if ((errno = pthread_mutex_lock(&netlink->mut_send)) != 0) EXIT_ON_SYSTEM_ERROR("libcomm : network_link_send() : pthread_mutex_lock()");

	/* Construction du message */
	retval = network_message_build(netlink, flag, vlink);

	if (retval == RET_ERR) EXIT_ON_ERROR("libcomm : network_link_send() : message construction error\n");

	/** On envoie le message */
	if (netlink->protocol == TCP) retval = tcp_link_send(netlink);
	/*	else if(netlink->protocol == UDP); */

	/*VERIF SI DECO*/
	if ((retval == RET_ERR) || (retval == RET_DECO))
	{
		kprints("libcomm : network_link_send() : netlink disconnected : %s\n", netlink->name);
		netlink->connected = 0;

		/** fermeture socket */
		tcp_link_close(netlink->socket);

		if ((errno = pthread_cancel(netlink->thread))) EXIT_ON_SYSTEM_ERROR("libcomm : network_link_send() : pthread_cancel()");
		virtual_link_disconnect(vlink->network_link);

		if ((errno = pthread_mutex_unlock(&netlink->mut_send)) != 0) EXIT_ON_SYSTEM_ERROR("libcomm : network_link_send() : pthread_mutex_unlock()");
		return (RET_ERR);
	}

	if ((errno = pthread_mutex_unlock(&netlink->mut_send)) != 0) EXIT_ON_SYSTEM_ERROR("libcomm : network_link_send() : pthread_mutex_unlock()");

	return (RET_OK);
}

/** GESTION DE LA RECEPTION DES MESSAGES */

void network_link_launch_recv_thread(network_link_t* netlink)
{
	if (pthread_create(&netlink->thread, NULL, network_link_recv_thread, netlink)) EXIT_ON_SYSTEM_ERROR("libcomm : network_link_launch_recv_thread() : pthread_create()");
	if (pthread_detach(netlink->thread)) EXIT_ON_SYSTEM_ERROR("libcomm : network_link_launch_recv_thread() : pthread_detach()");
}

void* network_link_recv_thread(void* data)
{
	fd_set rfds;
	struct timeval tv;
	int ret_select = 0;
	int ret_recv = 0;
	int ret_handle = 0;
	network_link_t* netlink = (network_link_t*) data;
	virtual_link_t * vl_ptr = NULL;

	/** Parametrage du thread pour pouvoir le terminer */
	if (pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, NULL)) EXIT_ON_SYSTEM_ERROR("libcomm : network_link_recv_thread() : pthread_setcancelstate()");
	if (pthread_setcanceltype(PTHREAD_CANCEL_ASYNCHRONOUS, NULL)) EXIT_ON_SYSTEM_ERROR("libcomm : network_link_recv_thread() : pthread_setcanceltype()");

	tv.tv_sec = TIMEOUT;
	tv.tv_usec = 0;

	/** on met a zero les id */
	netlink->id_tx = 0;
	netlink->id_rx = 0;

	while (1)
	{
		/** test si une requete d'annulation est en attente*/
		pthread_testcancel();

		FD_ZERO(&rfds);
		FD_SET(netlink->socket, &rfds);

		if (netlink->rx) ret_select = select(netlink->socket + 1, &rfds, NULL, NULL, &tv);
		else ret_select = select(netlink->socket + 1, &rfds, NULL, NULL, NULL);


#ifdef DEBUG_NETWORK
		/** check time for printing debug for the different related
		 * virtual links*/
		for (vl_ptr = netlink->vl_chaine; vl_ptr != NULL; vl_ptr = vl_ptr->vl_next)
			debug_network_print(&vl_ptr->debug);
#endif

		switch (ret_select)
		{
		case 0:
			kprints("libcomm : network_link_recv_thread() : No data from %s since %u seconds!\n", netlink->name, TIMEOUT);
			FD_CLR(netlink->socket, &rfds);
			tv.tv_sec = TIMEOUT;
			tv.tv_usec = 0;
			continue;
			break;
		case -1:
			EXIT_ON_SYSTEM_ERROR("libcomm : network_link_recv_thread() : select()");
			break;
		case 1:
			if (FD_ISSET(netlink->socket, &rfds))
			{
	    		ret_recv = tcp_link_recv(netlink);

				if (!netlink->rx) netlink->rx = 1;
				FD_CLR(netlink->socket, &rfds);
				tv.tv_sec = TIMEOUT;
				tv.tv_usec = 0;
			}
			else EXIT_ON_ERROR("libcomm : network_link_recv_thread() : select() error\n");
			break;

		default:
			EXIT_ON_ERROR("libcomm : network_link_recv_thread() : select() error\n");
			break;
		}

		switch (ret_recv)
		{
		case RET_DECO:
			netlink->connected = 0;
			virtual_link_disconnect(network_link_find(netlink->name));
			/** fermeture socket */
			tcp_link_close(netlink->socket);
			kprints("libcomm : network_link_recv_thread() : %s disconnected.\t Recv server stopped\n", netlink->name);
			pthread_exit(NULL);
			break;
		case RET_ERR:
			netlink->connected = 0;
			virtual_link_disconnect(network_link_find(netlink->name));
			/** fermeture socket */
			tcp_link_close(netlink->socket);

			PRINT_WARNING("libcomm : network_link_recv_thread() : %s error.\t Recv server stopped\n", netlink->name);
			pthread_exit(NULL);
			break;
		case RET_OK:
			ret_handle = network_message_read(netlink);
			break;
		default:
			netlink->connected = 0;
			virtual_link_disconnect(network_link_find(netlink->name));
			/** fermeture socket */
			tcp_link_close(netlink->socket);
			kprints("libcomm : network_link_recv_thread() : %s disconnected.\t Recv server stopped\n", netlink->name);
			pthread_exit(NULL);
			break;
		}

		switch (ret_handle)
		{
		case RET_ERR:
			EXIT_ON_ERROR("libcomm : network_link_recv_thread() : message read error\n");
			break;
		case RET_OK:
			break;
		default:
			EXIT_ON_ERROR("libcomm : network_link_recv_thread() : message read error\n");
			break;
		}
	}
	pthread_exit(NULL);
}

/** GESTION DE LA CONNECTION DES LIENS */

/**
 Tente de connecter un lien reseau
 */
void network_link_connect(network_link_t* netlink)
{
	int retval = 0;
	;

	if (!netlink->connected)
	{
	  retval = tcp_link_connect(netlink);

		/** envoie du message de connection */
		if (retval == RET_OK)
		{
			netlink->connected = 1;
			retval = network_link_send(netlink, MSG_CONNECT, NULL);
			/* TODO : test retval */
		}
		else
		{
			netlink->connected = 0;
			kprints("libcomm : network_link_connect() : connection failed : %s\n", netlink->name);
			return;
		}

		/** On recupere le message de retour */
		retval = tcp_link_recv(netlink);

		/** Lecture message de retour */
		if (retval == RET_OK) retval = network_message_read(netlink);
		else
		{
			netlink->connected = 0;
			kprints("libcomm : network_link_connect() : connection failed : %s\n", netlink->name);
			return;
		}

		/** On verifi le message de retour */
		if (retval != RET_CONNECT_OK)
		{
			netlink->connected = 0;
			kprints("libcomm : network_link_connect() : connection refused : %s\n", netlink->name);
			return;
		}

		network_link_launch_recv_thread(netlink);
		kprints("libcomm : network_link_connect() : connection succeed : %s\n", netlink->name);

	}
	else
	{
		kprints("libcomm : network_link_connect() : network link already connected\n");
		return;
	}
}

static int need_tcp_server()
{
	unsigned int i;
	for (i = 0; i < nb_netlink; i++)
		if (netlinks[i].protocol == TCP) return (1);
	return (0);
}

void network_link_launch_server(char name[STR_LIMIT], unsigned int port)
{
	if (need_tcp_server())
	{
		tcp_server = ALLOCATION(network_link_t);
		network_link_init(tcp_server, name, name, port, TCP);

		tcp_link_init_server(tcp_server);
		tcp_server->connected = 1;

		if (!tcp_server->thread)
		{
			if (pthread_create(&tcp_server->thread, NULL, network_link_accept_connection, tcp_server)) EXIT_ON_SYSTEM_ERROR("libcomm : network_link_launch_server() : pthread_create()");
			if (pthread_detach(tcp_server->thread)) EXIT_ON_SYSTEM_ERROR("libcomm : network_link_launch_server() : pthread_detach()");
		}
	}
	else
	{
		tcp_server = NULL;
	}
}

void network_link_close_server()
{
	if (tcp_server)
	{
		/** fermeture thread */
		if ((tcp_server->thread) && (errno = pthread_cancel(tcp_server->thread))) EXIT_ON_SYSTEM_ERROR("libcomm : network_link_close_server() : pthread_cancel()");

		/** fermeture mutex */
		if ((errno = pthread_mutex_destroy(&tcp_server->mut_send))) EXIT_ON_SYSTEM_ERROR("libcomm : network_link_close_server() : pthread_mutex_destroy()");


			tcp_link_close(tcp_server->socket_server);
			PRINT_WARNING("libcomm : network_link_close_server() : tcp server close\n");

		/** free buffer */
		if (tcp_server->message_tx) free(tcp_server->message_tx);
		if (tcp_server->message_rx) free(tcp_server->message_rx);

		free(tcp_server);
	}
}

void* network_link_accept_connection(void *data)
{
	int retval = 0;
	int num_link;
	network_link_t *server = (network_link_t *) data;

	/** Parametrage du thread pour pouvoir le terminer */
	if (pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, NULL)) EXIT_ON_SYSTEM_ERROR("libcomm : network_link_accept_connection() : pthread_setcancelstate()");
	if (pthread_setcanceltype(PTHREAD_CANCEL_ASYNCHRONOUS, NULL)) EXIT_ON_SYSTEM_ERROR("libcomm : network_link_accept_connection() : pthread_setcanceltype()");

	while (1)
	{
		/** On attend une connection */
		tcp_link_accept(server);

		/** On recupere le message de connection */
		retval = tcp_link_recv(server);

		/** On lit le message de connection */
		if (retval == RET_OK)
		{
			num_link = network_message_read(server);
		}
		else
		{
			close(server->socket);
			continue;
		}

		/** On renvoie un message de confirmation de connection */
		if (num_link < 0)
		{
			retval = network_link_send(server, MSG_CONNECT_REFUSED, NULL);
			close(server->socket);
			continue;
		}
		else	retval = network_link_send(server, MSG_CONNECT_OK, NULL);

		/** On met le netlink dans l'etat connecte */
		if ((retval == RET_OK) && (num_link >= 0))
		{
			netlinks[num_link].socket = server->socket;
			netlinks[num_link].connected = 1;
			network_link_launch_recv_thread(&netlinks[num_link]);
			kprints("libcomm : network_link_accept_connection() : connection succeed : %s\n", netlinks[num_link].name);
		}
		else
		{
			close(server->socket);
			continue;
		}
	}
}


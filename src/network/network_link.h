/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/**
   \defgroup network_link network_link.h
   \ingroup libcomm

   \brief structure of network link in libcomm

   \file
   \ingroup network_link
   \brief structure of network link in libcomm
*/

#if !defined (__NETWORK_LINK__)
#define __NETWORK_LINK__

#include <netinet/in.h>
#include <pthread.h>
#include "consts.h"
#include "virtual_link.h"

typedef struct network_link_s
{
/** Nom du lien reseau */
  char name[STR_LIMIT];			
/** ip de la machine a contacter */
  char ip[STR_LIMIT];
/** port de la machine a contacter */
  int port;
/** protocole de communication utilise */			
  unsigned short protocol;
/** indique si le lien reseau doit recevoir des informations */
  unsigned short rx;
/** socket */
  int socket;
/** socket : utilise uniquement pour les serveurs */
  int socket_server;
  struct sockaddr_in address;
/** mutex pour proteger le buffer de donnees */
  pthread_mutex_t mut_send;
/** thread de reception de donnees */
  pthread_t thread;
/** indique si le lien est connecte */
  unsigned short connected;
/** id du message en transmission */
  unsigned int id_tx;
/** buffer du message pour la transmission */
  char* message_tx;
/** taille du buffer */
  unsigned int size_max_msg_tx;	
/** id du messsage en reception */
  unsigned int id_rx;
/** buffer du message pour la receptio, */
  char* message_rx;
/** taille du buffer */
  unsigned int size_max_msg_rx;	
/** taille de la parti reseau du message */
  unsigned int size_netmsg;
/** ptr on related virtual links */
  virtual_link_t *vl_chaine;
} network_link_t;

/**
  initialise la structure network_link_t
*/
void network_link_init(network_link_t *netlink, char* name, char* ip , int port, unsigned short protocol);

/**
  libere la structure network_link_t et ferme la connection si besoin
*/
void network_link_close(network_link_t *netlink);

/**
  cherche un lien reseau avec son nom
  renvoie le numero du lien dans le tableau (variable globale)
  sinon renvoie -1
*/
int network_link_find(char name[STR_LIMIT]);

/**
  Construit puis envoie un message de flag flag
*/
int network_link_send(network_link_t* netlink, unsigned int flag, virtual_link_t *vlink);

/**
  Lance le thread de reception pour un network_link_t
*/
void network_link_launch_recv_thread(network_link_t* netlink);

/**
  thread de reception des messages
*/
void* network_link_recv_thread(void* data);

/**
  Tente de connecter un lien reseau
*/
void network_link_connect(network_link_t* netlink);

/**
  Lance les serveurs necessaire pour accepter les connexion
*/
void network_link_launch_server(char name[STR_LIMIT], unsigned int port);

/**
  Ferme les serveurs
*/
void network_link_close_server();

/**
  thread pour accepter les connections
*/
void* network_link_accept_connection(void *data);

#endif /* __NETWORK_LINK__ */

/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/time.h>
#include <sys/types.h> 

#include "global.h"
#include "consts.h"
#include "network_message.h"
#include "network_link.h"
#include "virtual_link.h"
#include "libcomm_types.h"

/**
*
*	network_message.c
*	function : network_message_build, network_message_read
*
*	Construit et traite les messages reseau selon le protocol :
*	---  size  ---  flag  ---  id  ---  nom lien  ---  timestamp  --- donnees  ---
*
**/


/* Tableau des fonctions associees aux messages envoye */
handler_assos_tx fct_handler_tx[] = {
	{MSG, msg_handler_tx},
	{MSG_ACK, msg_ack_handler_tx},
	{MSG_CONNECT, msg_connect_handler_tx},
	{MSG_CONNECT_OK, msg_connect_ok_handler_tx},
	{MSG_CONNECT_REFUSED, msg_connect_refused_handler_tx}
};

/* Tableau des fonctions associees aux messages recus */
handler_assos_rx fct_handler_rx[] = {
	{MSG, msg_handler_rx},
	{MSG_ACK, msg_ack_handler_rx},
	{MSG_CONNECT, msg_connect_handler_rx},
	{MSG_CONNECT_OK, msg_connect_ok_handler_rx},
	{MSG_CONNECT_REFUSED, msg_connect_refused_handler_rx}
};


/* 
	Construit les differents type de messages reseau en fonction du flag 
*/
int network_message_build(network_link_t* netlink, unsigned int flag, virtual_link_t* vlink)
{
	int retval;
	int index;
	int nb_flag;

	nb_flag = sizeof(fct_handler_tx) / sizeof(handler_assos_tx);

	for(index = 0; index <  nb_flag; index++)
	{
	  if((unsigned int)fct_handler_tx[index].flag == flag)
		{
			retval = fct_handler_tx[index].handler(netlink, vlink);
			break;
		}
	}

	if((retval == RET_OK) && (index < nb_flag))
	{
		/*if(vlink)
			printf("id_tx (%s) : %u\n", vlink->name, netlink->id_tx);
		else
			printf("id_tx : %u\n", netlink->id_tx);*/
		netlink->id_tx++;
		return (RET_OK);
	}
	else
	{
		return (RET_ERR);
	}
}


/* 
	Lit les message reseau et effectue les verification sur le message 
*/
int network_message_read(network_link_t* netlink)
{
	int retval;
	int index;
	int nb_flag;
	unsigned int flag;
	char* pt;

	/* LECTURE DU FLAG */
	
	pt = netlink->message_rx;
	
	/* On recupere la taille du message */
	pt += sizeof(unsigned int);

	/* On recupere le flag du message */
	flag = *(unsigned int*) pt;
	pt += sizeof(unsigned int);

	nb_flag = sizeof(fct_handler_rx) / sizeof(handler_assos_rx);

	for(index = 0; index <  nb_flag; index++)
	{
	  if((unsigned int)fct_handler_rx[index].flag == flag)
		{
			retval = fct_handler_rx[index].handler(netlink);
			break;
		}
	}

	if((index < nb_flag) && (retval >= 0))
	{
		/*printf("id_rx : %u\n", netlink->id_rx);*/
		netlink->id_rx++;
	}
	else
	{
		return (RET_ERR);
	}


	
	return (retval);
}



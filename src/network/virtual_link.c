/*
 Copyright  ETIS � ENSEA, Universit� de Cergy-Pontoise, CNRS (1991-2014)
 promethe@ensea.fr

 Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier,
 C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouz�ne,
 M. Lagarde, S. Lepr�tre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengerv�, A. Revel ...

 See more details and updates in the file AUTHORS

 This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
 This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software.
 You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license,
 users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability.
 In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software,
 that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge.
 Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured
 and, more generally, to use and operate it in the same conditions as regards security.
 The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
 */

/* #define DEBUG */
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <pthread.h>
#include <semaphore.h>
#include <sys/time.h>
#include <time.h>

#include "consts.h"
#include "virtual_link.h"
#include "network_link.h"
#include "global.h"
#include "debug_network.h"

int virtual_link_find(char* name)
{
   int i;
   for (i = 0; (unsigned int) i < nb_vlink; i++)
      if (!strcmp(name, vlinks[i].name)) return (i);
   return (RET_ERR);
}

static int virtual_link_find_flag(const char* flag, char* link_info)
{
   int size_flag;
   int size;
   int i;
   char* pt;

   size_flag = strlen(flag);
   size = strlen(link_info);
   i = 0;
   pt = link_info;

   while (i < size)
   {
      while (*pt != '-' && *pt != '\0')
      {
         pt++;
         i++;
      }
      pt++;
      i++;
      if ((size - i) < size_flag)
      {
         return 0;
      }
      else
      {
         if (!strncmp(flag, pt, size_flag)) return 1;
      }
   }

   return 0;
}

static int virtual_link_find_flag_with_value(const char *option, char* link_info)
{
   int size_flag;
   int size;
   int size_time = 0;
   int i;
   char* pt;
   char* ptr;

   int option_value;
   char save = 0;

   size_flag = strlen(option);
   size = strlen(link_info);
   i = 0;
   pt = link_info;

   while (i < size)
   {
      while (*pt != '-' && *pt != '\0' && *pt != '=')
      {
         pt++;
         i++;
      }
      pt++;
      i++;
      if ((size - i) < size_flag)
      {
         return -1;
      }
      else
      {
         if (!strncmp(option, pt, size_flag))
         {
            while (*pt != '-' && *pt != '\0' && *pt != '=')
            {
               pt++;
               i++;
            }

            if (*pt != '=')
            {
               return 0;
            }

            pt++;
            i++;

            ptr = pt;

            while (*ptr != '-' && *ptr != '\0')
            {
               ptr++;
               size_time++;
            }

            if (size_time == 0)
            {
               return 0;
            }

            save = pt[size_time];
            pt[size_time] = '\0';
            option_value = atoi(pt);
            pt[size_time] = save;
            return option_value;
         }
      }
   }

   return -1;
}

virtual_link_t* virtual_links_set_flags(char* link_info)
{
   int num_link;
   int size;
   int i;
   int option_value;
   char vl_name[STR_LIMIT];

   size = strlen(link_info);
   i = 0;
   while ((i < size) && (i < (STR_LIMIT - 1)) && link_info[i] != '-')
   {
      vl_name[i] = link_info[i];
      i++;
   }
   vl_name[i] = '\0';

   num_link = virtual_link_find(vl_name);

   if (num_link == RET_ERR) return NULL;

   if ((option_value = virtual_link_find_flag_with_value(RAZ, link_info)) >= 0)
   {
      vlinks[num_link].nbwait_raz = option_value;
      vlinks[num_link].raz = 1;
   }

   if (virtual_link_find_flag(NOEXT, link_info)) vlinks[num_link].img = 0;
   else vlinks[num_link].img = 1;

   if (virtual_link_find_flag(WITHEXT, link_info)) vlinks[num_link].img = 1;
   if (virtual_link_find_flag(BLOCK, link_info)) vlinks[num_link].block = 1;
   if (virtual_link_find_flag(ACK, link_info)) vlinks[num_link].ack = 1;
   if (virtual_link_find_flag(NEXT, link_info)) vlinks[num_link].next = 1;
   if ((option_value = virtual_link_find_flag_with_value(TIMEOUT_OPT, link_info)) > 0)
   {
      vlinks[num_link].nbsec_timeout = option_value;
      vlinks[num_link].timeout = 1;
   }

   /* On indique si le thread de reception doit faire un timeout */
   if (vlinks[num_link].ack || vlinks[num_link].mode == RX) netlinks[vlinks[num_link].network_link].rx = 1;

   vlinks[num_link].error_flag = 0;

   return &vlinks[num_link];
}

int virtual_link_send_data(virtual_link_t *vlink)
{
   int ret_send;

   ret_send = network_link_send(&(netlinks[vlink->network_link]), MSG, vlink);

   return ret_send;
}

int virtual_link_send_ack(virtual_link_t *vlink)
{
   int ret_send;

   ret_send = network_link_send(&netlinks[vlink->network_link], MSG_ACK, vlink);

   return ret_send;
}

void virtual_link_init(virtual_link_t *vlink, char* name, int network_link, unsigned int mode)
{
   int retval;
   virtual_link_t *ptr = NULL;

   dprints("virtual link init %s\n", name);

   memset(vlink->name, 0, STR_LIMIT);
   if (strlen(name) < STR_LIMIT)
   {
      memset(vlink->name, 0, STR_LIMIT);
      memcpy(vlink->name, name, strlen(name) + 1);
   }
   else EXIT_ON_ERROR("Nom d'hote superieur a� %d caracteres!\n", STR_LIMIT);

   vlink->num_groupe = 0;
   vlink->network_link = network_link;

   /** registering in network struct if RX */
   if (mode == RX)
   {
      if (netlinks[network_link].vl_chaine == NULL)
      {
         netlinks[network_link].vl_chaine = vlink;
      }
      else
      {
         /** parcours chaine virtual link */
         for (ptr = netlinks[network_link].vl_chaine; ptr->vl_next != NULL; ptr = ptr->vl_next)
            ;
         /** add new virtual link */
         ptr->vl_next = vlink;
      }
   }

   vlink->mode = mode;
   vlink->raz = 0;
   vlink->nbwait_raz = 0;
   vlink->nb_iter_since_raz = 0;
   vlink->img = 0;
   vlink->block = 0;
   vlink->ack = 0;
   vlink->next = 0;
   vlink->fft = 0;
   vlink->timeout = 0;
   vlink->nbsec_timeout = 0;
   vlink->data = NULL;
   vlink->size_data = 0;
   vlink->size_max_data = 0;
   vlink->read_data = 0;
   vlink->data_ext = NULL;
   vlink->size_data_ext = 0;
   vlink->size_max_data_ext = 0;
   /** -1 is data_ext not activated */
   vlink->read_data_ext = -1;

   /* Initialisation des semaphores pour les nouveau messages et les accuse */
   retval = sem_init(&vlink->new_message, 0, 0);
   if (retval < 0) EXIT_ON_SYSTEM_ERROR("libcomm : virtual_link_init() : sem_init()");

   retval = sem_init(&vlink->wait_ack, 0, 0);
   if (retval < 0)  EXIT_ON_SYSTEM_ERROR("libcomm : virtual_link_init() : sem_init()");

   /*On block la mutex pour les donnees*/
   if ((pthread_mutex_init(&vlink->mut_data, NULL)) != 0) EXIT_ON_SYSTEM_ERROR("libcomm : virtual_link_init() : pthread_mutex_init()");
   if ((pthread_mutex_init(&vlink->mut_send, NULL)) != 0) EXIT_ON_SYSTEM_ERROR("libcomm : virtual_link_init() : pthread_mutex_init()");

#ifdef DEBUG_NETWORK
   /** initialisation structure de debug */
   debug_network_init(&vlink->debug, vlink->name);
#endif
}

void virtual_link_close(virtual_link_t *vlink)
{
   /* free le buffer de donnees */
   if (vlink->data) free(vlink->data);

   /* ferme mutex de protection des donnees */
   if (pthread_mutex_destroy(&vlink->mut_data))  EXIT_ON_SYSTEM_ERROR(" data pthread_mutex_destroy()");
   if (pthread_mutex_destroy(&vlink->mut_send))  EXIT_ON_SYSTEM_ERROR(" send pthread_mutex_destroy()");

   /** On detruit les semaphores */
   if (sem_destroy(&vlink->new_message) < 0)  EXIT_ON_SYSTEM_ERROR("new_message sem_destroy()");
   if (sem_destroy(&vlink->wait_ack) < 0)  EXIT_ON_SYSTEM_ERROR("wait_ack sem_destroy()");
}
/** Actions a effectuer sur un les liens virtuels lors d'une deconnection */
void virtual_link_disconnect(int num_netlink)
{
   int i;
   int retval;

   for (i = 0; (unsigned int) i < nb_vlink; i++)
   {
      if (vlinks[i].network_link == (unsigned int) num_netlink)
      {
         /* deblocage semaphore */
         if (vlinks[i].ack)
         {
            retval = sem_post(&vlinks[i].wait_ack);
            if (retval < 0) EXIT_ON_SYSTEM_ERROR("libcomm : msg_ack_handler_rx() : sem_post()");
         }
      }
   }
}

/** set debug server period */
void recv_server_set_debug(virtual_link_t* vlink, int debug_period)
{
#ifdef DEBUG_NETWORK
   dprints("set debug period for connection %s is %d ms\n", vlink->name, debug_period);
   if (debug_period < 0)
   {
      vlink->debug.debug_cycle = DEBUG_CYCLE * 1000;
   }
   else vlink->debug.debug_cycle = debug_period * 1000;
#else
   (void)vlink;
   (void)debug_period;
   EXIT_ON_ERROR("Try to use debug_network but it is not compiled ! define DEBUG_NETWORK in debug_network.h (libcomm) to compile debug_network");
#endif
}

/** update debug info at reading */
void recv_server_update_debug(virtual_link_t* vlink, unsigned int update_ext_debug)
{
#ifdef DEBUG_NETWORK
   dprints("update reading time stamp in %s\n", vlink->name);
   debug_network_update_at_reading(&vlink->debug, vlink->read_data);
   if (update_ext_debug)
   {
      if (vlink->read_data_ext != -1) debug_network_update_at_reading_ext(&vlink->debug, vlink->read_data_ext);
      else debug_network_update_at_reading_ext(&vlink->debug, vlink->read_data);
   }
#else
   (void)vlink;
   (void)update_ext_debug;
   (void)old_msg;
#endif
}

/** init specific ext management (debug and allocation) */
void recv_server_init_ext_management(virtual_link_t* vlink)
{
#ifdef DEBUG_NETWORK
   dprints("Init specific ext management in %s\n", vlink->name);
   vlink->read_data_ext = 0;
#else
   (void)vlink;
#endif
}

/** update try reading info */
void recv_server_update_debug_try_reading(virtual_link_t* vlink)
{
#ifdef DEBUG_NETWORK
   dprints("update try reading in %s\n", vlink->name);
   debug_network_update_at_try_reading(&vlink->debug);
#else
   (void)vlink;
#endif
}

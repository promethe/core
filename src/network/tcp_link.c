/*
 Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
 promethe@ensea.fr

 Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier,
 C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,
 M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

 See more details and updates in the file AUTHORS

 This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
 This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software.
 You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license,
 users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability.
 In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software,
 that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge.
 Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured
 and, more generally, to use and operate it in the same conditions as regards security.
 The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
 */
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include "blc.h"

/* #define DEBUG */

#include <pthread.h>
#include <netdb.h>

#include "consts.h"
#include "global.h"
#include "network_link.h"
#include "tcp_link.h"

/* Extern header */
#include "Ivy/ivy.h"

int tcp_link_connect(network_link_t *netlink)
{
   int ret_val;
   struct hostent *hp;
   int i, ret;

   netlink->socket = socket(PF_INET, SOCK_STREAM, 0);
   netlink->address.sin_family = AF_INET;
   ret = inet_aton(netlink->ip, &netlink->address.sin_addr);
   netlink->address.sin_port = htons(netlink->port);
   CLEAR(netlink->address.sin_zero);

   if (ret != 0)
   {
      /* si ip est une ip */
      ret_val = connect(netlink->socket, (struct sockaddr *) &netlink->address, sizeof(netlink->address));
   }
   else
   {
      /* si ip est un nom de domaine */
      hp = gethostbyname(netlink->ip);
      i = 0;
      ret_val = -1;

      while (ret_val == -1 && hp->h_addr_list[i] != NULL)
      {
         bcopy(hp->h_addr_list[i], &netlink->address.sin_addr, sizeof(netlink->address.sin_addr));
         ret_val = connect(netlink->socket, (struct sockaddr *) &netlink->address, sizeof(netlink->address));
         i++;
      }
   }

   if (!ret_val) /* connect reussi */
   return (RET_OK);
   else /* Connection echouee */
   return (RET_ERR);

}

void tcp_link_close(int socket)
{
   if (shutdown(socket, SHUT_RDWR) == -1)
   {
      PRINT_WARNING("libcomm : tcp_link_close() : Error shutdown");
      PRINT_SYSTEM_ERROR();
   }
   close(socket);
}

int tcp_link_recv(network_link_t *netlink)
{
   int ret_recv;
   unsigned int size;
   unsigned int sizet;
   int left_to_read;
   char *buf = NULL;
   int oct_read = 0;

   /* reception de la taille du message */
   sizet = sizeof(unsigned int);
   oct_read = 0;
   left_to_read = sizeof(unsigned int);
   do
   {
      /* reception du message */
      ret_recv = recv(netlink->socket, ((char *) (&size)) + oct_read, left_to_read, 0);
      oct_read += ret_recv;

      if (ret_recv == 0)
      {
         return (RET_DECO);
      }
      else if (ret_recv < 0)
      {
         return (RET_ERR);
      }
      else if (ret_recv != left_to_read)
      {
         left_to_read = sizet - oct_read;
      }
   } while ((unsigned int) oct_read < sizet);

   /* On allou la memoire si il s'agit du premier message pour ce lien */
   if (!netlink->message_rx)
   {
      netlink->message_rx = malloc(size);
      if (!netlink->message_rx) EXIT_ON_SYSTEM_ERROR("libcomm : tcp_link_recv() : malloc()");

      memset(netlink->message_rx, 0, size);
      netlink->size_max_msg_rx = size;
   }
   /* On verifi la taille */
   else if (size > netlink->size_max_msg_rx)
   {
      netlink->message_rx = realloc(netlink->message_rx, size);
      if (!netlink->message_rx) EXIT_ON_SYSTEM_ERROR("libcomm : tcp_link_recv() : realloc()");
      netlink->size_max_msg_rx = size;
   }

   *(unsigned int *) netlink->message_rx = size;
   buf = netlink->message_rx + sizeof(unsigned int);

   /* 	oct_read += sizeof(unsigned int); */

   left_to_read = size - oct_read;

   do
   {
      /* reception du message */
      ret_recv = recv(netlink->socket, buf, left_to_read, 0);
      oct_read += ret_recv;

      if (ret_recv == 0)
      {
         return (RET_DECO);
      }
      else if (ret_recv < 0)
      {
         return (RET_ERR);
      }
      else if (ret_recv != left_to_read)
      {
         buf = netlink->message_rx + oct_read;
         left_to_read = size - oct_read;
      }
   } while ((unsigned int) oct_read < size);

   return (RET_OK);
}

int tcp_link_send(network_link_t* netlink)
{
   int ret_send;
   int left_to_send;
   int oct_send;
   char *buf;

   if (netlink->message_tx)
   {
      buf = netlink->message_tx;
      oct_send = 0;
      left_to_send = *(unsigned int*) netlink->message_tx;

      do
      {
         /* Envoie du message */
         ret_send = send(netlink->socket, buf, left_to_send, 0);
         oct_send += ret_send;

         dprints("oct_sent : %d / %d\n", oct_send, *(unsigned int*)netlink->message_tx);

         if (ret_send == 0)
         {
            return (RET_DECO);
         }
         else if (ret_send < 0)
         {
            return (RET_ERR);
         }
         else if (ret_send != left_to_send)
         {
            buf = netlink->message_tx + oct_send;
            left_to_send -= ret_send;
         }
      } while ((unsigned int) oct_send < *(unsigned int*) netlink->message_tx);

   }
   else EXIT_ON_ERROR("libomm : tcp_link_send() : try to send a empty message\n");

   return (RET_OK);
}

void tcp_link_init_server(network_link_t *netlink)
{
   int flag;

   /* on designe l adresse + port qu'on va ouvrir */
   netlink->address.sin_family = AF_INET;
   netlink->address.sin_addr.s_addr = htonl(INADDR_ANY); /* on attend sur toutes nos adresses */
   netlink->address.sin_port = htons(netlink->port);
   memset(&netlink->address.sin_zero, 0, sizeof(netlink->address.sin_zero));

   /* creation du socket d'écoute */
   netlink->socket_server = socket(PF_INET, SOCK_STREAM, 0);
   if (netlink->socket_server == -1) EXIT_ON_SYSTEM_ERROR("Creating socket.");
   flag = 1;
   /*         setsockopt(netlink->socket_server, SOL_SOCKET, SO_REUSEADDR, &flag, sizeof(int));*/

    setsockopt(netlink->socket_server, IPPROTO_TCP, TCP_NODELAY, (char *) &flag, sizeof(flag));

   /* Parametre la socket */
   if (bind(netlink->socket_server, (struct sockaddr *) &netlink->address, sizeof(netlink->address)) == -1)
   {
      if (errno == EADDRINUSE) EXIT_ON_ERROR("Address port %d already in use. \nThe previous connection was probably badly closed. You should wait 1 minute and try again.\n\n", netlink->port);
      else EXIT_ON_SYSTEM_ERROR("Server on port %d not created. Impossible to bind the socket.", netlink->port);
   }

   /* on definit la socket s_ecoute, comme etant une socket d'ecoute */
   if (listen(netlink->socket_server, nb_netlink) == -1) EXIT_ON_SYSTEM_ERROR("libcomm : tcp_link_init_server() : listen()");

}

void tcp_link_accept(network_link_t *netlink)
{
   netlink->socket = accept(netlink->socket_server, NULL, 0);
   if (netlink->socket < 0) EXIT_ON_SYSTEM_ERROR("libcomm : tcp_link_accept() : accept()");
}


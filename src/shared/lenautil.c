/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include "net_message_debug_dist.h"
#include "structlena.h"


/*  Source : ouvrelena.c   
    -->  ouverture d'un fichier lena    */

/* cette fonction renvoit une structure image associee au 
	fichier ouvert	*/

image ouvre(char mode, descripteur * ident, char *nomfich)
/* mode ='e' : ouverture en ecriture sans verification existence de fichier
	='w' : ouverture en ecriture avec verification existence de fichier
        ='l' : ouverture en lecture            */
/*  nomfich:    nom du fichier  */
/* ident: pointeur de structure logique en tete fichier .lena        */
{
    FILE *fp, *fopen();
    image nouvimag;
    int fd;
    char *gets();
    char repond;
    /*     verification du nom d'un fichier lena  */
    if ((mode != 'l') && (mode != 'e') && (mode != 'w'))
    {
        printf("mode inexistant, reentrer le mode ?:\n ");
        if (scanf("\n%c", &mode)!=1) PRINT_WARNING("Wrong mode %c", mode);
    }
    if (mode == 'l')
    {
        if ((fd = open(nomfich, 0)) == -1)
        {
            printf("nom du fichier incorrect.\n");
            printf("reentrer le nom du fichier : ?y ou n:");
            if (scanf("\n%c", &repond)!=1) PRINT_WARNING("Wrong format");
            if (repond == 'n')
            {
                printf("sortir d'execution\n");
                exit(1);
            }
            else
            {
                printf("reentrer le nom du fichier ?:\n");
                if (scanf("%s", nomfich)!=1) PRINT_WARNING("Wrong format %s", nomfich);
            }
        }
    }
    if (mode == 'w')
    {
        if ((fd = open(nomfich, 0)) != -1)
        {
            printf("ce fichier existe deja ! \n");
            printf(" reecrire dans ce fichier y ou n?:\n");
            if (scanf("\n%c", &repond)!=1) PRINT_WARNING("Wrong format");

            if (repond == 'n')
            {
                printf("interdire d'ecrire dans ce fichier\n");
                printf("sortir d'execution\n");
                exit(1);
            }
        }
        else
            printf("c'est un nouveau fichier\n");
    }
    switch (mode)
    {
    case 'l':
        {
            fp = fopen(nomfich, "r");
            if (fscanf(fp, "%d", &(ident->nx))!=1) PRINT_WARNING("Wrong format");
            fseek(fp, 6, 0);
            if (fscanf(fp, "%d", &(ident->ny))!=1) PRINT_WARNING("Wrong format");
            fseek(fp, 12, 0);
            if (fscanf(fp, "%d", &(ident->nz))!=1) PRINT_WARNING("Wrong format");
            fseek(fp, 16, 0);
            if (fscanf(fp, "%d", &(ident->typ))!=1) PRINT_WARNING("Wrong format");
            fseek(fp, 18, 0);
            if (fscanf(fp, "%14s", ident->nature)!=1) PRINT_WARNING("Wrong format");
            fseek(fp, 32, 0);
            if (fscanf(fp, "%64s", ident->chemin)!=1) PRINT_WARNING("Wrong format");
            fseek(fp, 96, 0);
            if (fscanf(fp, "%32s", ident->libre)!=1) PRINT_WARNING("Wrong format");
            fseek(fp, 128, 0);
            if (fscanf(fp, "%128s", ident->comment)!=1) PRINT_WARNING("Wrong format");
            fseek(fp, 256, 0);
            nouvimag.p_dec = ident;
            nouvimag.p_octet = (FILE *) fp;
            return (nouvimag);
        }
    case 'e':
    case 'w':
        {
            fp = fopen(nomfich, "w");
            fprintf(fp, "%5d ", ident->nx);
            fprintf(fp, "%5d ", ident->ny);
            fprintf(fp, "%3d ", ident->nz);
            fprintf(fp, "%1d ", ident->typ);
            fseek(fp, 18, 0);
            fprintf(fp, "%-13s ", ident->nature);
            fseek(fp, 32, 0);
            fprintf(fp, "%-63s ", ident->chemin);
            fseek(fp, 96, 0);
            fprintf(fp, "%-31s ", ident->libre);
            fseek(fp, 128, 0);
            fprintf(fp, "%-127s ", ident->comment);
            fseek(fp, 256, 0);
            nouvimag.p_dec = ident;
            nouvimag.p_octet = (FILE *) fp;
            return (nouvimag);
        }
    }
    printf("erreur lenautil \n");
    exit(1);

}

/* Lecture d'un fichier Lena */
void lec(image * p_im, unsigned char *im, int nblig)
/* image *p_im pointeur de la structure logique asssociee au fichiewr .lena initialisee
   lors de l,ouverture prealable */
/* unsigned char *im : pointeur du tableau accueillant l'image */
/* int nblig: nombre de lignes a lire */
{
    int k, n_octet;

    n_octet = nblig * (p_im->p_dec->nx) * (p_im->p_dec->nz);
    for (k = 0; k < n_octet; k++)
    {
        im[k] = (unsigned char) getc((FILE *) p_im->p_octet);
    }
    fclose((FILE *) p_im->p_octet);
}


/* Ecriture d'un fichier Lena */
void ecri(image * p_im, unsigned char *im, int nblig)
/* image *p_im: pointeur de la structure logique asssociee au fichiewr .lena initialisee
   lors de l,ouverture prealable */
/* unsigned char *im: pointeur du tableau accueillant l'image */
/* int nblig: nombre de lignes a lire */
{
    int n_octet;
    n_octet = nblig * (p_im->p_dec->nx) * (p_im->p_dec->nz);
    /*      for (k=0;k<n_octet;k++) {
       putc(im[k],(FILE*)p_im->p_octet);
       }
     */
    fwrite(im, n_octet, 1, (FILE *) p_im->p_octet);
    fclose((FILE *) p_im->p_octet);
}


/*--------------------------------------------------------------*/
/*       fabrique une droite dans une image lena pour Kohonen   */
/*--------------------------------------------------------------*/

void droite(unsigned char *im2, int x1, int y1, int x2, int y2, int xmax)
{
    int i, j;
    float dx;
    if (x1 == 0 && y1 == 0)
        return;
    if (x1 > x2)
    {
        i = x2;
        x2 = x1;
        x1 = i;
        i = y2;
        y2 = y1;
        y1 = i;
    }
    dx = ((float) (y2 - y1)) / ((float) (x2 - x1));
    for (i = 1; i < x2 - x1; i++)
    {
        j = y1 + (int) (dx * ((float) i));
        if (j > xmax)
            j = xmax;
        else if (j < 0)
            j = 0;
        im2[x1 + i + j * xmax] = 128;
    }
}

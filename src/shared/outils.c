/*
 Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
 promethe@ensea.fr

 Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier,
 C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,
 M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

 See more details and updates in the file AUTHORS

 This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
 This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software.
 You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license,
 users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability.
 In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software,
 that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge.
 Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured
 and, more generally, to use and operate it in the same conditions as regards security.
 The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
 */
/*#define DEBUG 1*/
#include "public.h"
#include <signal.h>
#include <string.h>

#include "net_message_debug_dist.h"

/*---------------------------------------------------------------*/
/*      CREATION DU TABLEAU CONTENANT LE RESEAU                  */
/*---------------------------------------------------------------*/

type_tableau creer_reseau(int n) /*liste des neurones              */
{
   int a;
   type_tableau t2;
   a = n + 2;
   t2 = (type_neurone *) calloc(a, sizeof(type_neurone));
   if (t2 == NULL) printf("\n\n ERREUR : le Calloc a echoue ?!?.\n\n\n");
   return (t2);
}

type_tableau_pando creer_reseau_para_supp_pando(int n) /*liste des neurones              */
{
   int a;
   type_tableau_pando t2;
   a = n + 2;
   t2 = (type_neurone_pando *) calloc(a, sizeof(type_neurone_pando));
   if (t2 == NULL) printf("\n\n ERREUR : le Calloc a echoue ?!?.\n\n\n");
   return (t2);
}

type_tableau_voies creer_voies(int n) /*liste des liens indexee          */
{
   int a;
   type_tableau_voies t2;
   a = n + 2;
   t2 = (type_liaison *) calloc(a, sizeof(type_liaison));
   if (t2 == NULL) printf("\n\n ERREUR : le Calloc a echoue ?!?.\n\n\n");
   return (t2);
}

/*--------------------------------------------------------------*/

type_noeud **creer_groupe(int n) /*liste neurones du groupe  */
{
   int i;
   type_noeud **t2;
   t2 = (type_noeud **) calloc(n, 4);
   for (i = 0; i < n; i++)
      t2[i] = (type_noeud *) 0;
   return (t2);
}

/*--------------------------------------------------------------*/
/*      reservation de la place pour 1 coefficient synaptique   */
/*--------------------------------------------------------------*/

type_coeff *creer_coeff(void)
{
   type_coeff *t2;
   t2 = (type_coeff *) calloc(1, sizeof(type_coeff));
   if (t2 == 0)
   {
      EXIT_ON_ERROR("Probl��me d'allocation memoire dans creer_noeud \n");
   }
   t2->s = NULL;

   return (t2);
}

/*--------------------------------------------------------------*/
/*     reservation de la place pour 1 neurone d'un groupe        */
/*--------------------------------------------------------------*/

type_noeud *creer_noeud(void)
{
   type_noeud *t2;
   t2 = (type_noeud *) calloc(1, sizeof(type_noeud));
   if (t2 == 0)
   {
      EXIT_ON_ERROR("ERREUR d'allocation memoire dans creer_noeud \n");
   }
   t2->s = NULL;
   return (t2);
}

/*---------------------------------------------------------------*/
/*             CREATION DU TABLEAU CONTENANT LES DONNEES         */
/*---------------------------------------------------------------*/

type_matrice creer_matrice(int n, int m)
/*n: nbre de lignes , m: nbre de colonnes */
{
   int i, a, b;
   type_matrice t2;
   a = m + 2;
   b = n + 2;
   t2 = (float **) calloc(b, sizeof(float *));
   for (i = 0; i < n; i++)
      t2[i] = (float *) calloc(a, sizeof(float));

   return (t2);
}

/*-----------------------------------------------------------------*/
/*                CREATION VECTEUR                                 */
/*-----------------------------------------------------------------*/

type_vecteur_entier creer_vecteur(int n)
{
   int i, a;
   type_vecteur_entier t2;
   a = n + 2;
   t2 = (int *) calloc(a, sizeof(int));
   for (i = 0; i < a; i++)
      t2[i] = 0;
   return (t2);
}

/*-------------------------------------------------------------*/
/*   Allocation de la memoire pour la creation des noeuds      */
/*-------------------------------------------------------------*/

type_groupe *creer_groupeb(type_groupe * prec)
{
   type_groupe *nouv;
   nouv = (type_groupe *) calloc(1, sizeof(type_groupe));
   if (prec != (type_groupe *) 0) prec->s = nouv;
   nouv->s = (type_groupe *) 0;
   nouv->seuil = 0.;
   nouv->reverse = 0;
   strcpy(nouv->nom, "???");
   return (nouv);
}

/*-------------------------------------------------------------*/

type_liaison *creer_liaison(type_liaison * prec)
{
   type_liaison *nouv;
   nouv = (type_liaison *) calloc(1, sizeof(type_liaison));
   if (prec != (type_liaison *) 0) prec->s = nouv;
   nouv->s = (type_liaison *) 0;
   return (nouv);
}

/*--------------------------------------------------------------*/

int trouver_entree(int gpe, const char *s)
{
   int i;

   for (i = 0; i < nbre_liaison; i++)
   {
      if (liaison[i].arrivee == gpe) if (strcmp(liaison[i].nom, s) == 0 || strcmp(s, "") == 0) return (liaison[i].depart);
   }
   if (strcmp(s, "") == 0)
   {
      printf(" Erreur :\n");
      printf(" aucune entree n'a ete trouvee pour le groupe %d \n", gpe);
   }
   else
   {
      printf(" Attention :\n");
      printf(" la liaison %s n'a pas ete trouvee pour le gpe %d\n", s, gpe);
   }
   return (-1);
}

/*---------------------------------------------------------------*/

int trouver_nbre_entree(int gpe)
{
   int nbre = 0;
   int i;

   for (i = 0; i < nbre_liaison; i++)
   {
      if (liaison[i].arrivee == gpe) nbre++;
   }
   return (nbre);
}

/* ce tableau est reinitialise a chaque lecture du script */

void init_input_link_matrix(void)
{
   int i, j;

   for (i = 0; i <= nbre_groupe; i++) /* penser au pseudo groupe pour gerer les micro neurones */
   {
      for (j = 0; j < nb_max_liaisons_pour_un_groupe; j++)
      {
         input_link_number[i][j] = -2;
      }
   }
}

/**
 \defgroup find_input_link find_input_link
 \ingroup group_tools
 \brief recuperation d'un connecte au groupe considere

 \details
 cherche dans une table le numero du xeme groupe de lien
 connecte en entree du groupe considere si c'est la premiere fois que
 cette requete est lance alors la fonction recherche et enregistre tous
 les liens du groupe de maniere a accelerer les recherches suivantes.
 retourne -1 s'il n'y a pas de numero de lien correspondant

 \file
 \ingroup find_input_link
 **/

int find_input_link(int group, int link_number)
{
   int val, nb_inputs, i;

   dprints("group=%d (%s) link_number=%d \n", group, def_groupe[group].no_name, link_number);
   if (link_number > nb_max_liaisons_pour_un_groupe)
   EXIT_ON_ERROR("find_input_link: group %d, link_number %d < %d \n", group, link_number, nb_max_liaisons_pour_un_groupe);

   val = input_link_number[group][link_number];
   if (val != -2)
   {
      dprints("group=%d (%s) link_number=%d link=%d\n", group, def_groupe[group].no_name, link_number, val);
      dprints("depart = %s, arrivee = %s(%d) \n", def_groupe[liaison[val].depart].no_name, def_groupe[liaison[val].arrivee].no_name);
      return val;
   }
   /* recherche pour la premiere fois la liaison correspondante */
   nb_inputs = 0;
   for (i = 0; i < nbre_liaison; i++)
   {
      dprints("liaison[%d].arrivee=%d \n", i, liaison[i].arrivee);
      if (liaison[i].arrivee == group)
      {
         dprints("OK\n");
         input_link_number[group][nb_inputs] = i;
         nb_inputs = nb_inputs + 1;
      }
   }
   for (i = nb_inputs; i < nbre_liaison + 1; i++)
      input_link_number[group][i /*bug !!! nb_inputs*/] = -1;

   return input_link_number[group][link_number];

}

/**
 \defgroup find_input_link_by_name find_input_link_by_name
 \ingroup group_tools

 cherche dans une table le numero du xeme groupe de lien connecte en entree du groupe considere
 si c'est la premiere fois que cette requete est lance alors la fonction recherche et enregistre
 tous les liens du groupe de maniere a accelerer les recherches suivantes.
 retourne -1 s'il n'y a pas de numero de lien correspondant

 \file
 \ingroup find_input_link_by_name
 **/

int find_input_link_by_name(int gpe, char *name)
{
   int i;
   for (i = 0; i < nbre_liaison; i++)
   {
      if (liaison[i].arrivee == gpe) if (strcmp(liaison[i].nom, name) == 0 || strcmp(name, "") == 0) return (i);
   }
   if (strcmp(name, "") == 0)
   {
      printf(" Erreur :\n");
      printf(" aucune entree n'a ete trouvee pour le groupe %d \n", gpe);
   }
   else
   {
      printf(" Attention :\n");
      printf(" la liaison %s n'a pas ete trouvee pour le gpe %d\n", name, gpe);
   }
   return (-1);
}

/**
 \defgroup prom_get_opt prom_get_opt
 \ingroup group_tools
 \brief
 donnez la chaine de parametres d'un lien, et le char de l'option et prom_get_opt vous renvoie la valeur de l'option
 \details
 \section Parametres
 args : designe la chaine de caractere du lien a analiser
 opt : le charactere designat l'option (exemple : "f" ou "-f")
 retour : est pointeur sur une chaine de 256 characteres qui contiendra la valeur de l'option
 elle recopiee depuis args en s'arretant au prochain espace, tiret ou fin de chaine
 en rajoutant un '\\0' a la fin de retour
 valeur renvoyee :
 0 si opt n'est pas trouve dans args
 1 si opt est dans args mais suivi d'un espace, d'une fin de chaine ou d'un tiret
 2 si opt est suivi d'une chaine de caractere
 exemples
 args = "-fnom_fichier.txt-g7 -k"
 opt=f
 => retour = "nom_fichier.txt"
 valeur renvoyee = 2
 opt=g
 => retour = "7"
 valeur renvoyee = 2
 opt=k
 => retour = "" ('\\0')
 valeur renvoyee = 1
 opt=z
 => retour = "" ('\\0')
 valeur renvoyee = 0

 \section   Remarques
 * ne gere pas les options longues
 * ne doit pas etre appelee a toutes les exec de la fonction appelante
 * ne gere pas les options repetees : -vv = -v-v = 2 fois l'option v :(
 ni -rf = -r-f
 * necessite string.h
 * supporte les threads !!!!

 JC BACCON, Octobre 2002

 \file
 \ingroup prom_get_opt
 **/
//
//int prom_getopt(const char *args, const char *opt, char *retour)
//{
//  /* on en peut pas utiliser un pointeur static
//   puisque dans le cas des threads on peut avoir
//   plusieurs appels simultanes a cette fonction */
//  char *souschaine;
//  char option[32];
//  int i, len;
//
//  /* on regarde si opt designe 'option' ou '-option' */
//  if (opt[0] == '-')
//  {
//    /* okay on accepte cette formulation */
//    strcpy(option, opt);
//  }
//  else
//  {
//    /* la formulation preferee */
//    option[0] = '-';
//    option[1] = '\0';
//    strcat(option, opt);
//  }
//  /* recherche de la souschaine */
//  souschaine = strstr(args, option);
//  /* on recopie vers la chaine du pointeur retour, si necessaire */
//  if (souschaine == NULL)
//  {
//    dprints("(%s) : recherche '%s' dans '%s' rien trouve \n", option, args);
//    if (retour != NULL) retour[0] = '\0';
//    return 0;
//  }
//  else
//  {
//    len = strlen(option);
//    i = 0;
//    while (souschaine[i + len] != ' ' && souschaine[i + len] != '\0' && souschaine[i + len] != '-' && i < (PARAM_MAX - len))
//    {
//      if (retour == NULL) EXIT_ON_ERROR("The option %s has an argument but it was not expected. The retour pointer is NULL", option);
//      retour[i] = souschaine[i + len]; /* ecrit les arguments de l'option dans retour */
//      i++;
//    }
//    if (i == 0)
//    {
//      dprints("recherche '%s' dans '%s' trouve mais sans valeur\n", option, args);
//      if (retour != NULL) retour[0] = '\0';
//      return 1;
//    }
//    else
//    {
//      retour[i] = '\0';
//      dprints("(%s) : recherche '%s' dans '%s' trouve : '%s'\n", option, args, retour);
//      return 2;
//    }
//  }
//}
//
///* Parse args pour trouver option et converti en int si possible.
// *
// * Return 0 s'il n'est pas trouv��, 1 si la conversion pause probl��me et 2 si tout se passe bien.
// *
// */
//int prom_getopt_int(const char *args, const char *option, int *value)
//{
//  char const *current_addr;
//
//  for (current_addr = strstr(args, option); current_addr != NULL;  current_addr = strstr(current_addr, option))
//  {
//     current_addr += strlen(option);
//     if (sscanf(current_addr, "%d", value) == 1) return 2; // This is followed by a number
//     if (current_addr[0]  == '-')  return 1;
//  }
//  return 0;
//}
//
///* Parse args pour trouver option et converti en float si possible.
// *
// * Return 0 s'il n'est pas trouv��, 1 si la conversion pause probl��me et 2 si tout se passe bien.
// *
// */
//int prom_getopt_float(const char *args, const char *option, float *value)
//{
//  char const *current_addr;
//
//  for (current_addr = strstr(args, option); current_addr != NULL;  current_addr = strstr(current_addr, option))
//  {
//     current_addr += strlen(option);
//     if (sscanf(current_addr, "%f", value) == 1) return 2; // This is followed by a number nor a new argument.
//     if (current_addr[0] == '-')  return 1;
//  }
//  return 0;
//}

static int get_parameter_default_value(type_link_option *link_option, void const **result_pt)
{
   switch (link_option->type)
   {
   case OPTIONAL_INT:
      *(int*) result_pt = link_option->default_value.integer;
      break;
   case OPTIONAL_FLOAT:
      *(float*) result_pt = link_option->default_value.real;
      break;
   case OPTIONAL_STRING:
      *result_pt = link_option->default_value.string;
      break;
   case FLAG:
      *(int*) result_pt = link_option->default_value.integer;
      break;
   default:
      return 0;
   }
   return 1;
}

static int get_parameter_on_link(int gpe, type_liaison *link, type_link_option *link_option, void **result_pt)
{
   int ret = 0;
   char full_option_name[NAME_MAX + 1]; // Option_name preceded by '-'
   char tmp_result[PARAM_MAX+1];

   SPRINTF(full_option_name, "-%s", link_option->name);

   switch (link_option->type)
   {
   case OPTIONAL_INT:
   case REQUIRED_INT:
      ret = prom_getopt_int(link->nom, full_option_name, (int*) result_pt);
      if (ret == 1) EXIT_ON_GROUP_ERROR(gpe, "Missing int with option %s.\n\tThis option is for : %s", link_option->name, link_option->help);
      break;
   case OPTIONAL_FLOAT:
   case REQUIRED_FLOAT:
      ret = prom_getopt_float(link->nom, full_option_name, (float*) result_pt);
      if (ret == 1) EXIT_ON_GROUP_ERROR(gpe, "Missing float with option %s.\n\tThis option is for : %s", link_option->name, link_option->help);
      break;
   case OPTIONAL_STRING:
   case REQUIRED_STRING:
      ret = prom_getopt(link->nom, full_option_name, tmp_result);
      if (ret == 1) EXIT_ON_GROUP_ERROR(gpe, "Missing string with option %s.\n\tThis option is for : %s", link_option->name, link_option->help);
      *result_pt = strdup(tmp_result);
      break;
   case OPTIONAL_INPUT_GROUP:
   case REQUIRED_INPUT_GROUP:
      if (strncmp(link_option->name, link->nom, strlen(link_option->name)) == 0) //Comparison with the beginning of the link. The only probleme may be if one link name as the same name that the beginning of another one
      {
         *(type_groupe**) result_pt = &def_groupe[link->depart];
         ret = 2; // We found it
      }
      break;
   case FLAG: // Flag is always optional otherwise meaningless
      ret = prom_getopt(link->nom, full_option_name, NULL);
      if (ret == 1) *(int*) result_pt = 1;
      else if (ret == 2) EXIT_ON_GROUP_ERROR(gpe, "Option %s is flag it is not suppose to be followed by anything which is not another parameter.\n\tThis flag is for: %s", link_option->name, link_option->help);
      break;
   }
   return ret;
}




void get_parameters_on_link(int gpe, type_liaison *link, type_link_option *link_options, char const *option_name, void *result_pt, ...)
{
   int ret, input_group_used = 0;
   type_link_option* link_option;
   va_list arguments;

   va_start(arguments, result_pt);
   while (option_name != NULL)
   {
      //We look for the same option in the array of options
      for (link_option = link_options; link_option->name != NULL; link_option = &link_option[1])
         if (strcmp(option_name, link_option->name) == 0) break;

      if (link_option->name == NULL) EXIT_ON_GROUP_ERROR(gpe, "%s is not defined as a link option for this function", option_name);
      ret = get_parameter_on_link(gpe, link, link_option, result_pt);

      if (ret == 0)
      {
         if (link_option->type == REQUIRED_INPUT_GROUP)
         {
            if (input_group_used) EXIT_ON_GROUP_ERROR(gpe, "You cannot define more than one input group on one link");
            *(type_groupe**)result_pt = &def_groupe[link->depart];
            input_group_used = 1;
         }
         else  get_parameter_default_value(link_option, result_pt);
      }

      option_name = va_arg(arguments, char*);
      result_pt = va_arg(arguments, void*);
   }
   va_end(arguments);
}


void get_parameters(int gpe, type_link_option *link_options, char const* option_name, void *result_pt, ...)
{
   int link_id, ret, links_nb, unknowned_required_input_groups_nb = 0;
   va_list arguments;
   type_link_option* link_option;
   type_liaison *link = NULL;
   type_groupe *group;

   va_start(arguments, result_pt);

   while (option_name != NULL)
   {
      //We look for the same option in the array of options
      for (link_option = link_options; link_option->name != NULL; link_option = &link_option[1])
         if (strcmp(option_name, link_option->name) == 0) break;

      if (link_option->name == NULL) EXIT_ON_GROUP_ERROR(gpe, "%s is not defined as a link option for this function", option_name);

      ret=0;

      for (links_nb = 0; (link_id = find_input_link(gpe, links_nb)) >= 0; links_nb++)
      {
         ret |= get_parameter_on_link(gpe, &liaison[link_id], link_option, result_pt);
      }

      if (ret == 0)
      {
         ret = get_parameter_default_value(link_option, result_pt);
         if (ret == 0)
         {
            if ((link_option->type == REQUIRED_INT) || (link_option->type == REQUIRED_FLOAT) || (link_option->type == REQUIRED_STRING))
               EXIT_ON_GROUP_ERROR(gpe, "Missing required option %s.\n\tThis option is for : %s", option_name, link_option->help);
            else if ((link_option->type == REQUIRED_INPUT_GROUP) && (links_nb == 1)) // We try to link by default
            {
               if (unknowned_required_input_groups_nb == 0) // if it is first link required
               {
                  group = &def_groupe[link->depart];
                  unknowned_required_input_groups_nb++;
                  *(type_groupe**) result_pt = group;
               }
               else   EXIT_ON_ERROR("You have more than one unnamed required input groups.");
            }
         }
      }
      option_name = va_arg(arguments, char*);
      result_pt = va_arg(arguments, void*);
   }
   va_end(arguments);
}

/**gestionnaire de signaux : masque blocant dans les threads*/
/*cf: promethe3.c pour le gestionnaire lui meme*/
void gestion_mask_signaux()
{
   sigset_t masque;
   sigemptyset(&masque);
   if (sigaddset(&masque, SIGUSR1) == -1)
   EXIT_ON_ERROR("Impossible de bloquer le signal SIGUSR1...\n");
   if (sigaddset(&masque, SIGCHLD) == -1)
   EXIT_ON_ERROR("Impossible de bloquer le signal SIGCHLD...\n");
   if (sigaddset(&masque, SIGINT) == -1)
   EXIT_ON_ERROR("Impossible de bloquer le signal SIGINT...\n");

   /*    if (sigaddset(&masque, SIGSEGV) == -1)
    {
    fprintf(stderr, "impossible de bloquer le signal SIGSEV...\n");
    exit(1);
    }*/
   if (pthread_sigmask(SIG_BLOCK, &masque, NULL) != 0)
   EXIT_ON_ERROR("Impossible d'imposer le mask des signaux au thread...\n");

}

/**
 * Envoie un message d'erreur avec name_of_file, name_of_function, number_of_line et affiche le message formate avec les parametres variables.
 * Puis exit le programme avec le parametre EXIT_FAILURE.
 */
void group_fatal_error(int group_id, const char *name_of_file, const char* name_of_function, int numero_of_line, const char *message, ...)
{
   va_list arguments;
   va_start(arguments, message);
   kprints("\n\033[1;31m %s \t %s \t %i \t group `%s`:\n \t Error: ", name_of_file, name_of_function, numero_of_line, def_groupe[group_id].no_name);
   vkprints(message, arguments);
   kprints("\033[0m\n\n");
   va_end(arguments);
   fflush(NULL);
   raise(SIGABRT);
   exit(EXIT_FAILURE);
}


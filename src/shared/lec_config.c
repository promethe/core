/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/*--------------------------------------------------------------------*/
/*  lecture du fichier de configuration , constantes application      */
/* taille_standart_trans = constante representant la fenetre d'analyse*/
/* nbre_max_rotation     = nbre de boucle de rot a tester = taille RN */
/* vigilence             = parametre multiplicatif pour learn,retour..*/
/* periode               = nbre de boucle iterations RN               */
/* eps                   = pas d'iteration apprentissage              */
/* dvn,dvp               = distance voisinage Kohonen                 */
/* taille_masque         = taille difusion pour extraction pts caract */
/* cste_gauss            = valeur du parametre de diffusion           */
/* rapport_echelle_pt_carac = ?                                       */
/* taille_fenetrex       = masque pour extraction de contours         */
/* distance_carac        = distance prise en compte des contours im8  */
/* alpha_contour         = parametre extraction de contours Grossberg */
/*--------------------------------------------------------------------*/

#include "public.h"
#include "outils_script.h"

void lecture_config(char *local_nomfich)
{
    static char ligne[TAILLE_CHAINE];
    type_noeud_comment * first_comment;
    FILE *f;
    int n, i,nbre;

    first_comment=NULL;
    f = fopen(local_nomfich, "r");
    if (f == NULL)
    {
        printf("ERREUR : Le fichier de config du systeme n'a pas ete defini \n");
        return;
    }

    if(max_ech_temps>=nb_max_echelles)
      {
	printf("ERROR lec_config2.c max_ech_temps %d >=nb_max_echelles %d \n",max_ech_temps,nb_max_echelles);
	exit(1);
      }

    for (i = 0; i <= max_ech_temps; i++)
    {
      echelle_temps[i]=0;
 /*     echelle_temps_dynamique[i] = 0; 
      echelle_temps_rt[i]=0; */
    }

    while(feof(f)==0)
      {

	first_comment = read_line_with_comment(f, first_comment, ligne);
	if (recherche_champs(ligne, "echelle temps") != NULL)
	  {
	    sscanf(ligne,"echelle temps %d = %d \n", &n,&nbre);
	    printf("echelle %d = %d \n", n,nbre);
	    echelle_temps[n]=nbre;
	  }

	if (recherche_champs(ligne, "vigilence") != NULL)
	  {    sscanf(ligne, "vigilence = %f \n", &vigilence);}

	if (recherche_champs(ligne, "periode") != NULL)
	  {    sscanf(ligne, "periode = %f \n", &periode);}
	
	if (recherche_champs(ligne, "eps") != NULL)
	  {    sscanf(ligne, "eps = %f \n", &eps);}

	if (recherche_champs(ligne, "apprentissage") != NULL)
	  {    sscanf(ligne, "echelle de temps pour l'apprentissage  = %d \n",
		      &echelle_temps_learn);}

	if (recherche_champs(ligne, "debug") != NULL)
	  {    sscanf(ligne, "echelle de temps pour afficher le debug  = %d \n",
		      &echelle_temps_debug);}

	if (recherche_champs(ligne, "resolution") != NULL)
	  {    sscanf(ligne,"resolution image pour le point de focalisation = %d\n",
		  &taille_standart_trans);}
      }


    if(fclose(f)==EOF)
    {
      perror("Erreur de fermeture du fichier de config  ");
    }

    for (i = max_ech_temps ; i >= 0; i--)
      {
        if(i == max_ech_temps)
          {
            if(echelle_temps[i]==0) EXIT_ON_ERROR("Erreur dans le .config : L'echelle de temps la plus grande s'execute 0 fois (donc l'ensemble du script egalement)\n Ceci peut arriver si une boite a pour paramettre echelle_de_temps une valeur superieure à celles definie dans le .config ou si la valeur est parametré à 0 dans le .config");
          }
        else
        {
          if(echelle_temps[i]==0) PRINT_WARNING("Attention, dans le .config une echelle de temps intermediaire est definie à 0 ou pas definie, ceci peut etre une erreur et aboutir à la fermeture du script !\n");
        }
      }




}

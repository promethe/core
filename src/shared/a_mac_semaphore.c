/**************************************80**************************************/
/* Use tab width = 5, indent level width = 5, and a fixed-font in a >= 80 
 * character wide window to view this file.
 */

/**
 * @file
 *
 * This file includes funtions that emulate POSIX semaphores on a Mac OS X
 * system using the "native" Carbon semaphores. 
 * Named semaphores are currently not implemented (i.e. sem_open() and 
 * sem_close() always fails with errno set to ENOSYS).
 */

/*
 * Created by Daniel Aarno on Mon Dec 15 2003.
 * Copyright (c) 2003 by Daniel Aarno - <macbishop@users.sf.net>
 * Electrical Engineer * http://www.e.kth.se/~e98_aar
 * 
 * Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
 * promethe@ensea.fr

 * Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
 * C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
 * M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

 * See more details and updates in the file AUTHORS 

 * This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
 * This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
 * You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
 *  users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
 * In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
 * that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
 * Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
 * and, more generally, to use and operate it in the same conditions as regards security. 
 * The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/

#ifdef Darwin

#warning "Compile semaphore library for MACOS - Darwin "

#include "semaphore.h"
#include "Carbon/Carbon.h"

/*#define DEBUG*/
#ifndef MAX_SEMAPHORES
#define MAX_SEMAPHORES 1024

MPSemaphoreID *sem_global_mac=NULL; /* important for a thread safe library ! */
 

typedef struct {
     MPSemaphoreID sem;
     int value;
} sememu_t;
#endif

#ifdef DYNAMIC_SEMAPHORES

#error Dynamic samaphores are not implemented

#else

static sememu_t semaphores[MAX_SEMAPHORES];
static unsigned char used_semaphores[MAX_SEMAPHORES] = {0};
#define GetSemaphorePtr(idx) &(semaphores[(idx)])
#define SemaphoreIsAvail(idx) !(used_semaphores[(idx)])
#define SemaphoreSetAvail(idx, b) used_semaphores[(idx)] = !(b)
#define SemaphoreValid(sem) !((sem) < 0 || (sem) > MAX_SEMAPHORES || \
               SemaphoreIsAvail((sem)))
               
#endif /*DYNAMIC_SEMAPHORES*/


/**
 * Set errno to the closes equivalent of an OSStatus.
 */
void OSStatus2Errno(OSStatus err)
{
     switch(err) {
          case noErr:
               errno = 0;
          break;
          
          case paramErr:
          case kMPInvalidIDErr:
               errno = EINVAL;
          break;
          
          case memFullErr:
               errno = ENOMEM;
          break;
          
          case kMPTimeoutErr:
               errno = ETIMEDOUT;
          break;
          
          case kMPInsufficientResourcesErr:
               errno = ENOSPC;
          break;
          
          default:
               errno = ENOSYS;
          break;
     }
}

/**
 * sem_init initializes the semaphore object  pointed  to  by
 * sem. The count associated with the semaphore is set ini-
 * tially to value.  The pshared argument  indicates  whether
 * the semaphore is local to the current process ( pshared is
 * zero) or is to  be  shared  between  several  processes  (
 * pshared is not zero). Sememu currently does not sup-
 * port  process-shared  semaphores,  thus  sem_init   always
 * returns with error ENOSYS if pshared is not zero.
 *
 * sem_init can set errno to one of the following errors:
 *
 * [EINVAL] - The value argument exceeds SEM_VALUE_MAX.
 *
 * [ENOSPC] - A resource required to initialize the semaphore has been
 * exhausted, or the limit on semaphores (MAX_SEMAPHORES) has been reached.
 *
 * @param sem A pointer to the semaphore object to initialize.
 * @param local Should always be 0!
 * @param value The initial value of the semaphore (usually 1).
 *
 * @return 0 upon successful completion, -1 otherwise. If -1 is returned
 * errno is set to the appropriate error.
 */
int sem_init(sem_t *sem, int local, unsigned int value)
{
     OSStatus err_mac_sem;
     int i;
     sememu_t *new_sem;
     int err;

#ifdef DEBUG	
	printf("MAC new sem_init() \n");
#endif

     if(sem_global_mac==NULL)
       {
         sem_global_mac=(MPSemaphoreID*) malloc(sizeof(MPSemaphoreID));
       /*  printf("Creation du semaphore global pour sem_init et init a 1\n");*/
         err_mac_sem = MPCreateSemaphore(SEM_VALUE_MAX, 1, sem_global_mac);
     
         if(err_mac_sem) 
         {
            OSStatus2Errno(err_mac_sem);
            printf("une erreur creation semaphore dans sem_init debut\n");
            return -1;
         } 
       }   
 

   /*printf("Try to take the global semaphore global (sem_wait for the sem_init)\n"); */   
  
   err_mac_sem = MPWaitOnSemaphore(*sem_global_mac, kDurationForever);
  /*   printf("OK prise (decremente) pour semaphore global (attente sem_wait pour le sem_init)\n");  */  
    
     if(err_mac_sem == kMPTimeoutErr) {
                         printf("error MPWaitOnSemaphore in sem_init timeout\n");
          errno = EAGAIN;
          return -1;
     }
     
     if(err_mac_sem) {
          OSStatus2Errno(err_mac_sem);
          printf("error MPWaitOnSemaphore in sem_init (other error)\n");
          return -1;
     }
        
	/*printf("MAC begining of sem_init() core \n");*/
     
     if(value > SEM_VALUE_MAX) {
          errno = EINVAL;
          return -1;
     }
     
     for(i = 0; i < MAX_SEMAPHORES; i++) {
          if(SemaphoreIsAvail(i))
               break;
     }
     
     if(i >= MAX_SEMAPHORES) {
          errno = ENOSPC;
          return -1;
     }
     
    /* printf("The semaphore number is %d \n",i);*/
     new_sem = GetSemaphorePtr(i);
     
     err = MPCreateSemaphore(SEM_VALUE_MAX, value, &(new_sem->sem));
     
     if(err) {
          OSStatus2Errno(err);
          return -1;
     }
     
     SemaphoreSetAvail(i, FALSE);
     new_sem->value = value;
     *sem = i;
  
  
 /*  printf("end of sem_init, the global semaphore is released \n");*/
     err_mac_sem = MPSignalSemaphore(*sem_global_mac);
     
     if(err_mac_sem) {
          printf("An error MPSignalSemaphore in sem_init (end)\n");       
          OSStatus2Errno(err_mac_sem);
          return -1;
     }  

     
     return 0;
}

/**
 * sem_destroy  destroys  a  semaphore  object,  freeing  the
 * resources  it  might hold. No threads should be waiting on
 * the semaphore at the time sem_destroy is  called.
 *
 * sem_destroy can set errno to one of the following errors:
 *
 * [EINVAL] - The sem argument is not a valid semaphore.
 *
 * [EBUSY] - There are currently processes blocked on the semaphore.
 *
 * @param sem A pointer to the semaphore object to destroy.
 *
 * @return 0 upon successful completion, -1 otherwise. If -1 is returned
 * errno is set to the appropriate error.
 */
int sem_destroy(sem_t *sem)
{
     OSStatus err;
     sememu_t *mac_sem;
     
     if(!SemaphoreValid(*sem)) {
          errno = EINVAL;
          return -1;
     }
     
     mac_sem = GetSemaphorePtr(*sem);

/*TODO: Maybe check value for bussy?  */

     err = MPDeleteSemaphore(mac_sem->sem);
     
     if(err) {
          OSStatus2Errno(err);
          return -1;
     }
     
     SemaphoreSetAvail(*sem, TRUE);
     
     return 0;
}

/**
 * sem_wait  suspends  the calling thread until the semaphore
 * pointed to by sem has non-zero count. It  then  atomically
 * decreases the semaphore count.
 *
 * sem_wait can set errno to one of the following errors:
 *
 * [EINVAL] - The sem argument is not a valid semaphore.
 *
 * @param sem A pointer to the semaphore object to wait on.
 *
 * @return 0 upon successful completion, -1 otherwise. If -1 is returned
 * errno is set to the appropriate error.
 */
int sem_wait(sem_t *sem)
{
     OSStatus err;
     sememu_t *mac_sem;
#ifdef DEBUG	
	printf("MAC On est dans le nouveau sem_wait() \n");
#endif

	
     if(!SemaphoreValid(*sem)) {
          errno = EINVAL;
          return -1;
     }
     
     mac_sem = GetSemaphorePtr(*sem);
     
     err = MPWaitOnSemaphore(mac_sem->sem, kDurationForever);
     
     if(err == kMPTimeoutErr) {
          errno = EAGAIN;
          return -1;
     }
     
     if(err) {
          OSStatus2Errno(err);
          return -1;
     }
     
     return 0;
}

int sem_timedwait(sem_t *sem,
       const struct timespec *abs_timeout)
{
    OSStatus err;
    int duration; /* 32-bit millisecond timer for drivers*/
     sememu_t *mac_sem;
#ifdef DEBUG	
	printf("MAC On est dans le nouveau sem_timedwait() \n");
#endif

	duration=(int) (abs_timeout->tv_nsec/1000/1000)+abs_timeout->tv_sec*1000;
    
     if(!SemaphoreValid(*sem)) {
          errno = EINVAL;
          return -1;
     }
     
     mac_sem = GetSemaphorePtr(*sem);
     
     err = MPWaitOnSemaphore(mac_sem->sem, duration);
     
     if(err == kMPTimeoutErr) {
          errno = EAGAIN;
          return -1;
     }
     
     if(err) {
          OSStatus2Errno(err);
          return -1;
     }
     
     return 0;   
}
/**
 * sem_trywait is a non-blocking variant of sem_wait.  If the
 * semaphore pointed to by sem has non-zero count, the  count
 * is   atomically   decreased  and  sem_trywait  immediately
 * returns 0.  If the semaphore count  is  zero,  sem_trywait
 * immediately returns with error EAGAIN.
 * 
 * sem_trywait can set errno to one of the following errors:
 *
 * [EINVAL] - The sem argument is not a valid semaphore.
 *
 * [EAGAIN] - The semaphore was already locked, so it cannot be immediately 
 * locked by the sem_trywait() operation.
 *
 * @param sem A pointer to the semaphore object to wait on.
 *
 * @return 0 upon successful completion, -1 otherwise. If -1 is returned
 * errno is set to the appropriate error.
 */
int sem_trywait(sem_t *sem)
{
     OSStatus err;
     sememu_t *mac_sem;
     
     if(!SemaphoreValid(*sem)) {
          errno = EINVAL;
          return -1;
     }
     
     mac_sem = GetSemaphorePtr(*sem);
     
     err = MPWaitOnSemaphore(mac_sem->sem, kDurationImmediate);
     
     if(err == kMPTimeoutErr) {
          errno = EAGAIN;
          return -1;
     }
     
     if(err) {
          errno = ENOSYS;
          return -1;
     }
     
     return 0;
}

/**
 * sem_post  atomically  increases the count of the semaphore
 * pointed to by sem.  This function  never  blocks  and  can
 * safely be used in asynchronous signal handlers.
 *
 * sem_post can set errno to one of the following errors:
 *
 * [EINVAL] - The sem argument is not a valid semaphore.
 *
 * @param sem A pointer to the semaphore object to unlock.
 *
 * @return 0 upon successful completion, -1 otherwise. If -1 is returned
 * errno is set to the appropriate error.
 */
int sem_post(sem_t *sem)
{
     OSStatus err;
     sememu_t *mac_sem;
	
     if(!SemaphoreValid(*sem)) {
          errno = EINVAL;
          return -1;
     }
     
     mac_sem = GetSemaphorePtr(*sem);

     err = MPSignalSemaphore(mac_sem->sem);
     
     if(err) {
          OSStatus2Errno(err);
          return -1;
     }
     
     return 0;
}

/**
 * sem_getvalue stores in the location pointed to by sval the
 * current count of the semaphore sem.
 *
 * sem_getvalue can set errno to one of the following errors:
 *
 * [EINVAL] - The sem argument is not a valid semaphore.
 *
 * @param sem A pointer to the semaphore object to get the value of.
 * @param sval A pointer to a location to store the value of sem.
 *
 * @return 0 upon successful completion, -1 otherwise. If -1 is returned
 * errno is set to the appropriate error.
 */
int sem_getvalue(sem_t *sem, int *sval)
{
     sememu_t *mac_sem;
     
     if(!SemaphoreValid(*sem)) {
          errno = EINVAL;
          return -1;
     }
     
     mac_sem = GetSemaphorePtr(*sem);
     
     *sval = mac_sem->value;
     
     return 0;
}

/**
 * sem_open is not implemented. This function always returnes with
 * errno set to ENOSYS and return value SEM_FAILED.
 *
 * @return SEM_FAILED
 */
sem_t *sem_open(const char *name, int oflag, ...)
{
     errno = ENOSYS;
     return (sem_t*)SEM_FAILED;
}

/**
 * sem_close is not implemented. This function always returnes with
 * errno set to ENOSYS and return value SEM_FAILED.
 *
 * @return SEM_FAILED
 */
int sem_close(sem_t *sem)
{
     errno = ENOSYS;
     return -1;
}


#else
#include <stdio.h>
void on_ne_fait_rien(void)
{
	printf("rien a faire si on est sous Linux...");
}
#endif

/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/*-------------------------------------------------------------------*/
/*     Promethe : simulateur de reseau de neurones                   */
/*     Gestion du sequencement des taches                            */
/*           initialisation des jetons                               */
/*           mise a jour des fonctions                               */
/*           mise a jour en fct des echelles de temps                */
/*             boucles de niveaux imbriquees en plus des jetons      */
/*     Copyright Philippe GAUSSIER 1992                              */
/*     modifie Nov 1994                                              */
/*     modifie Dec 1995 pour inclure 1/2 temps: buts                 */
/*     modifie Juin 1996 pour inclure separation fct_algo_mouv       */
/*      modif pour la plannification (Arnaud) d'abord apprentissage  */
/*      puis mouv effectif  (version problematique)                 */
/*                                                                   */
/*     modif du  19 sept 1996                                        */
/*     2 types de neurones doivent etre differentier                 */
/*       (mvt et les autres)                                         */
/*       general: M A M    (pas A si algo)                           */
/*       mvt    : O A M                                              */
/*       O = rien, M = mise_a_jour , A = apprentissage               */
/*-------------------------------------------------------------------*/

/*introduction d'un break !!! */

/*-------------------------------------------------------------------*/
/*   gere le sequencement des actions dans le reseau                 */
/*   utilise uniquement pour la partie algo                          */
/*   la partie RN fonctionne en mode synchrone qd sequencement fini  */
/*                                                                   */
/*   commence toujours par le noeud debut                            */
/*   lorsque un noeud a toute ses entrees actives, il s'active       */
/*   une fois active, il devient deja active                         */
/*   lorsque un groupe a toute ses entrees deja activees il s'active */
/*                                                                   */
/*   peut declencher un groupe si une liaison secondaire n'a pas de  */
/*   jeton en entree, si la liaison n'est pas secondaire: impossible */
/*-------------------------------------------------------------------*/

/*-------------------------------------------------------------------*/
/*   Sequencement du RN  :                                           */
/*       -  mise a jour des neurones / diffusion activite            */
/*       -  competition                                              */
/*       -  mise a jour STM                                          */
/*       -  apprentissage                                            */
/*-------------------------------------------------------------------*/

/* fevrier 2005 modif de la gestion de la STM */
/* La STM presynaptique sera mise a jour avant la mise a jour de chaque groupe */
/* La STM postsynaptique sera mise a jour apres la mise a jour de chaque groupe */
/* On arrete l'idee d'une mise a jour de la STM pre/post globalement pour toute*/
/* une echelle de temps comme c'etait fait avant. */

#include <sys/types.h>
#include <sys/time.h>
#include <unistd.h>
#include <stdlib.h>
#include <pthread.h>

/*#define DEBUG 1 */

#include "public.h"
#include "rttoken.h"         
#include "outils.h"
/*#define USE_THREADS 1 */

#include "gestion_threads.h"


/* mutex pour eviter que 2 fonctions ne modifient en meme temps les flags de sequencement des groupes rt_token... */
#ifdef USE_THREADS
pthread_mutex_t mutex_section_critique_sequenceur = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t mutex_section_critique_sequenceur_attend = PTHREAD_MUTEX_INITIALIZER;
#endif

/* #define DEBUG 1 */

/* verbose ne concerne que les messages affiches pendant le mode pas_a_pas.
 On peut donc laisser active cette option sans grand soucis d'affichage.
 Il suffit de ne pas se mettre en pas a pas. */
/* #define VERBOSE 1 */

/*#define without_p_trace 1*/

int etat_step_by_step = 1; /* variable utiliser pour synchroniser le mode step by step avec le bouton associe */
/* dans callback.c on change l'etat a chaque fois que step by step est clique. */
int memoire_etat_step_by_step = 1;

int echelle_temps_learn = 1; /* echelle de temps a la fin de laquelle */
/* l'apprentissage du RN est declenche   */
int echelle_temps_debug = 1; /* idem mais pour l'affichage dans promethe */
/* du debug du RN                           */

int flag_temps_dynamique = 0; /* =1 lorsque l'on est dans le 1/2 temps de modif mouv de Arnaud */

int iteration_echelle_courante = 0;

int flag_rt_break = 0; /* stocke le numero du dernier flag rt rencontre lors d'un break */

sem_t attend_un_rt_token;
sem_t first_group_finishing;
extern arg_thread_grp arg_global_thread_grp;
/* initialise tous les jetons de groupes correspondant a des ech de temps <= a ech */
void initialise_jetons(int ech)
{
	int i;

	dprints("Init token ech<=%d (gestion_sequ11.c)\n", ech);

	for (i = 0; i < nbre_groupe; i++)
	{
		if (def_groupe[i].ech_temps <= ech)
		{
			if (def_groupe[i].deja_active == 1 && def_groupe[i].rttoken <= 0)
			{
				def_groupe[i].deja_active = 0;
				def_groupe[i].deja_appris = 0;
			}
		}
	}
}

void initialise_jetons_juste_echelle(int ech)
{
	int i;

	dprints("Init token juste echelle ech=%d (gestion_sequ11.c)\n", ech);

	for (i = 0; i < nbre_groupe; i++)
	{
		if (def_groupe[i].ech_temps == ech)
		{
			if (def_groupe[i].deja_active == 1) /* on ne s'occupe plus des RTTOKEN pb pour la version non threadee */
			{
				def_groupe[i].deja_active = 0;
			}
		}
	}
}

/* en cas de break active tous les jetons des groupes situes
 sur des echelles de temps inferieures ou egales a l'echelle
 de t emps ayant declenchee le break.
 De cette maniere les groupes de plus haut niveau, ont l'impression
 que tout c'est passe normalement et peuvent eux aussi s'executer.
 Ils verront l'activite qu'avaient les groupes juste avant le break */

/* attention a l'incoherence avec les groupes qui ne pouvaient
 pas etre mis a jour pour cause de dead lock local !!! */

void set_tokens(int ech)
{
	int i;
	for (i = 0; i < nbre_groupe; i++)
	{
		if (def_groupe[i].ech_temps <= ech)
		{
			def_groupe[i].deja_active = 1;
			def_groupe[i].rttoken = flag_rt_break;
		}
	}
}

/* renvoie 2 si le break reussit et 1 sinon                     */
/* le break reussit si le neurone du groupe en entree est actif */
/* le groupe break peu prendre ses entrees de plusieurs gpes    */
/* dans ce cas la il effectue un OU logique. Il suffit qu'un des*/
/* neurones en entree soit active pour que le break soit declenche*/
/* ATTENTION les THREADS ne sont pas geres avec break...        */

int exec_cmd_break(int gpe)
{
	type_coeff *coeff;
	int deb;
	float input = 0.;
	float seuil;

	deb = def_groupe[gpe].premier_ele;
	coeff = neurone[deb].coeff;
	seuil = neurone[deb].seuil;

	if (coeff == NULL)
	{
		printf("ERROR the BREAK group (%s) must be linked with a real link to the input group\n", def_groupe[gpe].no_name);
		printf("Please do not use an algorithmic link - too slow ... \n");
		printf("You can use a one to one link for instance.\n");
		exit(0);
	}

	while (coeff != NULL)
	{
		input = neurone[coeff->entree].s1;
		if (input >= seuil) break;
		coeff = coeff->s;
	}

	neurone[deb].s = neurone[deb].s1 = neurone[deb].s2 = input;
	flag_rt_break = 0;

	if (input >= 1.)
	{
#ifdef VERBOSE
		if (p_trace > 0)
		printf("Beginning of BREAK execution at iteration %d\n", iteration_echelle_courante);
#endif
		if (p_trace > 1) printf("Break activated\n");

		flag_rt_break = def_groupe[gpe].rttoken;
		return 2;
	}

	return 1;
}

/*------------------------------------------------------------------*/
/* teste si toutes les entrees sont actives                         */
/* si oui :  active la fonction et met deja_active a 1              */
/*                                                                  */
/* retourne 1 si le groupe a pu etre active et que tout est OK      */
/* retourne 0 si le groupe n'a pu etre active                       */
/* 								                                                  */
/* retourne 2 si le groupe est break et que le break reussi         */
/* retourne 1 si le break echoue... mais est declenche              */
/*------------------------------------------------------------------*/

void wg_debug_pas_a_pas(int gpe, int local_flag_temps_dynamique)
{
	(void) local_flag_temps_dynamique;

	kprints("wg_debug_pas_a_pas gpe %s\n", def_groupe[gpe].no_name);
	dprints("promethe begin group %s , stm =%d, flag_temps_dynamique = %d \n", def_groupe[gpe].no_name, gestion_STM, local_flag_temps_dynamique);

#ifndef AVEUGLE
	led_groupe(&fenetre1, gpe, rouge);
	TxFlush(&fenetre1);
#endif

	while (p_trace == 2 && etat_step_by_step == memoire_etat_step_by_step)
		usleep(10); /*gdk_events_pending (); */

	memoire_etat_step_by_step = etat_step_by_step;
	printf("fin wg_debug_pas_a_pas gpe %s\n", def_groupe[gpe].no_name);
}

/*-----------------------------*/

void wg_trace_mode(void)
{
	printf("wg_trace_mode\n");

#ifndef AVEUGLE
	TxFlush(&fenetre1);
#endif

	while (p_trace > 0 && etat_step_by_step == memoire_etat_step_by_step)
		usleep(10); /* gdk_events_pending (); */

	memoire_etat_step_by_step = etat_step_by_step;
	printf("fin wg_trace_mode\n");
}

/*---------------------------------------------------------------------*/
/*                    MISE A JOUR DES FONCTIONS                        */
/*                                                                     */
/*   learn = 1 apprentissage modif des poids                           */
/*           sinon que propagation info et modif sorties               */
/*                                                                     */
/*   gestion_STM = 1 modification des memoires a court terme           */
/*                   sinon on considere que c'est un transitoire       */
/*                   tres rapide qui ne les affecte pas                */
/*   ech       niveau temporel pour savoir si l'on doit reexecuter une */
/*             fontion ou pas                                          */
/*                                                                     */
/*  execute tous les groupes ech_temps < ech sauf pour RN  ech ==0     */
/* pour Rn, il faut faire un appel avec ech=0                          */
/* et mise a jour sortie STM                                           */
/*---------------------------------------------------------------------*/

/*-------------------------------------------------------------------*/
/*  cas du fonctionnement sequentiel. Un seul groupe a executer      */
/* pas besoin d'utiliser des threads                                 */
/* mvt=1 dit qu'il faut mettre a jour les fct algo mvts              */
/*-------------------------------------------------------------------*/

int tente_d_activer_groupe(int gpe, int local_learn)
{
	/* le groupe peut etre active toutes ses entrees ont deja ete activees */

	if (def_groupe[gpe].breakpoint > 0)
	{
		p_trace = 2;
		printf("\n\n===== BREAKPOINT ======= \n\n groupe %s \n", def_groupe[gpe].no_name);
	}

	if (p_trace > 1)
	{
		printf("demande l'execution du groupe : %s, flag_temps_dynamique = %d \n", def_groupe[gpe].no_name, flag_temps_dynamique);
		wg_debug_pas_a_pas(gpe, flag_temps_dynamique);
	}

	if (def_groupe[gpe].type == No_BREAK)
	{
		def_groupe[gpe].deja_active = 1;
		return (exec_cmd_break(gpe));
	}

	/* si rt_token on suppose qu'on ne peut rien faire. C'est rttoken.c qui doit gerer. */
	if (def_groupe[gpe].type == No_RTTOKEN) return (0);

	if (def_groupe[gpe].type == No_Fonction_Algo || def_groupe[gpe].type == No_Fonction_Algo_mvt) (*def_groupe[gpe].appel_algo)(gpe);
	else
	{
		if (local_learn == 0) (*def_groupe[gpe].appel_algo)(gpe /*,local_learn */); /* ex mise_a_jour_normale */
		else (*def_groupe[gpe].appel_apprend)(gpe);
	}

	dprints("gpe %s  deja_active=1 \n", def_groupe[gpe].no_name);
	def_groupe[gpe].deja_active = 1;

#ifndef AVEUGLE
	if (def_groupe[gpe].debug != 0 && debug != 0 && def_groupe[gpe].ech_temps >= echelle_temps_debug)
	{
		affiche_debug_gpe(&fenetre1, gpe);
		TxDisplay(&fenetre1);
	}
#endif
	return 1;
}

/*-------------------------------------------------------------------*/
/*               ne met a jour que l'echelle consideree              */
/*            la gestion STM pour RN est reglee en dehors            */
/*-------------------------------------------------------------------*/

/* calcul temps t+1 du reseau  */

#define max_parallele 256

int gpes_en_parallele[max_parallele];
pthread_t un_thread[max_parallele];
arg_thread_grp arg[max_parallele];

/*----------------------------------------*/

/* ne teste pas si des groupes sont restes inactives */
/* ajout d'un test pour l'instruction BREAK */
/* la fct renvoie 1 si BREAK, 0 si pas de break */
/* equivalent de tente_d_activer_groupe avec threads */

#ifdef USE_THREADS
/** Apparement jamais appele ? a supprimer ?? AB. */
void *tente_d_activer_groupe_thread(void *local_arg)
{

	int no, gpe, local_learn;

	gestion_mask_signaux();
	no = ((arg_thread_grp *) local_arg)->no_thread;
	gpe = ((arg_thread_grp *) local_arg)->gpe;
//	gestion_STM = ((arg_thread_grp *) local_arg)->gestion_STM;
	local_learn = ((arg_thread_grp *) local_arg)->learn;

	/* le groupe peut etre active toutes ses entrees ont deja ete activees */


	if (def_groupe[gpe].breakpoint > 0)
	{
		p_trace = 2;
		printf("\n\n===== BREAKPOINT ======= \n\n groupe %s \n", def_groupe[gpe].no_name);
	}

	if (p_trace > 1)
	{
		printf("execution du thread %d correspondant au gpe %s \n", no, def_groupe[gpe].no_name);
		printf("demande l'execution du groupe : %s , flag_temps_dynamique = %d \n", def_groupe[gpe].no_name,  flag_temps_dynamique);
		wg_debug_pas_a_pas(gpe,  flag_temps_dynamique);
	}

	if (def_groupe[gpe].type == No_BREAK)
	{
		def_groupe[gpe].deja_active = 1;
		((arg_thread_grp *) local_arg)->retour = exec_cmd_break(gpe);
		pthread_exit(NULL);
	}

	/* si rt_token on suppose qu'on ne peut rien faire. C'est rttoken.c qui doit gerer. */
	if (def_groupe[gpe].type == No_RTTOKEN)
	{
		((arg_thread_grp *) local_arg)->retour = 0;
		pthread_exit(NULL);
	}

	if (def_groupe[gpe].type == No_Fonction_Algo || def_groupe[gpe].type == No_Fonction_Algo_mvt) (*def_groupe[gpe].appel_algo)(gpe /*,local_learn */);
	else if (def_groupe[gpe].type != No_Sub_Network)
	{
		if (local_learn == 0)
		{
			(*def_groupe[gpe].appel_algo)(gpe /*,local_learn */); /* ex mise_a_jour_normale */
		}
		else
		{
			(*def_groupe[gpe].appel_apprend)(gpe);
		}
	}

	dprints("gpe %s  deja_active=1 tente_d_activer_groupe_thread\n", def_groupe[gpe].no_name);
	def_groupe[gpe].deja_active = 1;

#ifndef AVEUGLE
	if(def_groupe[gpe].debug != 0 && debug != 0 &&  def_groupe[gpe].ech_temps >= echelle_temps_debug)
	{
		affiche_debug_gpe(&fenetre1, gpe);
		TxDisplay(&fenetre1);
	}
#endif

	((arg_thread_grp *) local_arg)->retour = 1;
	pthread_exit(NULL);
}
#endif

/* execute en parallele les groupes dont le numero figure dans gpes_en_parallele */
/* nbre_parallele contient le nbre de groupes a lancer simultanement             */
/* la fonction renvoie 0 si aucun groupe n'a pu s'executer, 1 si au moins un gpe */
/* a ete mis a jour et 2 si un break est intervenu                               */

#ifdef USE_THREADS
int execute_en_parallele(int *local_gpes_en_parallele, int nbre_parallele, int local_learn, int mvt)
{
	int i, j, res;
	void *local_resultat;
	int val_retour = 0;

	for (i = 1; i < nbre_parallele; i++)
	{
#ifdef VERBOSE
		if (p_trace > 0)
		printf("Creation thread %d  pour le groupe %s\n", i, def_groupe[local_gpes_en_parallele[i]].no_name);
#endif

		arg[i].no_thread = i;
		arg[i].gpe = local_gpes_en_parallele[i];
		//arg[i].gestion_STM = gestion_STM;
		arg[i].learn = local_learn;
		arg[i].mvt = mvt;
		arg[i].retour = 0;

		res = pthread_create(&(un_thread[i]), NULL, tente_d_activer_groupe_thread, (void *) &(arg[i]));
		if (res != 0)
		{
			printf("fealure on thread creation \n");
			exit(EXIT_FAILURE);
		}
	}

	val_retour = tente_d_activer_groupe(local_gpes_en_parallele[0], local_learn); /* pas de creation de thread pour le 1er gpe */

	if (nbre_parallele > 1)
	{
#ifdef VERBOSE
		if (p_trace > 0) printf("En attente de terminaison de tous les threads ... \n");
#endif
		for (j = 1; j < nbre_parallele; j++)
		{
			/* printf("j= %d \n",j); */
			res = pthread_join(un_thread[j], &local_resultat);
#ifdef VERBOSE
			if (p_trace > 0)
			printf("j=%d retour du groupe %s = %d \n", j, def_groupe[arg[j].gpe].no_name, arg[j].retour);
#endif
			if (res == 0)
			{
#ifdef VERBOSE
				if (p_trace > 0) printf("thread %d recueilli \n", j);
#endif
			}
			else
			{
				printf("echec de pthread_join %d pour le thread %d\n", res, j);
				exit(EXIT_FAILURE);
			}
			if (arg[j].retour > val_retour) val_retour = arg[j].retour; /* le break retour=2 est prioritaire */
			if (arg[j].retour == 0)
			{
				printf("Problem in the execution of the group %s \n", def_groupe[arg[j].gpe].no_name);
				printf("Return value after running is 0. This should not happend... \n");
				exit(0);
			}
		}
#ifdef VERBOSE
		if (p_trace > 0)
		printf("execution THREADS terminee \n");
#endif
	}

	return val_retour;
}
#endif

/*------------------------------------------*/

int execute_en_pseudo_parallele(int *local_gpes_en_parallele, int nbre_parallele, int local_learn, int mvt)
{
	int i, res;

	int val_retour = 0;
	(void) mvt;

#ifdef VERBOSE
	if (p_trace > 0) printf("execution en pseudo parallele de %d groupes \n", nbre_parallele);
#endif

	for (i = 0; i < nbre_parallele; i++)
	{
#ifdef VERBOSE
		if (p_trace > 0) printf("execution du groupe %s\n", def_groupe[local_gpes_en_parallele[i]].no_name);
#endif
		res = tente_d_activer_groupe(local_gpes_en_parallele[i], local_learn); /* pas de creation de thread         */
		if (res > val_retour) val_retour = res; /* le break retour=2 est prioritaire */
		if (res == 0)
		{
			printf("Problem in the execution of the group %s \n", def_groupe[arg[i].gpe].no_name);
			printf("Return value after running is 0. This should not happend... \n");
			exit(0);
		}
	}
	return val_retour;
}

/*------------------------------------------*/
/* fonction qui renvoie un 1 si un groupe est directement activable,
 c.a.d si l'esnemble des groupes en entree ont ete actives.
 Un groupe sans entree est directement activable.
 Un groupe du type rttoken ne depend que de la contrainte temps reel.
 Les groupes a mouvement retarde ne s'activent que si on est dans la passe mvt=1 */


static inline int directement_activable_mode_P(int gpe, int mvt)
{
    int gpe_entree, i, j;
    int no_token = PAS_RT_TOKEN;
    int token;
    int test_en_cours_dexecution;


    // Attention : ceci doit etre en premier pour etre valide !
    sem_getvalue(&(def_groupe[gpe].sem_lock_fields), &test_en_cours_dexecution);
    if (test_en_cours_dexecution > 0)
       {
           return -1;
       }
    if (def_groupe[gpe].type == No_RTTOKEN || def_groupe[gpe].rttoken > 0 || def_groupe[gpe].deja_active == 1 || (def_groupe[gpe].type2 != 0 && mvt == 0))
    {
        return 0;
    }

    j = 0;
    i = find_input_link(gpe, j);
    while (i != -1) /* tant que le groupe a encore un lien en entree a verifier */
    {
        gpe_entree = liaison[i].depart;
        token = def_groupe[gpe_entree].rttoken;
        if ((def_groupe[gpe_entree].deja_active == 0) && (liaison[i].secondaire == 0)) return 0;

        if (liaison[i].secondaire == 0)
        {
            if (no_token < 0 && token > 0) no_token = token; /* real time prioritere sur non temps reel */
            if (token < no_token && token > 0) no_token = token; /* si <0 ce n'est pas un token temps reel */
        }

        /* le groupe precedent est en fault */
        if (def_groupe[gpe_entree].fault == 1) return 0;

        j = j + 1;
        i = find_input_link(gpe, j);
    }
    def_groupe[gpe].rttoken = no_token;

    return 1;
}





int directement_activable(int gpe, int mvt)
{
	int gpe_entree, i, j;
	int no_token = PAS_RT_TOKEN;
	int token;

	if (def_groupe[gpe].type == No_RTTOKEN)
	{
		dprints("gpe %s est un rt_token on l'ignore (deja_active= %d rt = %d)\n", def_groupe[gpe].no_name, def_groupe[gpe].deja_active, def_groupe[gpe].rttoken);
		return 0;
	}
	if (def_groupe[gpe].rttoken > 0)
	{
		dprints("groupe %s deja active par un jeton rt, on ne le reactive pas \n", def_groupe[gpe].no_name);
		return 0;
	}

	if (def_groupe[gpe].deja_active == 1)
	{
		dprints("gpe %s deja active : echec\n", def_groupe[gpe].no_name);
		return 0;
	}

	if (def_groupe[gpe].type2 != 0 && mvt == 0)
	{
		return (0);
	}

	j = 0;
	i = find_input_link(gpe, j);
	while (i != -1) /* tant que le groupe a encore un lien en entree a verifier */
	{
		gpe_entree = liaison[i].depart;
		token = def_groupe[gpe_entree].rttoken;
		if ((def_groupe[gpe_entree].deja_active == 0) && (liaison[i].secondaire == 0)) return 0;

		if (liaison[i].secondaire == 0)
		{
			if (no_token < 0 && token > 0) no_token = token; /* real time prioritere sur non temps reel */
			if (token < no_token && token > 0) no_token = token; /* si <0 ce n'est pas un token temps reel */
		}

		/* le groupe precedent est en fault */
		if (def_groupe[gpe_entree].fault == 1) return 0;

		dprints("gpe %s : entree %s , token= %d min_token=%d \n", def_groupe[gpe].no_name, def_groupe[gpe_entree].no_name, token, no_token);

		j = j + 1;
		i = find_input_link(gpe, j);
	}
	def_groupe[gpe].rttoken = no_token;

	dprints("groupe %s directement activable token = %d \n", def_groupe[gpe].no_name, no_token);

	return 1;
}

/*-----------------------------------------------------------------------*/
/* idem que mise_a_jour_fonction2  mais avec la gestion des threads      */

/* renvoie 1 si break, 0 si au moins un groupe s'est execute, -1 si aucun groupe active */

int gere_une_vague_parallele(int ech, int mvt, int *pas_de_groupe_active)
{
	int i;
	int flag = 0; /* plus de groupe a activer a cette echelle ? */
	int nbre_parallele = 0; /* nbre de groupes a lancer en parallele   */
	int nb_rttoken_boxes = 0;
	int retour;

	for (i = 0; i < nbre_groupe; i++)
	{
		if (def_groupe[i].ech_temps == ech)
		{
			if (directement_activable(i, mvt) == 1)
			{
				gpes_en_parallele[nbre_parallele] = i;
				nbre_parallele = nbre_parallele + 1;
				*pas_de_groupe_active = 0;
				if (def_groupe[i].rttoken > 0) nb_rttoken_boxes++;
			}
		}
	}
	gpes_en_parallele[nbre_parallele] = -1;

	if (nbre_parallele < 1)
	{
#ifdef VERBOSE
		if (p_trace > 0) printf("no other group can be executed \n");
#endif

		dprints("no other group can be executed \n");
		return 0; /* arret global, pas_de_groupe_active = 0 si au moins un gpe active , -1 sinon */
	}

#ifdef USE_THREADS
#ifdef USE_THREADS_2
	retour = execute_en_parallele2(gpes_en_parallele, nbre_parallele, 0, mvt);
#else
	retour = execute_en_parallele(gpes_en_parallele, nbre_parallele, 0, mvt);
#endif
#else
	retour = execute_en_pseudo_parallele(gpes_en_parallele, nbre_parallele, 0, mvt);
#endif

	if (retour == 2) /* cas du break */
	{
		set_tokens(ech);
		*pas_de_groupe_active = 1;
		return 0; /* arret et break */
	}
	else if (retour == 1)
	{
		flag = 1; /* un groupe s'est active, il en reste peu etre d'autres ... */
	}

	return flag;
}

int gere_une_vague_parallele_protected(int ech, int mvt, int *pas_de_groupe_active)
{
	int val_retour;

#ifdef USE_THREADS
	dprints("mutex fonction_parallele ech %d \n", ech);

	pthread_mutex_lock(&mutex_section_critique_sequenceur_attend); /*c'est rttoken qui est prioritaire pour le lock mutex_section_critique_sequenceur */
	pthread_mutex_unlock(&mutex_section_critique_sequenceur_attend);

	pthread_mutex_lock(&mutex_section_critique_sequenceur);
#endif

	val_retour = gere_une_vague_parallele(ech, mvt, pas_de_groupe_active);

#ifdef USE_THREADS
	pthread_mutex_unlock(&mutex_section_critique_sequenceur);
	dprints("fin mutex fonction_parallele %d \n", ech);
#endif	

	return val_retour;
}

// Uniquement si parralele !
int gestion_des_groupes_P(int ech, int mvt, int* pas_de_groupe_active)
{
      int i;
      int flag = 1; /* plus de groupe a activer a cette echelle ? */
      int nbre_parallele = 0; /* nbre de groupes a lancer en parallele   */
      int val;
      int reste_en_execution=0;
      int gpe,res;
      int local_learn = 0;

      // Pour tout les groupes
      for (i = 0; i < nbre_groupe; i++)
      {
          //todo Si l'echelle de temps est bien celle en cours de travail (Meilleur methode quelques part ???? -> remplir des listes au début du script pour qu'a chaque echelle de temps sois associé un tableau de i correspondant au num du groupe)
          if (def_groupe[i].ech_temps == ech)
          {
            // Si le groupe est activable (jeton ok etc...)
              val=directement_activable_mode_P(i, mvt);
              if (val == 1)
              {
                  // On remplis le tableau de groupe à activer par la suite ! (pourquoi est-il en global ? ? ?)
                  gpes_en_parallele[nbre_parallele] = i;
                  nbre_parallele = nbre_parallele + 1;

                  // compteur exterieur à la fonction todo : pourquoi ?
                  *pas_de_groupe_active = 0;
              }
              else if(val==-1)
              {
                reste_en_execution=1;
              }
            //  else //non necessaire
            //  {
              //  printf("le groupe %s n'est pas active 0 \n", def_groupe[i].no_name)   ;
            //  }
          }
      }
      // Le bout de la liste est à -1 pour marquer la fin.
      gpes_en_parallele[nbre_parallele] = -1;

      if (nbre_parallele < 1 && reste_en_execution==0)
        {
           sem_post(&first_group_finishing);
           dprints("no other group can be executed \n");
           return 0; /* arret global, pas_de_groupe_active = 0 si au moins un gpe active , -1 sinon */
        }

      ////////////////////////////////

      // Reveil de tout les groupes à activer ce coups ci !
      for (i = 0; i < nbre_parallele; i++)
      {
        gpe = gpes_en_parallele[i];
        arg_global_thread_grp.learn = local_learn;
        arg_global_thread_grp.mvt = mvt;
        def_groupe[gpe].return_value = 0; /* on suppose l'echec avant le lancement */
        def_groupe[gpe].message[0] = 0; /* message vide correspond a un execution normale */

        res = sem_post(&def_groupe[gpe].sem_wake_up); /* reactive le thread en attente */
        if (res != 0) EXIT_ON_SYSTEM_ERROR("fealure on sem_post in execute_en_parallele2 gpe %s \n", def_groupe[gpe].no_name);
      }

          return flag;
}

int mise_a_jour_fonction_parallele(int ech, int mvt)
{
	int flag;
	int pas_de_groupe_active = -1;

	flag = 1;

    #if (GESTION_ALTERNATIVE == 1)
	  sem_init(&first_group_finishing, 0, 0);
    #endif

	while (flag != 0) /* tant qu'un groupe peut etre active */
	{
#if (GESTION_ALTERNATIVE == 1)
#ifdef USE_THREADS
	  pthread_mutex_lock(&mutex_section_critique_sequenceur_attend); /*c'est rttoken qui est prioritaire pour le lock mutex_section_critique_sequenceur */
	    pthread_mutex_unlock(&mutex_section_critique_sequenceur_attend);

	    pthread_mutex_lock(&mutex_section_critique_sequenceur);
#endif
#endif

#if (GESTION_ALTERNATIVE == 1)
	        flag = gestion_des_groupes_P(ech, mvt, &pas_de_groupe_active);
	        sem_wait(&first_group_finishing);
#else
	       flag = gere_une_vague_parallele_protected(ech, mvt, &pas_de_groupe_active);
#endif

#if (GESTION_ALTERNATIVE == 1)
#ifdef USE_THREADS
	    pthread_mutex_unlock(&mutex_section_critique_sequenceur);
	    dprints("fin mutex fonction_parallele %d \n", ech);
#endif
#endif
	}
    #if (GESTION_ALTERNATIVE == 1)
	  sem_destroy(&first_group_finishing);
    #endif

	return pas_de_groupe_active;
}

/*-----------------------------------------------------------------------*/

void gestion_groupes_rn(void)
{
	int gpe;

	for (gpe = 0; gpe < nbre_groupe; gpe++)
	{
		if (def_groupe[gpe].type != No_Sub_Network && def_groupe[gpe].type != No_Fonction_Algo) (*def_groupe[gpe].appel_gestion)(gpe);
	}
}

void apprend_groupe(int gpe)
{
	if (def_groupe[gpe].type != No_Sub_Network && def_groupe[gpe].type != No_RTTOKEN && def_groupe[gpe].type != No_Fonction_Algo && def_groupe[gpe].deja_appris == 0) /* == au lieu de <= */
	{
		(*def_groupe[gpe].appel_apprend)(gpe);
		def_groupe[gpe].deja_appris = 1;
	}
}

void apprend_groupes_rn(int ech)
{
	int gpe;

#ifdef VERBOSE
	if (p_trace > 0)
	printf("\n\n\n APPRENTISSAGE ECHELLE %d \n", ech);
#endif

	for (gpe = 0; gpe < nbre_groupe; gpe++)
	{
		/*if (def_groupe[gpe].type!= No_Sub_Network && def_groupe[gpe].type != No_RTTOKEN && def_groupe[gpe].type != No_Fonction_Algo && def_groupe[gpe].ech_temps == ech)*//* == au lieu de <= */
		/*{
		 * printf("########### apprend_groupes_rn, avant mise_a_jour\n");
		 * mise_a_jour(gpe, 1, 0);
		 *printf("########### apprend_groupes_rn, apres mise_a_jour\n");
		 }*/

		/*rajout*/
		if (def_groupe[gpe].ech_temps == ech && def_groupe[gpe].deja_active == 1) apprend_groupe(gpe);
	}
}

/*----------------------------------------------------------------*/
/*  lance par promethe2  si learn=1                               */
/*----------------------------------------------------------------*/

int au_moins_un_groupe_active = 0;

void mise_a_jour_des_sous_echellesb(int ech)
{
	long int i, k;
	int nn_break;

	/*   if(ech==0)  mise_a_jour_STM_entrees_neurones(); */

	flag_temps_dynamique = 0;

	dprints("ech=%d , echelle_temps_debug= %d \n", ech, echelle_temps_debug);
	dprints("p_trace=%d \n", p_trace);

	for (i = 0; (i < echelle_temps[ech]) || (echelle_temps[ech]==-1); i = i + au_moins_un_groupe_active)
	{
		/* verifier avant qu'il y a bien un rt_token a cette echelle sinon blocage ... */
		au_moins_un_groupe_active = 0; /* recapitule si un groupe active dans les ech de temps inferieures ou egales a ech*/
		/*    printf("ech %d iteration %d increment %d \n",ech,i,au_moins_un_groupe_active);*/
		iteration_echelle_courante = i; /* pas propre... pour affichage lors du break */
		initialise_jetons(ech);

		nn_break = mise_a_jour_fonction_parallele(ech, 0); /* 0 pour groupe sans dynamique */
		if(fin_echelle[ech]==1) i=echelle_temps[ech];

		dprints("echelle_temps_rt[%d]==%d nn_break=%d \n", ech, echelle_temps_rt[ech], nn_break);
		if (nn_break >= 0 || echelle_temps_rt[ech] == 0) au_moins_un_groupe_active = 1; /* important pour ne pas bloquer le simulateur dans le cas non rt*/

		if (p_trace > 0)
		{
			printf("fin 1ere partie mise_a_jour_des_sous_echellesb ech=%d , iteration = %ld , max= %d (descente)\n", ech, i, echelle_temps[ech]);
			wg_trace_mode();
		}

		if (nn_break == 1) break; /* simple non? */

		if (ech > 0)
		{
			mise_a_jour_des_sous_echellesb(ech - 1);
			/* remontee de l'information issus des echelles temporelles */
			/*                   de plus bas niveau                     */
			nn_break = mise_a_jour_fonction_parallele(ech, 0);
			if (nn_break >= 0 || echelle_temps_rt[ech] == 0) au_moins_un_groupe_active = 1;

			if (nn_break == 1) break;
		}
		/*
		 mise_a_jour_STM_entrees_neurones(ech);
		 mise_a_jour_STM_sortie_neurones(ech);
		 */

		if (p_trace >= 1)
		{
			printf("fin 2eme partie mise_a_jour_des_sous_echellesb (remontee)\n");
			wg_trace_mode();
		}

		if (ech <= echelle_temps_learn)
		{
			apprend_groupes_rn(ech); /* au lieu de ech -1 */
		}

		if (p_trace > 0)
		{
			printf("TRACE (end of learning) ech %d\n", ech);
			wg_trace_mode();
		}

		dprints("===ech %d , au_moins_un_groupe_active = %d \n", ech, au_moins_un_groupe_active);

		if (au_moins_un_groupe_active == 0)
		{
			/*  #define DEBUG*/
			dprints("ech=%d , pas de groupe mis a jour attend rt_token \n", ech);
			sem_wait(&attend_un_rt_token);
			dprints("un rt_token emit quelque part, on va retester si un groupe peut s'activer maintenant \n");
		}

	} /* fin de la boucle iterative de mise a jour d'une echelle de temps */

	if ((echelle_temps[ech] > 0 || echelle_temps[ech]==-1) && echelle_temps_dynamique[ech] == 1)
	{ /* pour Arnaud */
#ifdef VERBOSE
		if (p_trace > 0)
		{
			printf("()()()()()() 2eme mise a jour pour raison de dynamique ()()(ARNAUD)(B)()() \n");
		}
#endif

		/* 	  initialise_jetons_juste_echelle(ech);   */
		flag_temps_dynamique = 1;
		initialise_jetons(ech);
		for (k = ech; k >= 0; k--)
			nn_break = mise_a_jour_fonction_parallele(k, 1); /* nn_break ???? */
		for (k = 0; k <= ech; k++)
			nn_break = mise_a_jour_fonction_parallele(k, 1);
		/*      mise_a_jour_fonction2(gestion_STM,ech,1);   pour groupe avec dynamique mvt */

		if (p_trace > 0)
		{
			printf("Fin 2eme mise a jour groupes actions dynamiques \n");
			wg_trace_mode();
		}
	}

	/************** gestion des mouvements t+GrandDT  *************************/
	else if ((echelle_temps[ech] > 0 || echelle_temps[ech]==-1) && echelle_temps_dynamique[ech] == 2)
	{ /* pour Cedric */
#ifdef VERBOSE
		if (p_trace > 0)
		{
			printf("()()()()()() 2eme mise a jour pour raison de apprentissage retarde ()()(CEDRIC)()()() \n");
		}
#endif
		/*initialise_jetons_juste_echelle(ech);   */
		nn_break = mise_a_jour_fonction_parallele(ech, 1); /* pour groupe avec dynamique mvt */
		if (ech <= echelle_temps_learn) apprend_groupes_rn(ech);

		if (p_trace > 0)
		{
			printf("TRACE: appuyer sur une touche \n");
			wg_trace_mode();
		}
	}

	dprints(":::::::::::::::::::: mise_a_jour_des_sous_echellesb,  fin ech %d \n", ech);
}

/*----------------------------------------------------------------*/
/*  lance par promethe2  si learn=0                               */
/*----------------------------------------------------------------*/

void mise_a_jour_des_sous_echelles(int ech)
{
    long int i, k;
	int nn_break;
	/*   if(ech==0)  mise_a_jour_STM_entrees_neurones(); */
	flag_temps_dynamique = 0;

	for (i = 0; (i < echelle_temps[ech]) || (echelle_temps[ech]==-1); i = i + au_moins_un_groupe_active)
	{
		au_moins_un_groupe_active = 0; /* recapitule si un groupe active dans les ech de temps inferieures ou egales a ech*/
		iteration_echelle_courante = i; /* pas propre... pour affichage lors du break */
		initialise_jetons(ech);

		nn_break = mise_a_jour_fonction_parallele(ech, 0); /*0 pour groupe sans dynamique */
		if (nn_break >= 0 || echelle_temps_rt[ech] == 0) au_moins_un_groupe_active = 1;

		if (p_trace > 0)
		{
			printf("TRACE(use only): fin mise a jour en parallele (descente)\n");
			wg_trace_mode();
		}

		if (nn_break == 1) break; /*simple non ? */
		if (ech > 0)
		{
			mise_a_jour_des_sous_echelles(ech - 1);
			/* remontee de l'information issus des echelles temporelles */
			/*                   de plus bas niveau                     */
			nn_break = mise_a_jour_fonction_parallele(ech, 0);
			if (nn_break >= 0 || echelle_temps_rt[ech] == 0) au_moins_un_groupe_active = 1;
			if (nn_break == 1) break;
		}

		/*  la gestion STM est maintenant faite gpe par gpe */
		/*      mise_a_jour_STM_entrees_neurones(ech);
		 mise_a_jour_STM_sortie_neurones(ech); */

		if (p_trace >= 1)
		{
			printf("TRACE(use only): fin mise a jour en parallele (remontee)\n");
			wg_trace_mode();
		}

		if (au_moins_un_groupe_active == 0)
		{
			dprints("ech=%d , pas de groupe mis a jour attend rt_token \n", ech);
			sem_wait(&attend_un_rt_token);
			dprints("un rt_token emit quelque part, on va retester si un groupe peut s'activer maintenant \n");
		}
	} /* fin de la boucle iterative de mise a jour d'une echelle de temps */

	if ((echelle_temps[ech] > 0 || echelle_temps[ech]==-1) && echelle_temps_dynamique[ech] == 1)
	{ /* pour Arnaud */
		dprints("()()()()()() use-only 2eme mise a jour pour raison de dynamique ()()(ARNAUD)(NB)()() \n");

		/* 	  initialise_jetons_juste_echelle(ech);   */
		flag_temps_dynamique = 1;
		initialise_jetons(ech);

		for (k = ech; k >= 0; k--)
			nn_break = mise_a_jour_fonction_parallele(k, 1);
		for (k = 0; k <= ech; k++)
			nn_break = mise_a_jour_fonction_parallele(k, 1);
		/*    mise_a_jour_fonction2(gestion_STM,ech,1);   pour groupe avec dynamique mvt */

		if (p_trace > 0)
		{
			printf("TRACE: press the button: fin mise a jour en parallele \n");
			wg_trace_mode();
		}
	}

	/************** gestion des mouvements t+GrandDT  *************************/

	else if ((echelle_temps[ech] > 0 || echelle_temps[ech]==-1) && echelle_temps_dynamique[ech] == 2)
	{ /* pour Cedric */
		printf("()()()()()() use_only 2eme mise a jour pour raison de apprentissage retarde ()()(CEDRIC)()()() \n");
		/*initialise_jetons_juste_echelle(ech);   */
		nn_break = mise_a_jour_fonction_parallele(ech, 1); /* pour groupe avec dynamique mvt */

		if (p_trace > 0)
		{
			printf("TRACE: press the button \n");
			wg_trace_mode();
		}
	}
}

/* Fonction non utilisee. Presente juste pour permettre une lecture plus facile du code du sequenceur. */
/* Normalement seul les ifdef ont ete enleves.                                                         */
/* Voir mise_a_jour_des_sous_echelles()                                                                */

void code_du_sequ_de_base_juste_pour_lire(int ech)
{
    long int i, k;
	int nn_break;

	flag_temps_dynamique = 0;

	printf("ech=%d , echelle_temps_debug= %d \n", ech, echelle_temps_debug);

	for (i = 0; (i < echelle_temps[ech]) || (echelle_temps[ech]==-1); i++)
	{
		iteration_echelle_courante = i; /* pas propre... pour affichage lors du break */
		initialise_jetons(ech);

		nn_break = mise_a_jour_fonction_parallele(ech, 0); /*0 pour groupe sans dynamique */

		if (nn_break == 1) break; /* simple non? */

		if (ech > 0)
		{
			mise_a_jour_des_sous_echellesb(ech - 1);
			/* remontee de l'information issus des echelles temporelles */
			/*                   de plus bas niveau                     */
			nn_break = mise_a_jour_fonction_parallele(ech, 0);
			if (nn_break == 1) break;
		}
		if (ech <= echelle_temps_learn)
		{
			apprend_groupes_rn(ech);
		}
	}

	if ((echelle_temps[ech] > 0 || echelle_temps[ech]==-1) && echelle_temps_dynamique[ech] == 1)
	{ /* pour Arnaud */
		flag_temps_dynamique = 1;
		initialise_jetons(ech);
		for (k = ech; k >= 0; k--)
			nn_break = mise_a_jour_fonction_parallele(k, 1); /* nn_break ???? */
		for (k = 0; k <= ech; k++)
			nn_break = mise_a_jour_fonction_parallele(k, 1);
		/*      mise_a_jour_fonction2(gestion_STM,ech,1);   pour groupe avec dynamique mvt */
	}

	/************** gestion des mouvements t+GrandDT  *************************/

	else if ((echelle_temps[ech] > 0 || echelle_temps[ech]==-1) && echelle_temps_dynamique[ech] == 2)
	{ /* pour Cedric */
		/*initialise_jetons_juste_echelle(ech);   */
		nn_break = mise_a_jour_fonction_parallele(ech, 1); /* pour groupe avec dynamique mvt */
		if (ech <= echelle_temps_learn) apprend_groupes_rn(ech);
	}
}

/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/*----------------------------------------------------------------*/
/*            lecture du script du RN pour Promethe               */
/*             le nom du fichier est issus du fichier .res        */
/*             le nom du fichier script associe y est inscrit     */
/*                                                                */
/* Copyrights P. Gaussier 1995                                    */
/*                                                                */
/*  modif oct 2001 pour supprimer le fichier .draw et integrer    */
/*  la possibilite d'avoir une vitesse de simulation et un taux   */
/* d'apprentissage different par groupe                           */
/* modif oct 2004 gestion des commentaires dans le script         */
/*----------------------------------------------------------------*/

/*#define DEBUG*/
#include "public.h"
#include "outils_script.h"

#ifndef AVEUGLE
#include "gcd.h"
#endif

/* #define DEBUG 1 */

/* variables globales pour la gestion des commentaires non attaches a un groupe */
/* Il s'agit des commentaires precedent le debut des groupes et des commentaires */
/* precedent le debut des liens (si c'est possible! / correspond fin d'un groupe non?) */

type_noeud_comment *first_comment_group = NULL;
type_noeud_comment *first_comment_link = NULL;

extern int nbre_groupes_lus; /*defini dans le basic parser */
void lecture_script(char *local_nomfich1, int init)
{
    int i, no;
    FILE *local_f;
    static char ligne[TAILLE_CHAINE];

    nbre_groupes_lus=0;
    local_f = fopen(local_nomfich1, "r");
    if (local_f == NULL)
    {
        printf("\n Erreur lors de l'ouverture du fichier contenant le script \n");
        exit(0);
    }
    dprints("--- lecture du script: %s\n", local_nomfich1);
    first_comment_group = (type_noeud_comment *) read_line_with_comment(local_f, NULL, ligne);
    sscanf(ligne, "nombre de groupes = %d\n", &nbre_groupe);

    if (nbre_groupe > nb_max_groupes)
    {
        printf("ERREUR, la version actuelle ne marche qu'avec moins de %d groupes \n",nb_max_groupes);
        exit(0);
    }

    nbre_neurone = 0;
    for (i = 0; i < nbre_groupe; i++)
    {
        dprints("lecture groupe %d\n", i);
        no = read_one_group_promethe(local_f, def_groupe, nb_max_groupes);  /* numero du groupe lu */

        if (init == 1)
        {
          def_groupe[no].selected=0;
          def_groupe[no].breakpoint=0;
          def_groupe[no].video_ext = NULL;
          def_groupe[no].tactil_ext = NULL;
          def_groupe[no].audio_ext = NULL;
          def_groupe[no].ext = NULL;
          def_groupe[no].data = NULL;
          def_groupe[no].liste_types_neuromodulations = NULL;
          def_groupe[no].data_of_profiling = NULL;

        }

        nbre_neurone += def_groupe[no].nbre;
        /* ici mettre a jour max_ech_temps */
        if (def_groupe[no].ech_temps > max_ech_temps) max_ech_temps = def_groupe[no].ech_temps;
	      if(def_groupe[no].type == No_RTTOKEN) echelle_temps_rt[def_groupe[no].ech_temps]=1;

        dprints("grp %d ech_tps = %d \n", no, def_groupe[no].ech_temps);
    }
    
    dprints("max_ech_temps = %d \n", max_ech_temps);
    first_comment_link =(type_noeud_comment *) read_line_with_comment(local_f, NULL, ligne);
    sscanf(ligne, "nombre de liaisons = %d\n", &nbre_liaison);    dprints("nbre de liaisons = %d \n", nbre_liaison);

    liaison = creer_voies(nbre_liaison);
    for (i = 0; i < nbre_liaison; i++)
    {
      dprints("%s pointeur liaison = %p sizeof %d\n",__FUNCTION__,&liaison[i],sizeof(liaison[i]));
      read_one_link(local_f, &liaison[i], NULL, NULL);
      dprints("liaison %d entre %d et %d , type = %d , nbre = %d , norme = %f\n",i,
             liaison[i].depart, liaison[i].arrivee, liaison[i].type,
             liaison[i].nbre, liaison[i].norme);
    }
    fclose(local_f);
    init_input_link_matrix();
    printf("max_ech_temps = %d \n", max_ech_temps);
}

/** ATTENTION local_gcd n'est pas utilise */

void ecriture_script(char *local_nomfich1, char *local_gcd)
{
    int i;
    int comment = 1;            /* pour dire que l'on veut sauver les commentaires */
    FILE *local_f;
    (void) local_gcd;

    local_f = fopen(local_nomfich1, "w");
    if (local_f == NULL)
    {
        EXIT_ON_ERROR("\n Erreur lors de l'ouverture en ecriture du fichier script \n");
    }
    dprints("ecriture du script: %s\n", local_nomfich1);

    if (comment == 1 && first_comment_group != NULL) save_comment(local_f, first_comment_group);

    fprintf(local_f, "nombre de groupes = %d\n", nbre_groupe);
    for (i = 0; i < nbre_groupe; i++)
    {
        write_one_group(local_f, &(def_groupe[i]), comment, NULL);
    }

    if (comment == 1 && first_comment_link != NULL) save_comment(local_f, first_comment_link);

    fprintf(local_f, "nombre de liaisons = %d\n", nbre_liaison);

    for (i = 0; i < nbre_liaison; i++)
    {
        write_one_link(local_f, &liaison[i], comment);
    }
    fclose(local_f);
#ifndef AVEUGLE
    ecriture_gcd(local_gcd);
#endif

}

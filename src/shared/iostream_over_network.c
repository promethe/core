/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/* fonctions de bases pour faire des printfs et des scanfs en passant par le reseau */
/* Il suffit d'ouvrir un telnet sur le bon port pour derouter un printf sur ce port.*/
/* idem pour le scanf. Pour cela il faut employer des scanf et printf modifies.     */

/* PG. 2010 */

#include <stdio.h>
#include <errno.h>
#include <assert.h>
#include <unistd.h>
#include <semaphore.h>
#include "net_message_debug_dist.h"

int distant_terminal = 0;

void _debug(const char *fmt, ...)
{
#ifdef DEBUG
		va_list ap;
		va_start(ap, fmt);
		vprintf(fmt, ap);
		va_end(ap);
#else
		(void)fmt;
#endif
	}

/* socket IO kernel */
char kernel_buffer[MAXBUF];
int socket_kernel = -1;
int client_kernel = -1;
type_io_server_socket io_server_kernel; /* relie les donnees precedentes */

/* socket console promethe (pour les fonctions algo de USER) */
char console_buffer[MAXBUF];
int socket_console = -1;
int client_console = -1;
type_io_server_socket io_server_console; /* relie les donnees precedentes */

/* socket debug print */
char debug_buffer[MAXBUF];
int socket_debug = -1;
int client_debug = -1;
type_io_server_socket io_server_debug; /* relie les donnees precedentes */

void vkprints(const char *fmt, va_list ap)
{
	/*if (client_kernel > 0)
	{
		vsprintf(kernel_buffer, fmt, ap);
		send(client_kernel, kernel_buffer, strlen(kernel_buffer), MSG_NOSIGNAL);
	} else*/ vfprintf(stdout, fmt, ap);
	fflush(stdout);
}

void kprints(const char *fmt, ...) /* extern inline dans network_debug.h*/
{
	va_list ap;
	va_start(ap, fmt);
	vkprints(fmt, ap);
	va_end(ap);
}

void kscans(const char *fmt, ...) /* extern inline dans network_debug.h*/
{
	va_list ap;
	int args;
	(void)args;

	va_start(ap, fmt);
	/*if(client_kernel>0)
	{
		recv(client_kernel, kernel_buffer, MAXBUF, 0);
		vsscanf(kernel_buffer,fmt, ap);
	}
	else*/ args = vfscanf(stdin,fmt, ap);
	va_end(ap);
}

/*-------------------------------------------------------------------------------------*/

void cprints(const char *fmt, ...) /* extern inline dans network_debug.h*/
{
	va_list ap;

	va_start(ap, fmt);
	/*if(client_console>0)
	{
		vsprintf(console_buffer,fmt, ap);
		send(client_console,console_buffer,strlen(console_buffer),0);
	}
	else*/ vfprintf(stdout,fmt, ap);
	va_end(ap);
}

void cscans(const char *fmt, ...) /* extern inline dans network_debug.h*/
{
	va_list ap;
	int args;
	(void) args;

	va_start(ap, fmt);
	/*if(client_console>0)
	{
		recv(client_console, console_buffer, MAXBUF, 0);
		vsscanf(console_buffer,fmt, ap);
	}
	else*/ args = vfscanf(stdin,fmt, ap);
	va_end(ap);
}

/*------------------------------------------------------------------------------------*/
/* affichage des message de debug / desactive si pas de DEBUG demande a la compilation*/

void true_dprints(const char *fmt, ...) /* ne peut pas etre un inline / fct variadique... */
{
	va_list ap;
	va_start(ap, fmt);
#ifndef NO_SOCKET_DEBUG  /* si pas de socket possible on repasse au printf classique */
		if(client_debug>0)
		{
			vsprintf(debug_buffer,fmt, ap);
			send(client_debug,debug_buffer,strlen(debug_buffer),0);
		}
		else vfprintf(stdout,fmt, ap);
#else
		vfprintf(stdout,fmt, ap);
#endif
		va_end(ap);
	}

/*---------------------------------------------------------------------*/

void base_close_io_server(type_io_server_socket *io_server)
{
	int res;

	res = shutdown(*(io_server->client), 2);
	if (res != 0)
	{
		printf("erreur shutdown socket client %s / %s \n", io_server->name, io_server->comment);
	}
	res = close(*(io_server->client));
	if (res != 0)
	{
		printf("erreur close  socket client %s / %s \n", io_server->name, io_server->comment);
	}
	res = shutdown(*(io_server->socket_file), 2);
	if (res != 0)
	{
		printf("erreur shutdown socket socket_file %s / %s \n", io_server->name, io_server->comment);
	}
	res = close(*(io_server->socket_file));
	if (res != 0)
	{
		printf("erreur close  socket socket_file %s / %s \n", io_server->name, io_server->comment);
	}dprints("I/O socket closed for %s (port=%d)\n", io_server->comment, io_server->port);
	client_debug = -1;
}

void close_io_server(type_io_server_socket *io_server)
{
	io_server->state = CLOSED;

	if ((io_server->client != NULL) && (*(io_server->client) > 0))
	{
		base_close_io_server(io_server);
	}
}

/*-----------------------------------------------------------------------*/
void global_close_io_server()
{
  close_io_server(&io_server_debug); /* socket_debug, client_debug); */
  close_io_server(&io_server_console); /* (socket_console, client_console); */
  close_io_server(&io_server_kernel); /* (socket_kernel, client_kernel); */
  kprints("Telnet servers closed\n");
}

/*---------------------------------------------------------------------*/
/* thread gerant l'execution des entrees sorties d'un serveur generique*/
/** Pourquoi void * ??? AB.*/
void *thread_io_server(void *arg)
{
	socklen_t addrlen;
	char local_buffer[MAXBUF];
	type_io_server_socket *io_server = (type_io_server_socket *) arg;

	(io_server->gestionnaire_de_signaux)(); /*appel de gestion_mask_signaux(); du kernel*/

	addrlen = sizeof(io_server->client_addr);
	/*---accept a connection (creating a data pipe)---*/
	dprints("En attente de la connexion du client pour %s sur le port %d \n", io_server->comment, io_server->port);
	while (io_server->state == ACTIVATED) /* process all incoming clients */
	{
		*(io_server->client) = accept(*(io_server->socket_file), (struct sockaddr*) &io_server->client_addr, &addrlen);
		/* il ne faudrait pas continuer le accept si on veut tout arreter !!!! */

		dprints("%s:%d connected\n", inet_ntoa(io_server->client_addr.sin_addr), ntohs(io_server->client_addr.sin_port));
		sprintf(local_buffer, "Server promethe %s / %s : %s:%d connected\n", io_server->name, io_server->comment, inet_ntoa(io_server->client_addr.sin_addr), ntohs(io_server->client_addr.sin_port));
#ifdef DEBUG
		send(*(io_server->client), local_buffer, strlen(local_buffer), 0);
#endif

	}

	dprints("fermeture definitive du io_server pour %s \n", io_server->comment);
	/*base_close_io_server(io_server); La fermeture se fait deja par close_io_server*/

	/* quand l'execution est terminee */
	pthread_exit(NULL);
}

/* pour un controle par un simple telnet */
void open_io_server(type_io_server_socket *io_server, char *nom, int server_port, int *socket_file, int *client, const char *comment, void(*gestionnaire_de_signaux)())
{

	int yes = 0;
	pthread_attr_t thread_attr;
	int res;

	dprints("creation d'un serveur (%s) I/O text sur le port = %d \n", comment, server_port);
	/*---Create streaming socket---*/
	if ((*socket_file = socket(AF_INET, SOCK_STREAM, 0)) < 0)
	{
		printf("Creation Socket pour Kernel: open_text_server: %s", strerror(errno));
		exit(errno);
	}

	/*---Initialize address/port structure---*/
	bzero(&io_server->self, sizeof(io_server->self));
	io_server->self.sin_family = AF_INET;
	io_server->self.sin_port = htons(server_port);
	io_server->self.sin_addr.s_addr = INADDR_ANY;

	/*---Assign a port number to the socket---*/
	res = 1;
	while (res != 0)
	{
		res = setsockopt(*socket_file, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(yes)); /* evite le delai d'attente lorsque l'on relance le programme... */
		if (res != 0)
		{
			printf("echec changement attributs socket_file %s:  %s\n", comment, strerror(errno));
		}
		else
		{
			res = (bind(*socket_file, (struct sockaddr*) &io_server->self, sizeof(io_server->self)) != 0 );
			if (res != 0)
			{
				dprints("Creation socket--bind pour Kernel: open_text_server: %s", strerror(errno));
				server_port++;
				io_server->self.sin_port = htons(server_port);
				dprints("on va essayer sur le port %d \n", server_port);
			}
		}
	}

	/*---Make it a "listening socket"---*/
	if (listen(*socket_file, 20) != 0)
	{
		dprints("Creation socket--listen pour Kernel: open_text_server: %s", strerror(errno));
		exit(errno);
	}

	io_server->port = server_port;
	io_server->socket_file = socket_file;
	io_server->client = client;
	io_server->state = ACTIVATED;
	strcpy(io_server->comment, comment);
	strcpy(io_server->name, nom);
	io_server->gestionnaire_de_signaux = gestionnaire_de_signaux;

	pthread_attr_init(&thread_attr);
	res = pthread_create(&io_server->pthread, &thread_attr, thread_io_server, (void *) io_server);
	if (res != 0) EXIT_ON_ERROR("fealure on thread creation for %s %s : %s\n", io_server->name, io_server->comment, strerror(errno));
}


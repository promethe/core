/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier,
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software.
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license,
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability.
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge.
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured
and, more generally, to use and operate it in the same conditions as regards security.
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/

/* gestion de token temps reel*/
/* modifie P. Gaussier juillet 2006 */

/**
 *@ingroup NN_Core
 *@defgroup rt_token rt_token
 *
 * \section Description
 * Emet un jeton a interval regulier independemment de l'echelle dans laquelle il est. Si la chaine de groupe qui suis n'a pas le temps de s'executer alors certains groupes ne sont pas execute. Sinon utiliser "-w"
 * \section Options
 * - -t100: Periode d'execution en us (microsecondes) (ici 100us).
 * - -w :A utiliser avec un lien secondaire venant de la fin de l achaine suivant le rt_token.
 *       Force toute la chaine derriere le rt_token a s'executer jusqu'au lien secondaire.
 *       Un warning est emis si le delaiis a ete trop long.
 *
 *@file
 *@ingroup rt_token
 */

#include <sys/types.h>
#include <sys/time.h>
#include <unistd.h>
#include <stdlib.h>
#include <pthread.h>
#include <rttoken.h>
#include <time.h>
#include <errno.h>

/*#define DEBUG */

#include "public.h"
#include "outils.h"
#include "oscillo_kernel.h"
#if USE_ENET
#include "prom_enet_debug.h"
#endif
#include <Ivy/ivy.h>

#include "prom_tools/include/prom_bus.h"

typedef struct {
  int gpe; /**< Numero du groupe a activer */
  long timeout; /**< Intervalle de temps de generation du token en microsecondes (max 999 999)*/
} Parametres;

#ifndef AVEUGLE
#define DISPLAY_RT_ERROR(x,y) display_rt_error(x,y)
#else
#define DISPLAY_RT_ERROR(x,y)
#endif

#ifndef AVEUGLE

extern int taille_bloc_debug; /* vient de gestion_debug5.c */

void display_rt_error(int gpe, char c)
{
  TxPoint point;
  char buffer[255];
  int couleur;
  int rayon=0;

  /*effacer le groupe */
  kprints("display_rt_error %s (%d) %d  rayon=%d\n",def_groupe[gpe].no_name,gpe, taille_bloc_debug,rayon);

  point.x = def_groupe[gpe].p_posx -4;
  point.y = def_groupe[gpe].p_posy -4;
  TxDessinerRectangle(&fenetre1, noir, TxPlein, point, taille_bloc_debug + 6, taille_bloc_debug + 6, 0);
  rayon=(int)neurone[def_groupe[gpe].premier_ele].s1;

  point.x = def_groupe[gpe].p_posx + taille_bloc_debug/2;
  point.y = def_groupe[gpe].p_posy + taille_bloc_debug/2;
  if(c=='w') couleur=rouge;
  else if(c=='c') couleur=vert;
  else couleur=bleu;

  if(rayon<=4) rayon= taille_bloc_debug/2;
  else rayon=rayon-1;
  neurone[def_groupe[gpe].premier_ele].s=neurone[def_groupe[gpe].premier_ele].s1=neurone[def_groupe[gpe].premier_ele].s2=(float)rayon;

  TxDessinerCercle(&fenetre1, couleur, TxVide, point,rayon, 4);
  prom_bus_send_message("msg_rt(%d)", rayon);
  sprintf(buffer,"--- %c ---",c);
  point.x=point.x-strlen(buffer)*4;

  TxEcrireChaine(&fenetre1, couleur, point, buffer, NULL);
  TxDisplay(&fenetre1);
}
#endif

/* remet a zero le flag rttoken de tous les groupes suivant le groupe rttoken */
/* cela permet ensuite de les relancer aussi souvent qu'on le desire sans synchro */
/* avec le reste de l'echelle de temps. */
/* la presence d'une boite rttoken type toute les boites qui lui sont connectes en rttoken.*/
/* attention gros pb si boucle !!!!!!!!!!!!!!!! - recursion infinie  */
/* algo a corriger PG */

void attend_groupe_libre(int gpe)
{
  int etat, error;

  etat = 1;
  while (etat != 0) /* attend que le groupe ne soit plus utilise / ni ses champs modifies! */
  {
    error = sem_getvalue(&def_groupe[gpe].sem_lock_fields, &etat);
    if (error != 0) EXIT_ON_GROUP_ERROR(gpe, "la valeur de sem_lock_field n'a pue etre lue error=%d \n", error);
    dprints("***attend_groupe_libre()***  groupe %s etat = %d \n", def_groupe[gpe].no_name, etat);

  }
  return;
}

void reset_rttoken_protected(int gpe)
{
  int no_token;
  int i;

  no_token = def_groupe[gpe].rttoken;

  dprints("reset du token rt %d pour le gpe rttoken %d: gpes = ", no_token, gpe);
  if (no_token < 1)
  {
    kprints("WARNING: pas de reset de rttoken. Bizarre ... (peut etre 2 jetons rt de meme priorite? - ca ne marche pas)\n");
    return;
  }
  for (i = 0; i < nbre_groupe; i++) /* reset de la chaine temps reel consideree */
  {
    if (def_groupe[i].rttoken == no_token)
    {
      /* attend_groupe_libre(i); *//* xxxx modif PG  risque intro erreur, test 18 nov 2007 */
      /*	  printf(" %d ",i); */
      def_groupe[i].rttoken = 0;
      def_groupe[i].deja_active = 0;
    }
  }
  /*  printf("\n");*/
}

void reset_rttoken(int gpe)
{
#ifdef USE_THREADS
  pthread_mutex_lock(&mutex_section_critique_sequenceur);
#endif

  reset_rttoken(gpe);

#ifdef USE_THREADS
  pthread_mutex_unlock(&mutex_section_critique_sequenceur);
#endif
}

void print_diff_time(struct timeval *TimeTrace, struct timeval *StartTimeTrace)
{
  long int usec;
  long int secondes;

  usec = TimeTrace->tv_usec - StartTimeTrace->tv_usec;
  secondes = TimeTrace->tv_sec - StartTimeTrace->tv_sec;
  if (usec < 0)
  {
    secondes = secondes - 1;
    usec = 1000000 + usec;
  }
  kprints(" %ld min,  %ld s et  %ld usec\n", secondes / 60, secondes % 60, usec);
}

/*----------------------------------------------------------------------------*/
/* renvoie 0 si le reset n'est pas possible: cas continu                      */

int reset_rttoken2_protected(int gpe, int *warning_tab, int warning_nb, int *error_tab, int error_nb, int *continu_tab, int continu_nb, struct timeval *TimeTrace, struct timeval *StartTimeTrace, int nb_iter_local)
{
  int no_token;
  int i, j;
  int ech;

  no_token = def_groupe[gpe].type2; /*=def_groupe[gpe].rttoken;*/
  dprints("reset du token rt %d pour le gpe rttoken %s\n", no_token, def_groupe[gpe].no_name);
  if (no_token < 1)
  {
    kprints("WARNING: pas de reset de rttoken. Bizarre... \n");
    return 0;
  }

  for (i = 0; i < continu_nb; i++)
  {
    j = continu_tab[i];
    if (def_groupe[j].rttoken != no_token)
    {
      kprints("ERR iter = %d ", nb_iter_local);
      print_diff_time(TimeTrace, StartTimeTrace);
      kprints("ERR group %s RT_error because of group %s rttoken flag= %d instead of %d \n", def_groupe[gpe].no_name, def_groupe[j].no_name, def_groupe[j].rttoken, no_token);
      kprints("ERR pas de reset pour le rt_token, on continue en attendant le groupe %s (%d) \n", def_groupe[j].no_name, j);
      DISPLAY_RT_ERROR(gpe,'c');
      return 0;
    }
  }

  for (i = 0; i < warning_nb; i++)
  {
    j = warning_tab[i];
    if (def_groupe[j].rttoken != no_token)
    {
      kprints("WAR iter = %d ", nb_iter_local);
      print_diff_time(TimeTrace, StartTimeTrace);
      kprints("WAR group %s RT_warning because of group %s rttoken flag= %d instead of %d \n", def_groupe[gpe].no_name, def_groupe[j].no_name, def_groupe[j].rttoken, no_token);
      DISPLAY_RT_ERROR(gpe,'w');
    }
  }

  for (i = 0; i < error_nb; i++)
  {
    j = error_tab[i];
    if (def_groupe[j].rttoken != no_token)
    {
      kprints("iter = %d ", nb_iter_local);
      print_diff_time(TimeTrace, StartTimeTrace);
      EXIT_ON_GROUP_ERROR(gpe, "group %s RT_error because of group %s rttoken flag= %d instead of %d \n", def_groupe[gpe].no_name, def_groupe[j].no_name, def_groupe[j].rttoken, no_token);
    }
  }

  for (i = 0; i < nbre_groupe; i++) /* reset de la chaine temps reel consideree */
  {
    dprints("reset gpe %d , ", def_groupe[i].rttoken);
    if (def_groupe[i].rttoken == no_token)
    {
      dprints("reset rttoken deja_active gpe %s (%d) \n", def_groupe[i].no_name, i);
      /* attend_groupe_libre(i); *//* xxxx modif PG risque erreur, test 18 nov 2007 */
      def_groupe[i].rttoken = 0;
      def_groupe[i].deja_active = 0;
      /*rajout P.G*/
      def_groupe[i].deja_appris = 0;
      ech = def_groupe[i].ech_temps;
      if (ech <= echelle_temps_learn && global_learn == 1) apprend_groupe(i);
      def_groupe[i].deja_appris = 1;
    }
  }
  dprints("\n");
  return 1; /* tout est OK, reset effectue */
}

/* renvoie 0 si le reset n'est pas possible / cas continu */
int reset_rttoken2(int gpe, int *warning_tab, int warning_nb, int *error_tab, int error_nb, int *continu_tab, int continu_nb, struct timeval *TimeTrace, struct timeval *StartTimeTrace, int nb_iter_local)
{
  int val_retour;
#ifdef USE_THREADS
  dprints("mutex reset_rttoken %s \n",def_groupe[gpe].no_name);

  pthread_mutex_lock(&mutex_section_critique_sequenceur_attend); /*c'est rttoken qui est prioritaire pour le lock mutex_section_critique_sequenceur */
  pthread_mutex_lock(&mutex_section_critique_sequenceur); /* utilise par lance une vague parallele dans gestion_sequ12.c */
#endif

  val_retour = reset_rttoken2_protected(gpe, warning_tab, warning_nb, error_tab, error_nb, continu_tab, continu_nb, TimeTrace, StartTimeTrace, nb_iter_local);

#ifdef USE_THREADS
  pthread_mutex_unlock(&mutex_section_critique_sequenceur);
  pthread_mutex_unlock(&mutex_section_critique_sequenceur_attend);
  dprints("fin mutex reset_rttoken %s \n",def_groupe[gpe].no_name);
#endif
  return val_retour;

}

/* attention ces variables doivent devenir specifique du token et groupe associe ! */
struct timeval InputFunctionTimeTrace =
  { -1, -1 }, OutputFunctionTimeTrace;

int exec_cmd_rttoken_new(int gpe)
{
  int i, p, error;
  char retour[256];
  long periode;
  struct timespec timeout;
  long MicroSecondesFunctionTimeTrace;
  //long SecondesFunctionTimeTrace,
  int deb;

  dprints("exec_cmd_rttoken_new\n");
  dprints("deja active =%d \n", def_groupe[gpe].deja_active);

  /* premier appel on initialise a l'heure courante */
  if (InputFunctionTimeTrace.tv_sec < 0) gettimeofday(&InputFunctionTimeTrace, (void *) NULL );

  periode = 0.;
  for (i = 0; (liaison[i].arrivee != gpe) && (i < nbre_liaison); i++);
  if (liaison[i].arrivee == gpe)
  {
    if (prom_getopt(liaison[i].nom, "t", retour) != 2) EXIT_ON_GROUP_ERROR(gpe, "option -t sans valeur de periode (liaison[%d].nom=%s)",  i, liaison[i].nom);
    periode = atol(retour);
  }
  else EXIT_ON_GROUP_ERROR(gpe, "input link not found");


  gettimeofday(&OutputFunctionTimeTrace, (void *) NULL );
  if (OutputFunctionTimeTrace.tv_usec >= InputFunctionTimeTrace.tv_usec)
  {
    //SecondesFunctionTimeTrace = OutputFunctionTimeTrace.tv_sec - InputFunctionTimeTrace.tv_sec;
    MicroSecondesFunctionTimeTrace = OutputFunctionTimeTrace.tv_usec - InputFunctionTimeTrace.tv_usec;
  }
  else
  {
   // SecondesFunctionTimeTrace = OutputFunctionTimeTrace.tv_sec - InputFunctionTimeTrace.tv_sec - 1;
    MicroSecondesFunctionTimeTrace = 1000000 + OutputFunctionTimeTrace.tv_usec - InputFunctionTimeTrace.tv_usec;
  }

  timeout.tv_nsec = ((long) periode - MicroSecondesFunctionTimeTrace) * 1000;
  timeout.tv_sec = 0;
  if (timeout.tv_nsec < 0)
  {
    kprints("WARNING la constante de temps %ld n'a pu etre respectee \n", periode);
    kprints("Il manque %ld microsecondes \n", (long) (timeout.tv_nsec / 1000));
    sleep(1);
  }
  else
  {
    kprints("nanosleep de %ld micro secondes \n", timeout.tv_nsec / 1000);
    error = nanosleep(&timeout, NULL );
    if (error == -1)
    {
      kprints("pb dans nanosleep \n");
      kprints("errno = %d \n", errno);
    }
  }

  def_groupe[gpe].rttoken = def_groupe[gpe].type2; /*  numero correspondant a la priorite du token */
  reset_rttoken(gpe); /* il faudrait normalement le faire dans une boite speficique pour verifier */
  /* la contrainte rt sur la chaine complete */
  def_groupe[gpe].rttoken = def_groupe[gpe].type2; /*  numero correspondant a la priorite du token */
  deb = def_groupe[gpe].premier_ele;
  for (p = deb; p < deb + def_groupe[gpe].nbre; p++)
  {
    neurone[p].s = neurone[p].s1 = neurone[p].s2 = 0.5;
  }

  gettimeofday(&InputFunctionTimeTrace, (void *) NULL );

  return 1;
}

#define nb_max_error 255
#define nb_max_warning nb_max_error
#define nb_max_continu nb_max_error

/* fonctions appelee par defaut pour gerer le thread associe a un gpe */
/* toDo: - selection de la periode de mise ajour la plus courte lors de la fusion de 2 voies rt ?
 au lieu de propage le numero d'un des groupes arbitrairement.
 - mutex pour proteger le reset des tokens ?
 - prediction du timing moyen pour plus de regularite
 - ajout de fonctions pour la gestion de la fin d'une sequence temps reel */
void *rt_token_create_and_manage(void *p_arg_gpe)
{
  int gpe, fin;
  int val, res;
  int i, j, error;
  char retour[256];
  long periode, temps_sec, temps_usec;
  /*   int secondes;*/
  int warning_nb, error_nb, continu_nb;
  int warning_tab[nb_max_warning], error_tab[nb_max_error], continu_tab[nb_max_continu]; /* tab de no des groupes a tester pour verifier la contrainte rt */
  struct timespec timeout;
  long SecondesFunctionTimeTrace, MicroSecondesFunctionTimeTrace;
  /*char MessageFunctionTimeTrace[255];*/
  struct timeval my_InputFunctionTimeTrace =
    { -1, -1 }, my_OutputFunctionTimeTrace, StartTimeTrace;
  int nb_iter_local = 0;
  int succes;

  gestion_mask_signaux();

  gpe = ((arg_thread_grp *) p_arg_gpe)->gpe;
  def_groupe[gpe].return_value = 1; /* ca ne devrait pas servir pour ce type de groupe */

  dprints("Global: Lancement du thread rt pour le gpe %s \n", def_groupe[gpe].no_name);

  res = sem_init(&def_groupe[gpe].sem_wake_up, 0, 0);
  if (res != 0)  EXIT_ON_GROUP_ERROR(gpe, "res init sem_wake_up : %d \n", res);
  res = sem_init(&def_groupe[gpe].sem_ack, 0, 0);
  if (res != 0)  EXIT_ON_GROUP_ERROR(gpe, "res init sem_ack  res: %d \n", res);
  def_groupe[gpe].function_new(gpe);

  /*semaphore compteur signalant que le thread est bien cree*/
  sem_post(((arg_thread_grp *) p_arg_gpe)->p_is_launched);
  do
  {
    res = sem_wait(&def_groupe[gpe].sem_wake_up); /* attend qu'on le reveille */
    if (res < 0)
    {
      kprints("ERROR: pb pour le reveil du gpe %s res semaphore wake_up = %d\n", def_groupe[gpe].no_name, res);
      kprints("ou est le pb, diff compilateur et options... \n");
      /*  exit(EXIT_FAILURE); */
    }
  } while (res < 0);

  if (my_InputFunctionTimeTrace.tv_sec < 0)
  {
    gettimeofday(&my_InputFunctionTimeTrace, (void *) NULL );
    StartTimeTrace.tv_sec = my_InputFunctionTimeTrace.tv_sec;
    StartTimeTrace.tv_usec = my_InputFunctionTimeTrace.tv_usec;
  }

  warning_nb = 0;
  error_nb = 0;
  continu_nb = 0;
  periode = -1;
  j = 0;
  i = find_input_link(gpe, j);
  if (i == -1) printf("%s pas de lien pour gpe=%d gpe_name=%s \n", __FUNCTION__, gpe, def_groupe[gpe].no_name);
  while (i != -1)
  {
    dprints("%s gpe=%d gpe_name=%s liaison %d nom=%s \n", __FUNCTION__, gpe, def_groupe[gpe].no_name, i, liaison[i].nom);
    val = prom_getopt(liaison[i].nom, "t", retour); /* le lien donne la valeur de la periode */
    if (val > 0)
    {
      if (val != 2)   EXIT_ON_GROUP_ERROR(gpe, "option -t sans valeur de periode (liaison[%d].nom=%s)\n",  i, liaison[i].nom);
      else periode = atol(retour);
      kprints("periode =%ld usec\n", periode);
      kprints("nbre sec= %ld \n", periode / 1000000);
    }
    else
    {
      val = prom_getopt(liaison[i].nom, "w", retour); /* le lien correspond a un warning pour le rt */
      dprints("warning: liaision %d , val = %d \n", i, val);
      if (val > 0)
      {
        warning_tab[warning_nb] = liaison[i].depart;
        warning_nb++;
        dprints("nbre warning = %d \n", warning_nb);
        if (warning_nb >= nb_max_warning)  EXIT_ON_GROUP_ERROR(gpe, "trop de liens warning / taille tableau stokage. Augmenter nb_max_warning \n");
      }
      else
      {
        val = prom_getopt(liaison[i].nom, "e", retour); /*le lien correspond a une erreur pour le rt */
        dprints("error: liaision %d , val = %d \n", i, val);
        if (val > 0)
        {
          error_tab[error_nb] = liaison[i].depart;
          error_nb++;
          dprints("nbre error = %d \n", error_nb);
          if (error_nb >= nb_max_error)   EXIT_ON_GROUP_ERROR(gpe, "trop de liens error / taille tableau stokage. Augmenter nb_max_error \n");
        }
        else val = prom_getopt(liaison[i].nom, "c", retour); /*le lien correspond a un continue pour le rt */
        if (val > 0)
        {
          continu_tab[error_nb] = liaison[i].depart;
          continu_nb++;
          if (continu_nb >= nb_max_continu) EXIT_ON_GROUP_ERROR(gpe, "trop de liens error / taille tableau stokage. Augmenter nb_max_error \n");
        }
        else
        {
          kprints("Warning rt_token_create_and_manage %s (%d) :\n", def_groupe[gpe].no_name, gpe);
          kprints("Contenu du lien non reconnu: %s \n", liaison[i].nom);
        }
      }
    }
    j++;
    i = find_input_link(gpe, j);
  }

  if (periode < 0) EXIT_ON_GROUP_ERROR(gpe, "input link not found in rt_token_create_and_manage (no -txxxx argument for the period)\n");

  kprints("def_groupe[%s].type2=%d \n", def_groupe[gpe].no_name, def_groupe[gpe].type2);
  if (def_groupe[gpe].type2 < 1) def_groupe[gpe].type2 = 10000; /* par defaut si ce n'est pas fait la priorite du token est tres faible */

  /* boucle principale de la gestion du groupe RTTOKEN */
  fin = 0;
  while (fin == 0)
  {
    error = gettimeofday(&my_OutputFunctionTimeTrace, (void *) NULL );
    if (error != 0)
    {
      kprints("ERROR %d dans gettimeofday rt_token_create_and_manage \n", errno);
    }
    if (my_OutputFunctionTimeTrace.tv_usec >= my_InputFunctionTimeTrace.tv_usec)
    {
      SecondesFunctionTimeTrace = my_OutputFunctionTimeTrace.tv_sec - my_InputFunctionTimeTrace.tv_sec;
      MicroSecondesFunctionTimeTrace = my_OutputFunctionTimeTrace.tv_usec - my_InputFunctionTimeTrace.tv_usec;
    }
    else
    {
      SecondesFunctionTimeTrace = my_OutputFunctionTimeTrace.tv_sec - my_InputFunctionTimeTrace.tv_sec - 1;
      MicroSecondesFunctionTimeTrace = 1000000 + my_OutputFunctionTimeTrace.tv_usec - my_InputFunctionTimeTrace.tv_usec;
    }


    MicroSecondesFunctionTimeTrace = SecondesFunctionTimeTrace * 1000000 + MicroSecondesFunctionTimeTrace;
    MicroSecondesFunctionTimeTrace = temps_usec = ((long) periode - MicroSecondesFunctionTimeTrace); /* temps restant en usec */
    temps_sec = temps_usec / 1000000; /* temps restant en secondes entieres */
    temps_usec = temps_usec - temps_sec * 1000000; /* on enleve les secondes entieres aux microsecondes */
    timeout.tv_nsec = temps_usec * 1000;
    timeout.tv_sec = temps_sec;

    if (MicroSecondesFunctionTimeTrace < 0)
    {
      kprints("WARNING la constante de temps %ld n'a pu etre respectee \n", periode);
      if (timeout.tv_nsec < 0) kprints("Il manque %ld microsecondes \n", (long) (-timeout.tv_nsec / 1000));
      if (timeout.tv_sec < 0) kprints("Il manque %ld secondes \n", (long) (-timeout.tv_sec));
      DISPLAY_RT_ERROR(gpe,'e'); /*PG: ca manquait pour le debug*/
      /*  sleep(1); */
    }
    else
    {
      dprints("gpe %s : nanosleep de %ld micro secondes \n", def_groupe[gpe].no_name, timeout.tv_nsec/1000);
      error = nanosleep(&timeout, NULL );
      if (error == -1)
      {
        kprints("pb dans nanosleep \n");
        kprints("errno = %d \n", errno);
      }
    }

    gettimeofday(&my_InputFunctionTimeTrace, (void *) NULL );

    def_groupe[gpe].rttoken = def_groupe[gpe].type2; /* numero correspondant a la priorite du token */
    UPDATE_OSCILLO_KERNEL(gpe,0);
    succes = reset_rttoken2(gpe, warning_tab, warning_nb, error_tab, error_nb, continu_tab, continu_nb, &my_InputFunctionTimeTrace, &StartTimeTrace, nb_iter_local); /* on pourrait aussi le faire dans une boite speficique pour verifier */
    def_groupe[gpe].rttoken = def_groupe[gpe].type2; /*  numero correspondant a la priorite du token */

    def_groupe[gpe].deja_active = 1;
    UPDATE_OSCILLO_KERNEL(gpe, 1);
    sem_post(&attend_un_rt_token);
    nb_iter_local++; /* incremente le nbre de mise a jour pour le debug */


    if (succes == 0)  kprints("le reset pour le rt_token %s (%d) n'a pas ete effectue, on continue... \n", def_groupe[gpe].no_name, gpe);

    /* un message a ete envoye: choix de l'execution en fonction du message */
    /* message vide = execution normale */

    if (def_groupe[gpe].message[0] == 'k' || def_groupe[gpe].message[0] == 'i')
    {
      if (strcmp(def_groupe[gpe].message, "kill") == 0) fin = 1;
      else if (strcmp(def_groupe[gpe].message, "init") == 0)
      {
        kprints("init du thread du gpe %s \n", def_groupe[gpe].no_name);
        def_groupe[gpe].function_new(gpe);
      }
    }

    UPDATE_OSCILLO_KERNEL(gpe,2); /* fin de la mise a jour rttoken */
  } /* while attente fin */

  /* destruction du thread associe au groupe et liberation des semaphores associes */

  def_groupe[gpe].destroy(gpe); /* fonction pour la destruction des donnees / utilisateur */

  res = sem_destroy(&def_groupe[gpe].sem_wake_up);
  kprints("res destroy sem_wake_up gpe %s = %d \n", def_groupe[gpe].no_name, res);
  res = sem_destroy(&def_groupe[gpe].sem_ack);
  kprints("res destroy sem_ack gpe %s = %d \n", def_groupe[gpe].no_name, res);

  dprints("Global: Arret du thread rt pour le gpe %s \n", def_groupe[gpe].no_name);
  free(p_arg_gpe);
  pthread_exit(NULL );
}

/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/*------------------------------------------------------------*/
/*          SYSTEME D'INTERPRETATION                          */
/* focalisation de l'attention sur un point caracteristique   */
/* transfo log a partir de ce point                           */
/* memorisation de la vue associee a l'objet                  */
/* realisation du mouvement par sortie reseau muscul          */
/* mouvement commande par sortie vision                       */
/* integre la partie bas niveau                               */
/* possibilite de remise en cause des contours                */
/*  (c) Copyrights GAUSSIER Philippe  3 Avril 1991            */
/*                            1992,1993                       */
/*  pour HPstations avec graphisme , menus simplifies         */
/*                                                            */
/* nouvelle version tient compte du temps de mem des liaisons */
/* version du Janvier  1993                                   */
/*------------------------------------------------------------*/

/*------------------------------------------------------------*/
/* gere les echelles de temps dans le fichier  config         */
/*------------------------------------------------------------*/

/*------------------------------------------------------------*/
/* sequense meme le RN avec les jetons cirulants              */
/* liaisons secondaires ou non                                */
/*------------------------------------------------------------*/
#define DEBUG

#if DIAGNOSTIC
#undef USE_IVY
#endif

#include <stdio.h>
#include <math.h>
#include <sched.h>

#include "public.h"
#include "gestion_threads.h"

#include <locale.h>
#include "outils.h"
#include <signal.h>
#include <search.h>  /* pour l'initialisation de la table de hachage */

#include "net_message_debug_dist.h"

#ifndef AVEUGLE
#include "gestion_debug.h"
#include "gcd.h"
#endif

#include "libx.h"
#include <X11/Xlib.h>
#include <read_prt.h>
#include <init_comm.h>

#include "prom_enet_debug.h"

#include "oscillo_kernel.h"
#include "prom_tools/include/prom_bus.h"

#include "enet_network.h"
#include "pandora_connect.h"

#define VARIABLE_NAME_SIZE 64

extern void libhardware_open(char* dev_name);
extern void libhardware_close(void);
extern void libhardware_start(void);

struct default_values {
  int iconify;
  int start_mode;
} default_values;

pthread_mutex_t mutex_step_by_step = PTHREAD_MUTEX_INITIALIZER;

    int nbre_fichiers; /* utilises en arg du programme */

    /* PB: devrait disparaitre quand tout sera mis dans le user */
    extern void initialise_sortie_reseau(void);
   // extern void initialise_STM(void);
    gboolean send_control=FALSE;
    sem_t pandora_lock;
    sem_t enet_pandora_lock;
    int pandora_debug_mem_grp=0;
    int pandora_debug_mem_grp_premier_lanc=0;
    int pandora_debug_mem_grp_cloture=0;
    float variable_hasard = 0.;
    int fildes;
    char nomfich1[taille_chaine],
caract [
taille_chaine
], freseau[taille_chaine],
fscript[taille_chaine], fconfig[taille_chaine], fgcd[taille_chaine];
char argv_fichier[nb_fichiers][taille_chaine];
image p_im, p_im2;
int nx, ny, nz;

float vigilence = 1.; /* variable entre 0 et 1                        */
/* determine l'etat de vigilence du systeme     */
/* il s'agit d'une constante multiplicative     */
/* pour un seuil                                */

int global_learn; /* learn=1 learning mode, 0 otherwise */

/*Variable globale pour specifier le mode de depart :
 ONLINE-LEARNING/USE-ONLY
 ou
 CONTINUE-LEARNING/CONTINUE-USING
 presence dans libx.h
 */

int promethe_simulation_en_cours = 0;
int continue_simulation_status;
#define WAIT -1
#define START 0
#define CONTINUE 1
#define QUIT 2

/*----------------------------------------------------------------------*/
/*  echelles de temps dans le reseau                                    */
/*----------------------------------------------------------------------*/

int max_ech_temps = 0; /* on prend max lors de la lecture du script */
/* sert pour la gestion_sequ.h               */
/* evite calculs inutiles car repetitif ident */
int echelle_temps[nb_max_echelles]; /* nbre de cycles par echelle                */
int fin_echelle[nb_max_echelles];
/* lu dans le fichier config                 */
int echelle_temps_dynamique[nb_max_echelles]; /* vaut 1 si dynamique temporelle dans l'echelle de temps */
int echelle_temps_rt[nb_max_echelles]; /* vaut 1 si dynamique temporelle dans l'echelle de temps */

/*----------------------------------------------------------------------*/

int taille_standart_trans;

/* taille image dans laquelle est definit le pt foc */

int p_trace = 0; /* mode trace */

/*----------------------------------------------------------------------*/
/* #ifndef AVEUGLE */
int debug = 1;

#ifndef AVEUGLE
int gpe_courant_a_afficher; /* affiche lors de la mise a jour          */
int nbre_element_gpe_courant;
int premier_element_gpe_courant;

int inverse_video = 1; /* 1 pour inverser pour sortie imprimante  */
int rafraichissement_image = 1;

int demo = 0;
int rapide = 1; /* 1 lors interpretation n'affiche pas transfo */

int nbre_couleur_gris = 20;
/*----------------------------------------------------------------------*/
/* pointeurs sur des tableaux crees pour l'affichage des neurones       */
/* lorsque l'on clique sur un neurone, on peut ensuite voir toute       */
/*  ses liaisons                                                        */
/*----------------------------------------------------------------------*/

int *pos_n_x; /* position de tous les neurones dans l'image */
int *pos_n_y;
int position_neurones_deja_initialisee = 0;
#endif

/*----------------------------------------------------- ----------------*/

int n_octet;

int xmax_init, ymax_init; /* taille de l'image de depart         */
int xmax, ymax; /* taille de l'image traitee           */

/*----------------------------------------------------------------------*/

unsigned char *im, *im_recons, *im_recons2;
unsigned char *im4;

/*----------------------------------------------------------------------*/
/*               gestion des donnees relatives au script                */
/*----------------------------------------------------------------------*/

int nbre_groupe, nbre_liaison;

/*--------------------------------------------------------------------*/
/*       VARIABLES  GLOBALES A L'ENSEMBLE DU PROGRAMME                */
/*--------------------------------------------------------------------*/

type_groupe *deb_groupe, *fin_groupe;
type_liaison *deb_liaison, *fin_liaison;

type_tableau neurone; /*tableau contenant l'ens du reseau   */
type_tableau_pando info_neurone_pour_pando;
type_tableau_voies liaison;
type_matrice donnee;
type_matrice corrige;
type_matrice resultat;

type_noeud **groupe;
/*tableau permettant d'acceder aux differents groupes */

char nom_groupe[nb_max_groupes][taille_chaine]; /* nom associe a un gpe, 200 gpes max */
type_groupe def_groupe[nb_max_groupes];
type_pando_groupe para_pando_group[nb_max_groupes];

int input_link_number[nb_max_groupes][nb_max_liaisons_pour_un_groupe];

int nbre_donnee; /* nombre de vecteurs a etudier       */
int nbre_groupe; /* nombre de groupes de neuones compet */
int nbre_neurone; /* nombre de neurones dans le reseau  */
int nbre_entree; /* nbre de vecteurs utilises en entree */
int nbre_sortie; /* nbre de neurones utilises en sortie */

int cycle; /*nombre de cycles de calcul avec 1 ex */
float periode; /*periode moyennage entree            */

int nbre_couche;

float eps; /* epsilon pour la modif des coeff    */

GTimer *simulation_timer;

void init_images(void)
{
  im = (unsigned char *) malloc(sizeof(unsigned char) * n_octet);
  im_recons = (unsigned char *) malloc(sizeof(unsigned char) * n_octet);
  im_recons2 = (unsigned char *) malloc(sizeof(unsigned char) * n_octet);
  im4 = (unsigned char *) malloc(sizeof(unsigned char) * n_octet);
}

#ifndef DIAGNOSTIC
/*******************  Pour statistique et profiling des groupes **********************************************/

typedef struct wrap_appel_algo {
  int maximum_size;
  Statistic duration, period;
  struct timeval last_call;

  void (*original_appel_algo)(int);
  void (*original_new)(int gpe);
  void (*original_destroy)(int gpe);
  void (*original_save)(int gpe);
} Wrap_appel_algo;

void statistical_debug_display(Wrap_appel_algo *my_data, type_groupe *group)
{
  Statistic *stat;
  char variable_name[VARIABLE_NAME_SIZE];

  snprintf(variable_name, VARIABLE_NAME_SIZE, "%s:%s", group->no_name, "period");
  stat = &my_data->period;
  if (stat->size > 0)
  {
    prom_bus_send_message("stat(%s,%d,%f,%f,%f,%f)", variable_name, stat->size, stat->min, stat->max, stat->mean, stat->sigma2);
    stat->size = 0;
  }

  stat = &my_data->duration;
  snprintf(variable_name, VARIABLE_NAME_SIZE, "%s:%s", group->no_name, "duration");
  if (stat->size > 0)
  {
    prom_bus_send_message("stat(%s,%d,%f,%f,%f,%f)", variable_name, stat->size, stat->min, stat->max, stat->mean, stat->sigma2);
    stat->size = 0;
  }

}

void wrap_new(int index_of_group)
{
  Wrap_appel_algo *my_data;

  my_data = def_groupe[index_of_group].data_of_profiling;
  my_data->original_new(index_of_group);
}

void wrap_appel_algo(int index_of_group)
{
  float duration, period;
  Statistic *stat;
  struct timeval starting_time, now;
  Wrap_appel_algo *my_data;

  my_data = def_groupe[index_of_group].data_of_profiling;

  gettimeofday(&starting_time, NULL );
  my_data->original_appel_algo(index_of_group);
  gettimeofday(&now, NULL );

  duration = diff_timespec_in_ms(&starting_time, &now);
  statistic_add_item(&my_data->duration, duration);

  if (my_data->last_call.tv_sec != 0)
  {
    period = diff_timespec_in_ms(&my_data->last_call, &starting_time);
    statistic_add_item(&my_data->period, period);
  }

  memcpy(&my_data->last_call, &starting_time, sizeof(struct timeval));

  stat = &my_data->duration;
  if (stat->size >= (unsigned int) my_data->maximum_size && my_data->maximum_size != -1)
  {
    statistical_debug_display(my_data, &def_groupe[index_of_group]);
  }
}

void wrap_destroy(int index_of_group)
{
  Wrap_appel_algo *my_data;
  my_data = def_groupe[index_of_group].data_of_profiling;
  my_data->original_destroy(index_of_group);
  destroy_statistical_debug_group(index_of_group);
}

void wrap_save(int index_of_group)
{
  Wrap_appel_algo *my_data;
  my_data = def_groupe[index_of_group].data_of_profiling;
  my_data->original_save(index_of_group);
}

void new_statistical_debug_group(int index_of_group, int number_of_iterations)
{
  Wrap_appel_algo *my_data;

  if (promethe_simulation_en_cours == 1)
  {
    if (def_groupe[index_of_group].type == No_Fonction_Algo || def_groupe[index_of_group].type == No_Fonction_Algo_mvt)
    {
      if (def_groupe[index_of_group].appel_algo != wrap_appel_algo)
      {
        my_data = ALLOCATION(Wrap_appel_algo);
        my_data->duration.size = 0;
        my_data->period.size = 0;
        my_data->last_call.tv_sec = 0;
        my_data->last_call.tv_usec = 0;

        my_data->original_new = def_groupe[index_of_group].function_new;
        my_data->original_appel_algo = def_groupe[index_of_group].appel_algo;
        my_data->original_destroy = def_groupe[index_of_group].destroy;
        my_data->original_save = def_groupe[index_of_group].save;

        def_groupe[index_of_group].data_of_profiling = my_data;
        def_groupe[index_of_group].function_new = wrap_new;
        def_groupe[index_of_group].appel_algo = wrap_appel_algo;
        def_groupe[index_of_group].destroy = wrap_destroy;
        def_groupe[index_of_group].save = wrap_save;

        prom_bus_send_message("Start profiling group '%s' each %d iterations.", def_groupe[index_of_group].no_name, number_of_iterations);
      }
      else
      {
        my_data = def_groupe[index_of_group].data_of_profiling;
        statistical_debug_display(my_data, &def_groupe[index_of_group]);
      }
      my_data->maximum_size = number_of_iterations;

    }
    else prom_bus_send_message("Group %d cannot be profiled it is not a group of type algo.", def_groupe[index_of_group].no_name);
  }
  else prom_bus_send_message("The simulation is not running, profiling of group '%s' is not possible.", def_groupe[index_of_group].no_name);
}

void destroy_statistical_debug_group(int index_of_group)
{
  type_groupe *group;
  Wrap_appel_algo *my_data;

  group = &def_groupe[index_of_group];

  if (group->appel_algo == wrap_appel_algo)
  {
    my_data = group->data_of_profiling;

    group->function_new = my_data->original_new;
    group->appel_algo = my_data->original_appel_algo;
    group->destroy = my_data->original_destroy;
    group->save = my_data->original_save;

    prom_bus_send_message("Stop profiling group '%s'", def_groupe[index_of_group].no_name);
    statistical_debug_display(my_data, &def_groupe[index_of_group]);

    free(my_data);

  }
  else prom_bus_send_message("Group '%s' was not profiled ! The profiling cannot be stopped.", def_groupe[index_of_group].no_name);
}
/*************************            End profiling *****************************************************/
#endif /* DIAGNOSTIC */

/* thread dans lequel est lancee la simulation. Indispensable pour
 un affichage graphique sans pb du debug et des differentes images
 Permet de recuperer la main dans le gestionnaire de fenetre pour lancer
 d'autres options. Tres gourmant en ressources calcul... */

void *simulation_thread(void *arg)
{
  (void) arg;
  promethe_simulation_en_cours = 1;
  gestion_mask_signaux();
  sem_init(&attend_un_rt_token, 0, 0); /* semaphore utilise dans gestion_sequ.c pour attendre un rt_temps_reel lorsqu'aucune
   boite ne peut s'excuter a une echelle de temps donnee. */

#ifndef SEQ_AUTONOME
  if (global_learn != 1) mise_a_jour_des_sous_echelles(max_ech_temps);
  else mise_a_jour_des_sous_echellesb(max_ech_temps);
#endif
#ifdef SEQ_AUTONOME
  kprints("Autonomous sequencer start !!!\n");
  init_sequenceur_autonome();
#endif

  fin_simulation_utilisateur();

#ifndef DIAGNOSTIC
  prom_bus_change_state(No_Not_Running);
#endif

#ifndef SEQ_AUTONOME
#ifdef USE_THREADS_2
  terminate_each_group_as_a_thread();
#endif
  promethe_simulation_en_cours = 0;
  pthread_exit(NULL );
#endif
  promethe_simulation_en_cours = 0;
  return NULL ;
}

/* initialise tous les groupes dans le cas ou l'on utilise pas les threads2 (anciennes versions) */

void init_groupes(void)
{
  int gpe;

  for (gpe = 0; gpe < nbre_groupe; gpe++)
  {
    def_groupe[gpe].function_new(gpe);
  }
}

/* Pour tous les debuts de simuls meme s'il s'agit de continuer une simul. */
/* On perd le temps de relire le script et le reste au cas ou l'utilisateur voudrait faire de petits changements...*/

void first_step_begin_simulation(void)
{
#ifndef DIAGNOSTIC

  int mode;
#endif

  promethe_simulation_en_cours = 1;

  lecture_config(fconfig);
  lecture_script(fscript, 0);

#ifndef AVEUGLE
  lecture_gcd(fgcd);
#endif

  initialisation_des_groupes();/* PG: est ce que ca ne devrait pas aller dans le new a terme?  */
#ifndef SEQ_AUTONOME
#ifdef USE_THREADS_2
  launch_each_group_as_a_thread(); /* le new est integre */
#else
  init_groupes(); /* fait le new pour chaque groupe sequentiellement */
#endif
#endif

#ifndef DIAGNOSTIC
   libhardware_start();
#endif

  initialise_gaussiennes();

  g_timer_start(simulation_timer);
#ifndef DIAGNOSTIC
  if (global_learn == 1) mode = No_Learn_and_Use;
  else mode = No_Use_Only;
  prom_bus_change_state(mode);
#endif
}

void begin_simulation(void)
{
  /*#ifndef AVEUGLE*/
  pthread_t un_thread;
  int res;
#ifndef DIAGNOSTIC
  int mode;

  if (global_learn == 1) mode = No_Learn_and_Use;
  else mode = No_Use_Only;

  prom_bus_change_state(mode);
#endif

  continue_simulation_status = START;

#ifndef SEQ_AUTONOME
#ifdef USE_THREADS
  printf("USE_THREADS\t");
#endif
#ifdef USE_THREADS_2
  printf(" USE_THREADS_2");
#endif
#endif
  printf("\n");

  first_step_begin_simulation();
  /* la suite est specifique a un premier lancement  */

  initialise_sortie_reseau();

#ifndef AVEUGLE
  modif_coeff(NULL, NULL); /* affiche la fenetre avec selection pas a pas, abort... */
#endif

  res = pthread_create(&un_thread, NULL, simulation_thread, NULL );
  res = pthread_detach(un_thread);
  if (res != 0) printf("Echec detach simulation_thread\n");
}

void continue_simulation(void)
{
  pthread_t un_thread;
  int res;

  promethe_simulation_en_cours = 1;
  continue_simulation_status = CONTINUE;

#ifdef DEBUG
  printf("global learn = %d \n", global_learn);
#endif

  first_step_begin_simulation();
#ifndef AVEUGLE
  modif_coeff(NULL, NULL); /* affiche la fenetre avec selection pas a pas, abort... */
#endif

  res = pthread_create(&un_thread, NULL, simulation_thread, NULL );
  res = pthread_detach(un_thread);

  if (res != 0) PRINT_WARNING("Echec detaching thread");
}

/*----------------------------------------------------------------------------*/
/* ces fcts sont appelees par l'IHM et au demarrage mode graphique ou aveugle */

void set_learn_and_use_mode(DECLARATION_PARAMS_CALLBACK)
{
  (void) data;
  (void) widget;
  if (promethe_simulation_en_cours > 0)
  {
    kprints("Une simulation est deja en cours ! (arretez la d'abord)\n");
    return;
  }
  global_learn = 1;
  begin_simulation();
}

void set_use_only_mode(DECLARATION_PARAMS_CALLBACK)
{
  (void) data;
  (void) widget;
  if (promethe_simulation_en_cours > 0)
  {
    kprints("Une simulation est deja en cours ! (arretez la d'abord)\n");
    return;
  }
  global_learn = 0;
  begin_simulation();
}

void set_continue_learning_mode(DECLARATION_PARAMS_CALLBACK)
{
  (void) data;
  (void) widget;
  if (promethe_simulation_en_cours > 0)
  {
    printf("Une simulation est deja en cours ! (arretez la d'abord)\n");
    return;
  }
  global_learn = 1;
  continue_simulation();
}

void set_continue_only_mode(DECLARATION_PARAMS_CALLBACK)
{
  (void) data;
  (void) widget;
  if (promethe_simulation_en_cours > 0)
  {
    kprints("Une simulation est deja en cours ! (arretez la d'abord)\n");
    return;
  }
  global_learn = 0;
  continue_simulation();
}

/*----------------------------------------------------------------------------*/

void ecriture_reseaub(DECLARATION_PARAMS_CALLBACK)
{
  (void) data;
  (void) widget;
  ecriture_reseau(freseau, fscript);
}

/* on ne detruit pas vraiment les groupes */
/* on lance seulement la fonction proposee par l'utilisateur lors de la fin de la simulation */
/* enregistrement de donnees, liberation memoire ... */

void destroy_groupes(void)
{
  int gpe;

  for (gpe = 0; gpe < nbre_groupe; gpe++)
  {
    def_groupe[gpe].destroy(gpe);
  }
}

/* arret "rapide de la simulation. On finit l'iteration en cours pour eviter des incoherences.*/
int cancel_pressed(DECLARATION_PARAMS_CALLBACK)
{
  int i;
  (void) data;
  (void) widget;

  p_trace = 0;
  for (i = 0; i <= max_ech_temps; i++)
    echelle_temps[i] = 0;
  kprints("Canceling the simulation\n");

#ifndef DIAGNOSTIC
  prom_bus_change_state(No_Not_Running);
#endif

  return (TRUE);
}

extern int memoire_etat_step_by_step;

/* passage en mode step by step */
int step_by_step_pressed(DECLARATION_PARAMS_CALLBACK)
{
#ifdef USE_THREADS
  pthread_mutex_lock(&mutex_step_by_step);
#endif
  (void) data;
  (void) widget;

  if (p_trace == 0)
  {
    p_trace = 2;
    memoire_etat_step_by_step = etat_step_by_step;
    kprints("Detailled step by step mode activated\n");
  }
  else
  {
    if (etat_step_by_step == 1) etat_step_by_step = 2;
    else if (etat_step_by_step == 2) etat_step_by_step = 1;
  }

#ifdef USE_THREADS
  pthread_mutex_unlock(&mutex_step_by_step);
#endif

  return (TRUE);
}

/* quitte le mode step by step */
int continue_pressed(DECLARATION_PARAMS_CALLBACK)
{
  (void) data;
  (void) widget;
#ifdef USE_THREADS
  pthread_mutex_lock(&mutex_step_by_step);
#endif

  p_trace = 0; /* arret du mode pas a pas / trace */

  kprints("Stop the step by step mode %d %d\n", etat_step_by_step, memoire_etat_step_by_step);

#ifdef USE_THREADS
  pthread_mutex_unlock(&mutex_step_by_step);
#endif

  return (TRUE);
}

/* ajout - retrait d'un breakpoint */
void *breakpoint_pressed_threaded(void *ptr/*DECLARATION_PARAMS_CALLBACK*/)
{
  (void) ptr;

#ifndef AVEUGLE
  if (selected_group < 0)
  {
    kprints("%s : Cannot insert/delete a breakpoint: no group selected \n", __FUNCTION__);
    pthread_exit(NULL);
    return NULL;
  }
  if (def_groupe[selected_group].selected > 0)
  def_groupe[selected_group].selected = 0;
  if (def_groupe[selected_group].breakpoint > 0)
  {
    kprints("Breakpoint suppressed on group %d\n", selected_group);
    def_groupe[selected_group].breakpoint = 0;
    affiche_breakpoint(&fenetre1, selected_group, FALSE);
  }
  else
  {
    kprints("Breakpoint added on group %d\n", selected_group);
    def_groupe[selected_group].breakpoint = 1;
    affiche_breakpoint(&fenetre1, selected_group, TRUE);
    /*  affiche_rn(&fenetre1);  */
  }
  affiche_selected_group(&fenetre1, selected_group, FALSE);
  selected_group = -1;
#endif
  pthread_exit(NULL );
  return NULL ;
}

/* ajout - retrait d'un breakpoint */
int breakpoint_pressed(DECLARATION_PARAMS_CALLBACK)
{
  pthread_t un_thread;
  int res;
  (void) data;
  (void) widget;

  res = pthread_create(&un_thread, NULL, breakpoint_pressed_threaded, NULL );
  if (res != 0)
  {
    PRINT_WARNING("Error with creating a new thread");
    return (FALSE);
  }
  return (TRUE);
}
/* partie execution de simulation_b() partagee avec la procedure de lancement automatique au demarrage */

void simulation_b_exec()
{
#ifndef DIAGNOSTIC
  int mode;
#endif

  if (continue_simulation_status == QUIT) return;
  promethe_simulation_en_cours = 1;

  lecture_config(fconfig);
  lecture_script(fscript, 0);
#ifndef AVEUGLE
  lecture_gcd(fgcd);
#endif

#ifndef DIAGNOSTIC
  if (global_learn == 1) mode = No_Learn_and_Use;
  else mode = No_Use_Only;
  prom_bus_change_state(mode);
#endif

  initialisation_des_groupes(); /* fonction appelee par user/src/NN_Core/main.c definie l'ensemble des groupes */
#ifndef SEQ_AUTONOME
#ifdef USE_THREADS_2
  launch_each_group_as_a_thread();
#else
  init_groupes();
#endif
#endif

#ifndef DIAGNOSTIC
  libhardware_start();
#endif

  initialise_gaussiennes();

  if (continue_simulation_status == START)
  {
    initialise_sortie_reseau();
  }
  sem_init(&attend_un_rt_token, 0, 0); /* semaphore utilise dans gestion_sequ.c pour attendre un rt_temps_reel lorsqu'aucune
   boite ne peut s'excuter a une echelle de temps donnee. */

#ifndef SEQ_AUTONOME
  if (global_learn != 1) mise_a_jour_des_sous_echelles(max_ech_temps);
  else mise_a_jour_des_sous_echellesb(max_ech_temps);
#endif
#ifdef SEQ_AUTONOME
  kprints("GO !!!!!!!!!!!\n");
  init_sequenceur_autonome();
#endif

  fin_simulation_utilisateur();
  promethe_simulation_en_cours = 0;

#ifndef DIAGNOSTIC
  prom_bus_change_state(No_Not_Running);
#endif

#ifndef SEQ_AUTONOME
#ifdef USE_THREADS_2
  terminate_each_group_as_a_thread();
#else
  destroy_groupes();
#endif
#endif
}

void simulationb(DECLARATION_PARAMS_CALLBACK)
{
  int reponse, pb_scanf = 0;
  (void) data;
  (void) widget;

  if (continue_simulation_status == QUIT) return;

  kprints("-----------------------------------------------\n\n");
  kprints(" BLIND SIMULATION \n\n");
#ifndef SEQ_AUTONOME
#ifdef USE_THREADS
  kprints(" USE_THREADS   \n");
#endif
#ifdef USE_THREADS_2
  kprints(" USE_THREADS_2 \n");
#endif
#endif
  if (continue_simulation_status == WAIT)
  {
    do
    {
      kprints("\nMode lancement:\n 0: USE ONLY\n 1: ONLINE LEARNING\n 2: CONTINUE USING\n 3: CONTINUE LEARNING\n--------\nVotre choix: ");
      kscans("%d", &reponse);

      pb_scanf = 0;
      if (reponse == 0 || reponse == 2) global_learn = 0;
      else if (reponse == 1 || reponse == 3) global_learn = 1;
      else
      {
        kprints("erreur dans le choix\n");
        pb_scanf = 1;
      }
    } while (pb_scanf == 1);

    if (continue_simulation_status == QUIT) return;
    if (reponse == 0 || reponse == 1) continue_simulation_status = START;
    else if (reponse == 2 || reponse == 3) continue_simulation_status = CONTINUE;
  }

  if (global_learn < 0 || global_learn > 1) return;

  simulation_b_exec();
}

#ifdef AVEUGLE
/* menu en mode texte seulement pour la version aveugle */
void menu(void)
{
  int choix = 0;
  while (choix != 3 && continue_simulation_status != QUIT)
  {
    if (continue_simulation_status == WAIT)
    {
      kprints("Promethe: \n\n");
      kprints(" 1 - Simulation\n");
      kprints(" 2 - Ecriture du reseau\n");
      kprints(" 3 - Fin\n");
      kprints(" 4 - Remote debug\n");
      kprints("\n\n Votre choix : ");
      kscans("%d", &choix);
    }
    else if (continue_simulation_status == START || continue_simulation_status == CONTINUE)
    {
      /* == START or CONTINUE */
      choix = 1;
    }
    if (continue_simulation_status == QUIT) choix = -1; /* status can change in the middle*/
    switch (choix)
    {
    case 1:
      simulationb(NULL, NULL );
      /* pas thread safe */
      if (continue_simulation_status != QUIT) continue_simulation_status = WAIT;
      break;
    case 2:
      ecriture_reseaub(NULL, NULL );
      break;
    case 4:
      while (1)
        usleep(1000000);
      break;
    }
  }
  kprints("\n\nBye Bye...\n");

  if (continue_simulation_status == QUIT)
  {
    /* QUIT ordered by promethe_quit */

    sleep(42); /* wait until promethe_quit() kills the process*/
  }

  exit(0); /* promethe_quit() execute avant exit grace a atexit */
}
#endif

/*ATTENTION : n'appele que des fcts safe au sens des threads et signaux POSIX dans ce gestionnaire,
 sinon comportement indefini (entre autres aucone fct avec malloc printf des variables static
 ou des variables globales utilisee autrement qu'en lecture seule*/

#define max_parallele 256
extern int gpes_en_parallele[max_parallele];

void gestionnaire_signaux(int numero)
{
  (void) numero;
#ifndef DIAGNOSTIC
  switch (numero)
  {
  case SIGUSR1: /*enregistre le .res */
    ecriture_reseau(freseau, fscript);
    break;

	case SIGTERM:
		exit(0);
		break;

  case SIGINT:
    exit(0); /* promethe_quit() execute avant exit grace a atexit */
    break;
  case SIGABRT: /* Error raised by EXIT_ON_ERROR */
    exit(2);
    break;
  case SIGSEGV:
  {
    int i, indice, last_indice;
    last_indice = gpes_en_parallele[0];

    kprints("SEGFAULT\n");
    kprints("Liste des threads en traitement: \n");

    for (i = 0; i < max_parallele; i++)
    {
      indice = gpes_en_parallele[i];
      if (indice < last_indice || last_indice < 0) break;
      kprints("i=%d --- gpe=%d, no_name=%s , name %s\n", i, indice, def_groupe[indice].no_name, def_groupe[indice].nom);
      last_indice = indice;
    }
    kprints("fin de la pile des threads en traitement \n");
    exit(1);
    break;
  }
  default:
  {
    kprints("signal %d capture par le gestionnaire mais non traite... revoir le gestinnaire ou le remplissage de sigaction\n", numero);
    break;
  }
  }
#endif
}

/*---------------------------------------------------------------*/
/*                      PROG PRINCIPAL                           */
/*---------------------------------------------------------------*/

#ifndef DIAGNOSTIC
extern void main_loop_ivy(int argc, char *argv[NB_MAX_ARG]);
#endif

#define NB_MAX_ARG 20 /* attention def dans main.c */

void prom_kernel_print_version()
{
  kprints("kernel\n");
  kprints("======\n");
  kprints("Svn version: %s\n", STRINGIFY_CONTENT(SVN_REVISION));
  kprints("enet:   %s\n", STRINGIFY_CONTENT(USE_ENET));
}

#ifndef AVEUGLE
gboolean on_start(gpointer user_data)
{
  (void)user_data;

  if (default_values.iconify >= 0)
  {
    if(!(default_values.iconify&1)) gtk_window_iconify(GTK_WINDOW(fenetre1.window));
    if(!(default_values.iconify&2)) gtk_window_iconify(GTK_WINDOW(fenetre3.window));
    if(!(default_values.iconify&4)) gtk_window_iconify(GTK_WINDOW(fenetre2.window));
    if(!(default_values.iconify&8)) gtk_window_iconify(GTK_WINDOW(image1.window));
    if(!(default_values.iconify&16)) gtk_window_iconify(GTK_WINDOW(image2.window));
  }

  gdk_threads_enter(); /* Lock sur les appels Gtk*/
  switch (default_values.start_mode)
  {
    case 0: set_learn_and_use_mode(NULL, NULL); break;
    case 1: set_use_only_mode(NULL, NULL);break;
    case 2: set_continue_learning_mode(NULL,NULL);break;
    case 3: set_continue_only_mode(NULL,NULL);break;
    default: break;
  }
  gdk_threads_leave(); /* Liberation du lock */
  affiche_rn(&fenetre1);
  return 0;
}
#endif

void promethe_main(int argc, char *argv[NB_MAX_ARG])
{
#ifndef DIAGNOSTIC
  prt_t *prt = NULL;
#endif

  char char_adr_ip[1024], *buffer;
  int i;

#ifndef AVEUGLE
  char c;
#endif

  struct sigaction action;
  signal(SIGPIPE, SIG_IGN );
  hcreate(10000); /* table de hachage pour les no de groupes symboliques */

#ifndef AVEUGLE
  XInitThreads();
  gtk_init(&argc, &argv); /* mis au plus tot pour eviter les warnings */
  gdk_threads_init(); /* Called to initialize internal mutex "gdk_threads_mutex".*/
#endif

  atexit(promethe_quit); /* initialise l'appel de promethe_quit avant chaque exit */

  clock();

  simulation_timer = g_timer_new();

  if (sem_init(&(pandora_lock), 0, 1) != 0) PRINT_WARNING("Critical error ! Fail to init semaphore !");
  if (sem_init(&(enet_pandora_lock), 0, 1) != 0) PRINT_WARNING("Critical error ! Fail to init semaphore !");

  if (argc < No_arg_config + 1) EXIT_ON_ERROR("Nombre d'arguments incorrect ! \n promethe reseau.script reseau.res reseau.config reseau.dev reseau.gcd  ...\n");

  for (i = 0; i < argc; i++)
    strcpy(argv_fichier[i], argv[i]);
  nbre_fichiers = argc;
  /* je ne sais pas qui a ecrit ca mais c'est tres dangereux si l'on a d'autres arguments ... PG */
  /* En plus c'est incompatible avec le mode aveugle. Je rajoute un ifndef ...  en attendant mieu */

  if (argc > No_arg_config) strcpy(nomfich1, argv[No_arg_config]); /* pourquoi ???? PG : ce n'est pas le 4. A quoi ca sert ici dans le kernel ? */

  strcpy(fscript, argv[No_arg_script]);
  strcpy(freseau, argv[No_arg_res]);
  strcpy(fconfig, argv[No_arg_config]);

#ifndef DIAGNOSTIC
  open_io_server(&io_server_kernel, fscript, KERNEL_SERVER_PORT + atoi(argv[No_arg_telnet]), &socket_kernel, &client_kernel, "client_kernel", gestion_mask_signaux);
  open_io_server(&io_server_debug, fscript, DEBUG_SERVER_PORT + atoi(argv[No_arg_telnet]), &socket_debug, &client_debug, "client_debug", gestion_mask_signaux);
  open_io_server(&io_server_console, fscript, CONSOLE_SERVER_PORT + atoi(argv[No_arg_telnet]), &socket_console, &client_console, "client_console_user", gestion_mask_signaux);
  if (strlen(argv[No_arg_dev]) != 0) libhardware_open(argv[No_arg_dev]);

  main_loop_ivy(argc, argv); /* lancement du bus ivy */
  prt = prt_init(argv[No_arg_prt], argv[No_arg_VirtualNetwork]);
  init_comm(prt);
  enet_network_init();

#endif

  setlocale(LC_ALL, "C");

  ny = 512; /* taille max des images acceptees */
  nx = 512;
  nz = 1;
  n_octet = ny * nx * nz;

  for (i = 0; i < nb_max_echelles; i++) /* initialisation des tableaux pour eviter des pbs... */
  {
    echelle_temps[i] = 0;
    echelle_temps_dynamique[i] = 0;
    echelle_temps_rt[i] = 0;
  }

  lecture_script(fscript, 1);
  lecture_config(fconfig);
  lecture_reseau(freseau);

#ifndef AVEUGLE
  (void)c;
  if (argv[No_arg_DebugState][0] != '\0') sscanf(argv[No_arg_DebugState], "%d", &i);
  else i = 1;

  if (i < 0) check_debug(NULL, 2, NULL);
  else if (i == 0) check_debug(NULL, 3, NULL);
  else if (i < 3) check_debug(NULL, i, NULL);
  else if (i == 3) check_debug(NULL, 0, NULL);

  sprintf(fenetre1.name, "PROMETHE2: %s", argv[No_arg_script]);
  create_fenetre1();
  sprintf(fenetre2.name, "Fenetre2: %s", argv[No_arg_script]);
  create_fenetre2();
  sprintf(image1.name, "Image1:  %s", argv[No_arg_script]);
  create_fenetre3();
  sprintf(image2.name, "Image2:  %s", argv[No_arg_script]);
  create_fenetre4();

  if (argv[No_arg_gcd][0] != '\0')
  {
    strcpy(fgcd, argv[No_arg_gcd]);
    lecture_gcd(fgcd);
  }
  else strcpy(fgcd, "");

  gpe_courant_a_afficher = 1;
  nbre_element_gpe_courant = def_groupe[gpe_courant_a_afficher].nbre;
  premier_element_gpe_courant = def_groupe[gpe_courant_a_afficher].premier_ele;
#endif

  initialisation_des_groupes();
  kprints("\n----------------------------------------------------\n");
  kprints("Vigilence: %f\n", vigilence);
  kprints("Cycles par fenetre : %d\n", cycle);
  kprints("Periodes pour le moyennage: %f\n", periode);
  kprints("Epsilon: %f\n", eps);
  kprints("----------------------------------------------------\n\n");

  xmax = ymax = xmax_init = ymax_init = 256;
  init_images();

  /*mise en place du gestionnaire de signaux (cf aussi generic_create_and_manage*/
  /*attention : fscript etc... doivent etre initialises*/

  kprints("PID: %ld\n", (long) getpid());
  sigfillset(&action.sa_mask);
  action.sa_handler = gestionnaire_signaux;
  action.sa_flags = 0;
  if (sigaction(SIGUSR1, &action, NULL ) != 0) PRINT_WARNING("ERR signal SIGUSR1 non capture par promethe pour la sauvegarde du reseau\n");
  if (sigaction(SIGINT, &action, NULL ) != 0) PRINT_WARNING("ERR signal SIGINT non capture par promethe pour tout fermer proprement\n");

  buffer = getenv("DEBUG"); /* channel id global var */
  if (buffer == NULL ) sprintf(char_adr_ip, "127.0.0.1");
  else sprintf(char_adr_ip, "%s", buffer);

  if (sigaction(SIGSEGV, &action, NULL ) != 0) PRINT_WARNING("ERR signal SIGSEGV non capture par promethe... :(\n");
	if (sigaction(SIGTERM, &action, NULL ) != 0) PRINT_WARNING("ERR signal SIGTERM non capture par promethe... :(\n");

#ifndef AVEUGLE

  init_rgb_colormap();
  init_rgb_colormap16M();
  gdk_set_show_events(TRUE);

  gtk_widget_show_all(fenetre1.window);
  gtk_widget_show_all(fenetre2.window);
  gtk_widget_show_all(image1.window);
  gtk_widget_show_all(image2.window);

  sscanf(argv[No_arg_Windows], "%d", &default_values.iconify);

  sprintf(fenetre3.name, "Range control : %s", argv[No_arg_script]);
  create_range_controls(&fenetre3);

  gtk_widget_show_all (fenetre3.window);

#endif

  /* init continue_simulation_status - needed for menu() */
  continue_simulation_status = WAIT;

  default_values.start_mode = -1;
  if (argv[No_arg_StartMode][0] != '\0') sscanf(argv[No_arg_StartMode], "%d", &default_values.start_mode);

#ifndef AVEUGLE
  g_timeout_add(500, on_start, NULL); /* 100 ms apres le demarage */
#else /* BLIND */
  if (default_values.start_mode >= 0)
  {
    if (promethe_simulation_en_cours > 0)
    {
      kprints("Une simulation est deja en cours ! (arretez la d'abord)\n");
      return;
    }
    if (default_values.start_mode == 0)
    {
      global_learn = 1;
      continue_simulation_status = START;
    }
    else if (default_values.start_mode == 1)
    {
      global_learn = 0;
      continue_simulation_status = START;
    }
    else if (default_values.start_mode == 2)
    {
      global_learn = 1;
      continue_simulation_status = CONTINUE;
    }
    else if (default_values.start_mode == 3)
    {
      global_learn = 0;
      continue_simulation_status = CONTINUE;
    }
    else
    {
      global_learn = 0;
      continue_simulation_status = START;
    } /*use only par defaut*/
  }
#endif


  kprints("Time to start : %lf seconds.\n", g_timer_elapsed(simulation_timer, NULL ));

#ifndef AVEUGLE
  gdk_threads_enter(); /*  version threadee de l'IHM */
  gtk_main(); /* Boucle principale de Gtk */
  gdk_threads_leave();
#else
  menu();
#endif
}

void promethe_quit()
{

#ifndef DIAGNOSTIC
  int i = 0;
#endif

  kprints("Terminating promethe ...\n");

#ifndef DIAGNOSTIC
  /* PG: ne faudrait il pas d'abord tester qu'une simul est bien en route? */

  enet_close_connections();

  continue_simulation_status = QUIT; /* about to quit */

  cancel_pressed(NULL, NULL );
  for (i = 0; i <= max_ech_temps; i++)
    set_tokens(i);

  i = 0;

  if (promethe_simulation_en_cours == 1)  sleep(1); /* passe a 0 a la fin de la simul */

  if (promethe_simulation_en_cours == 1)
  {
    kprints("It cannot stop -> hard terminate \n");
    terminate_each_group_as_a_thread_hard(); /* cancel est thread et appel les destroy des fonctions
     meme si le groupe n'est pas arrete. */
  }

  prom_bus_change_state(No_Quit);

  close_comm();

  quit_enet_token_oscillo_kernel();

  libhardware_close();
  enet_network_finish();
#endif
  kprints("\nPromethe has quitted\n");
  global_close_io_server();
  /*    fin_transmissions(); */

#ifndef AVEUGLE
  gtk_main_quit();
#endif
  exit(0);
}

/*
 Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
 promethe@ensea.fr

 Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier,
 C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,
 M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

 See more details and updates in the file AUTHORS

 This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
 This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software.
 You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license,
 users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability.
 In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software,
 that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge.
 Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured
 and, more generally, to use and operate it in the same conditions as regards security.
 The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
 */
/*---------------------------------------------------------------*/
/*    gestion_ES.h                                               */
/*    s'occupe de toutes les taches ne concernant pas la simul   */
/*    Entrees et Sorties, Affichage , Debug...                   */
/*    Copyrights GAUSSIER Philippe 18 oct 1991                   */
/*---------------------------------------------------------------*/

/* transforme sorties d'un groupe carre en une image  */
/*#define DEBUG 1*/
#include "public.h"

#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include "outils_script.h"

/* ce n'est plus utilise, garde pour vieille compatibilite ? */
type_vecteur_entier nbre_neurones_couche; /* nbre de neurones pour couche       */
type_vecteur_entier no_type_groupe; /* type du groupe pour apprentissage  */

/*-----------------------------------------------------------------*/
/*        LECTURE DU FICHIER DES VECTEURS D'ENTREE                 */
/*-----------------------------------------------------------------*/

void lecture_entree(char *fentree)
{
  FILE *f;
  int i, j;

  f = fopen(fentree, "r");
  printf("\nLecture du fichier de donnees d'entree\n");
  if (f == NULL)
  {
    printf("\n Erreur lors de la lecture du fichier d'entree \n");
    exit(0);
  }
  if (fscanf(f, "%d\n", &nbre_donnee) != 1) PRINT_WARNING("Not a integer."); /* nbre de vecteurs d'entree   */
  if (fscanf(f, "%d\n", &i) != 1) PRINT_WARNING("Not a integer."); /* dim vecteur d'entree        */

  printf("nombre de regions : %d \n", nbre_donnee);
  printf("dimention d'une entree : %d\n", i);

  if (i != nbre_entree)
  {
    printf("\n Erreur : la dimention des vecteurs d'entree ne correspond pas a celle prevue par le reseau utilise \n");
    exit(0);
  }

  donnee = creer_matrice(nbre_donnee, nbre_entree);
  resultat = creer_matrice(nbre_donnee, nbre_sortie);

  while (feof(f) == 0)
  {
    if (fscanf(f, "%d", &i) != 1) PRINT_WARNING("Not a integer.");
    for (j = 0; j < nbre_entree; j++)
      if (fscanf(f, "%f\n", &donnee[i][j]) != 1) PRINT_WARNING("Not a float.");
  }
  fclose(f);
  printf("Lecture du fichier d'entree terminee\n\n");
}

/*-----------------------------------------------------------------*/
/*        LECTURE DU FICHIER DES VECTEURS DU CORRIGE               */
/*-----------------------------------------------------------------*/

void lecture_corrige(char *fcorrige)
{
  FILE *f;
  int i, j;
  printf("\nLecture du corrige pour chaque region\n");
  f = fopen(fcorrige, "r");
  if (f == NULL)
  {
    printf("\n Erreur lors de la lecture du fichier corrige \n");
    exit(0);
  }
  if (fscanf(f, "%d\n", &i) != 1) PRINT_WARNING("Not a integer");
  if (fscanf(f, "%d\n", &j) != 1) PRINT_WARNING("Nor a integer");

  printf("nombre de regions : %d \n", i);
  printf("dimention du vecteur (sortie) : %d\n", j);

  if ((i != nbre_donnee) || (j != nbre_sortie))
  {
    printf("\n Erreur le fichier corrige ne correspond pas avec les autres caracteristiques \n");
    exit(0);
  }

  corrige = creer_matrice(nbre_donnee, nbre_sortie);
  while (feof(f) == 0)
  {
    if (fscanf(f, "%d\n", &i) != 1) PRINT_WARNING("Wrong format");
    for (j = 0; j < nbre_sortie; j++)
      if (fscanf(f, "%f\n", &corrige[i][j]) != 1) PRINT_WARNING("Wrong format");
  }
  fclose(f);
  printf("Lecture du fichier corrige terminee\n\n");
}

/*-----------------------------------------------------------------*/
/*        ECRITURE DU FICHIER DES VECTEURS DE SORTIE               */
/*-----------------------------------------------------------------*/

void ecriture_sortie(char *fsortie)
{
  int i, j;
  FILE *f;

  printf("Ecriture du fichier de vecteurs de sortie\n");
  f = fopen(fsortie, "w");
  if (f == NULL)
  {
    printf("\n Erreur lors de l'ecriture du fichier de sortie \n");
    exit(0);
  }
  fprintf(f, "%d\n", nbre_donnee);
  fprintf(f, "%d\n", nbre_sortie);
  for (i = 0; i < nbre_donnee; i++)
  {
    fprintf(f, "%d\n", i);
    for (j = 0; j < nbre_sortie; j++)
      fprintf(f, "%f\n", resultat[i][j]);
  }
  fclose(f);
}

/*---------------------------------------------------------------*/
/*             LECTURE DU RESEAU DEPUIS UN FICHIER               */
/*---------------------------------------------------------------*/

void lecture_reseau_ascii(char *local_freseau)
/* local_freseau: nom du fichier reseau de neurone    */
/* local_fscript: nom du fichier script               */
{
  char ligne[100]; /*lecture ligne commentaire */
  int nbre_coeff, i, j, v;
  type_coeff *pt, *pt1;
  int taille, taillex;
  int x, y;
  FILE *f;

  printf("\nLecture du reseau de neurone \n");
  f = fopen(local_freseau, "r");
  if (f == NULL)
  {
    EXIT_ON_ERROR("Erreur lors de l'ouverture du fichier contenant le reseau");
  }

  if (fgets(ligne, 80, f) == NULL)
  {
    EXIT_ON_ERROR("fgets => End of file reached while reading res file (first line)");
  }

  if (fgets(ligne, 80, f) == NULL)
  {
    EXIT_ON_ERROR("fgets => End of file reached while reading res file (second line)");
  }

  if (fscanf(f, "Version %i\n\n", &v) != 1)
  {
    EXIT_ON_ERROR("fscanf => Incorrect number of items read in res file (Version)");
  }
  printf("Reseau de neurone : Version = %i \n", v);

  if (v != RESEAU_VERSION)
  {
    printf("ERREUR : La version du reseau de neurone n'est pas celle du programme\n");
    printf("         Programme version %i \n", RESEAU_VERSION);
    printf(" version actuelle %i\n", v);
    exit(1);
  }

  if (fscanf(f, "script associe : %s", ligne) != 1)
  {
    EXIT_ON_ERROR("fscanf => Incorrect number of items read in res file (Script associe)");
  }
  printf("script associe : %s\n", ligne);

  if (fscanf(f, "%i\n", &nbre_couche) != 1)
  {
    EXIT_ON_ERROR("fscanf => Incorrect number of items read in res file (nbre_couche)");
  }
  printf("nbre_couche = %i\n", nbre_couche);

  if (fscanf(f, "%i\n", &nbre_groupe) != 1)
  {
    EXIT_ON_ERROR("fscanf => Incorrect number of items read in res file (nbre_groupe)");
  }
  printf("nbre_groupe = %i\n", nbre_groupe);

  if (fscanf(f, "%i\n", &nbre_neurone) != 1)
  {
    EXIT_ON_ERROR("fscanf => Incorrect number of items read in res file (nbre_neurone)");
  }
  printf("nbre_neurone = %i\n", nbre_neurone);

  if (fscanf(f, "%i\n", &nbre_entree) != 1)
  {
    EXIT_ON_ERROR("fscanf => Incorrect number of items read in res file (nbre_entree)");
  }
  printf("nbre_entree = %i\n", nbre_entree);

  if (fscanf(f, "%i\n", &nbre_sortie) != 1)
  {
    EXIT_ON_ERROR("fscanf => Incorrect number of items read in res file (nbre_sortie)");
  }
  printf("nbre_sortie = %i\n", nbre_sortie);

  nbre_neurones_couche = creer_vecteur(nbre_couche);
  no_type_groupe = creer_vecteur(nbre_groupe);

  /* lecture du nombre de neurones par couche */
  for (i = 0; i < nbre_couche; i++)
    READ_LINE_INT(f, &nbre_neurones_couche[i]);

  for (i = 0; i < nbre_couche; i++)
  {
    if (fscanf(f, "%i\n", &nbre_neurones_couche[i]) != 1)
    {
      EXIT_ON_ERROR("fscanf => Incorrect number of items read in res file (couche %i)", i);
    }
    printf("couche %i, val = %i\n", i, nbre_neurones_couche[i]);
  }

  /* lecture du type du groupe pour apprentissage */
  /* lecture du nom du groupe pour utilisation    */

  for (i = 0; i < nbre_groupe; i++)
  {
    READ_LINE_INT(f, &no_type_groupe[i]);
    if (fscanf(f, "nom du groupe : %s\n", nom_groupe[i]) != 1) PRINT_WARNING("Wrong format.");
    printf("groupe %d , type : %d , nom : %s \n", i, no_type_groupe[i], nom_groupe[i]);
  }

  neurone = creer_reseau(nbre_neurone);
  for (i = 0; i < nbre_neurone; i++)
  {
    READ_LINE_INT(f, &neurone[i].groupe);

    /*   printf("neurone %d , groupe %d \n",i,neurone[i].groupe); */
    if (no_type_groupe[neurone[i].groupe] == No_Fonction_Algo)
    {
      if (fscanf(f, "taille = %d \n", &taille) != 1) PRINT_WARNING("Wrong format");
      taillex = def_groupe[neurone[i].groupe].taillex;
      printf("groupe %d , taille = %d , taillex= %d\n", neurone[i].groupe, taille, taillex);
      x = 0;
      y = 0;
      for (j = i; j < i + taille; j++)
      {
        neurone[j].groupe = neurone[i].groupe;
        neurone[j].posx = (float) x;
        neurone[j].posy = (float) y;
        neurone[j].posz = (float) neurone[i].groupe;
        neurone[j].coeff = NULL;
        x++;
        if (x > taillex - 1)
        {
          x = 0;
          y++;
        }
      }
      i = taille + i - 1;
    }
    else
    {
      READ_LINE_INT(f, &nbre_coeff);
      neurone[i].nbre_coeff = nbre_coeff;
      READ_LINE_FLOAT(f, &neurone[i].seuil);

      if (no_type_groupe[neurone[i].groupe] == No_Winner_Colonne || no_type_groupe[neurone[i].groupe] == No_PTM)
      READ_LINE_FLOAT(f, &neurone[i].d);

      if (fscanf(f, "%f %f %f\n", &neurone[i].posx, &neurone[i].posy, &neurone[i].posz) != 3) PRINT_WARNING("Wrong format");

      neurone[i].nbre_voie = 0;
      if (nbre_coeff > 0)
      { /*SINON C'EST UNE ENTREE */
        pt = creer_coeff();
        neurone[i].coeff = pt;
        if (fscanf(f, "%f %d %d %d", &pt->val, &pt->entree, &pt->type, &pt->evolution) != 4) PRINT_WARNING("Wrong format");
        if ((pt->type + 2) / 2 > neurone[i].nbre_voie) neurone[i].nbre_voie = (pt->type + 2) / 2;
        pt1 = pt;
        for (j = 1; j < nbre_coeff; j++)
        {
          pt = creer_coeff();
          pt1->s = pt;
        }
      }
      else neurone[i].coeff = NULL;
    }
  }
  fclose(f);
  printf("Lecture du reseau terminee \n\n");
}

int load_one_weight(FILE *f, type_coeff *pt, char *statut)
{
  int nb_lu;
  int taille_int, taille_float, taille_char;

  taille_int = sizeof(int); /* a rendre plus propre: sauvegarde format d'abord, test ensuite ... */
  taille_float = sizeof(float);
  taille_char = sizeof(char);

  if (fread(&(pt->val), taille_float, 1, f) == 0) PRINT_WARNING("Nothing read");
  dprints("%x->val = %f, ", pt, pt->val);
  if (fread(&(pt->entree), taille_int, 1, f) == 0) PRINT_WARNING("Nothing read");
  dprints("entree = %i, ", pt->entree);
  if (fread(&(pt->type), taille_int, 1, f) == 0) PRINT_WARNING("Nothing read");
  dprints("type = %i, ", pt->type);
  if (fread(&(pt->evolution), taille_int, 1, f) == 0) PRINT_WARNING("Nothing read");
  dprints("evolution = %i, ", pt->evolution);
  if (fread(&(pt->proba), taille_float, 1, f) == 0) PRINT_WARNING("Nothing read");
  dprints("proba = %f, ", pt->proba);
  if (fread(&(pt->gpe_liaison), taille_int, 1, f) == 0) PRINT_WARNING("Nothing read");
  dprints("gpe_liaison = %i, ", pt->gpe_liaison);
  if (fread(&(pt->Nbre_E), taille_float, 1, f) == 0) PRINT_WARNING("Nothing read");
  dprints("Nbre_E = %f, ", pt->Nbre_E);
  if (fread(&(pt->Nbre_S), taille_float, 1, f) == 0) PRINT_WARNING("Nothing read");
  dprints("Nbre_S = %f, ", pt->Nbre_S);
  if (fread(&(pt->Nbre_ES), taille_float, 1, f) == 0) PRINT_WARNING("Nothing read");
  dprints("Nbre_ES = %f\n", pt->Nbre_ES);
  nb_lu = fread(statut, taille_char, 1, f); /* statut est modifie et renvoye --> acces par pointeur */
  /* si *statut=0 c'est la fin */
  return nb_lu;
}

int compte_one_weight(FILE *f, char *statut)
{
  int nb_lu;
  int taille_int, taille_float, taille_char;
  type_coeff pt;

  taille_int = sizeof(int); /* a rendre plus propre: sauvegarde format d'abord, test ensuite ... */
  taille_float = sizeof(float);
  taille_char = sizeof(char);

  if (fread(&(pt.val), taille_float, 1, f) == 0) PRINT_WARNING("Nothing read");
  dprints("%x->val = %f, ", pt, pt.val);
  if (fread(&(pt.entree), taille_int, 1, f) == 0) PRINT_WARNING("Nothing read");
  dprints("entree = %i, ", pt.entree);
  if (fread(&(pt.type), taille_int, 1, f) == 0) PRINT_WARNING("Nothing read");
  dprints("type = %i, ", pt.type);
  if (fread(&(pt.evolution), taille_int, 1, f) == 0) PRINT_WARNING("Nothing read");
  dprints("evolution = %i, ", pt.evolution);
  if (fread(&(pt.proba), taille_float, 1, f) == 0) PRINT_WARNING("Nothing read");
  dprints("proba = %f, ", pt.proba);
  if (fread(&(pt.gpe_liaison), taille_int, 1, f) == 0) PRINT_WARNING("Nothing read");
  dprints("gpe_liaison = %i, ", pt.gpe_liaison);
  if (fread(&(pt.Nbre_E), taille_float, 1, f) == 0) PRINT_WARNING("Nothing read");
  dprints("Nbre_E = %f, ", pt.Nbre_E);
  if (fread(&(pt.Nbre_S), taille_float, 1, f) == 0) PRINT_WARNING("Nothing read");
  dprints("Nbre_S = %f, ", pt.Nbre_S);
  if (fread(&(pt.Nbre_ES), taille_float, 1, f) == 0) PRINT_WARNING("Nothing read");
  dprints("Nbre_ES = %f\n", pt.Nbre_ES);
  nb_lu = fread(statut, taille_char, 1, f); /* statut est modifie et renvoye --> acces par pointeur */
  /* si *statut=0 c'est la fin */
  return nb_lu;
}

/* surcharge le contenu des poids d'une voie de liaison par ceux lus depuis un fichier*/
/* on gere le decalage des neurones en entree: translation et dilatation suite a l'ajout de nouveaux micro neurones sur le groupe d'entree */

/**
 *  Attention increment et new_inc ne sont pas utilise alors qu'ils sont surement utiles pour les microneurones.
 */
int load_weights_in_neuron(FILE *f, int input_grp, type_coeff *coeff, int old_deb, int increment, int new_deb, int new_inc)
{
  int nb_links_read = 0, nb_read = 1, new_no;
  char status = 1;
  char status_fin = 0;
  int gpe_liaison;
  (void) increment;
  (void) new_inc;

  while (status != status_fin && nb_read == 1)
  {
    gpe_liaison = coeff->gpe_liaison;
    dprints("input_grp = %i, liaison.depart = %i\n", input_grp, liaison[gpe_liaison].depart);
    if (input_grp < 0 || input_grp == liaison[gpe_liaison].depart)
    {
      nb_read = load_one_weight(f, coeff, &status);
      coeff->gpe_liaison = gpe_liaison;

      new_no = new_deb + coeff->entree - old_deb;
      dprints("new_no = %i (%i + %i - %i)\n", new_no, new_deb, coeff->entree, old_deb);
      coeff->entree = new_no; /* adapatation du poids vers le nouveau neurone en entree */
      nb_links_read++;
      dprints("Nb of links read: %i\n", nb_links_read);
    }

    coeff = coeff->s;

    if (coeff == NULL && nb_links_read == 0)
    {
      nb_read = fread(&status, sizeof(char), 1, f);
    }

    if (coeff == NULL && status != status_fin)
    {
      printf("\nWarning not enough links on the neuron to read the file (more weights!) in function %s\n", __FUNCTION__);
      return 0;
    }
    dprints("status = %i\n", (int) status);
  }
  return 1;
}

/* lecture du reseau de neurone sauvegarde sous forme binaire */
/* ATTENTION: le fichier .res depend du hardware utilise      */
/* Il faut regenerer le reseau pour chaque type de processeur */

void lecture_reseau(char *local_freseau)
{
  char ligne[100]; /*lecture ligne commentaire */
  int i, v;
  int j;
  long int position;
  type_coeff *pt, *pt1;

  int nb_lu, u = 0;
  char statut_fin = 0;
  char statut;
  int nombre;

  FILE *f;

  dprints("\nLecture du reseau de neurone \n");
  f = fopen(local_freseau, "r");
  if (f == NULL)
  {
    EXIT_ON_ERROR("Erreur lors de l'ouverture du fichier contenant le reseau");
  }

  if (fgets(ligne, 80, f) == NULL)
  {
    EXIT_ON_ERROR("fgets => End of file reached while reading res file (first line)");
  }

  if (fgets(ligne, 80, f) == NULL)
  {
    EXIT_ON_ERROR("fgets => End of file reached while reading res file (second line)");
  }

  if (fscanf(f, "Version %i\n\n", &v) != 1)
  {
    EXIT_ON_ERROR("fscanf => Incorrect number of items read in res file (Version)");
  }
  dprints("Reseau de neurone : Version = %i \n", v);

  if (v != RESEAU_VERSION) EXIT_ON_ERROR("The fersion of the .res %i does not fit with promethe %i\n", v, RESEAU_VERSION);

  if (fscanf(f, "script associe : %s", ligne) != 1)
  {
    EXIT_ON_ERROR("fscanf => Incorrect number of items read in res file (Script associe)");
  }
  dprints("script associe : %s\n", ligne);

  if (fscanf(f, "%i\n", &nbre_couche) != 1)
  {
    EXIT_ON_ERROR("fscanf => Incorrect number of items read in res file (nbre_couche)");
  }
  dprints("nbre_couche = %i\n", nbre_couche);

  if (fscanf(f, "%i\n", &nbre_groupe) != 1)
  {
    EXIT_ON_ERROR("fscanf => Incorrect number of items read in res file (nbre_groupe)");
  }
  dprints("nbre_groupe = %i\n", nbre_groupe);

  if (fscanf(f, "%i\n", &nbre_neurone) != 1)
  {
    EXIT_ON_ERROR("fscanf => Incorrect number of items read in res file (nbre_neurone)");
  }
  dprints("nbre_neurone = %i\n", nbre_neurone);

  nbre_neurones_couche = creer_vecteur(nbre_couche);
  no_type_groupe = creer_vecteur(nbre_groupe);
  neurone = creer_reseau(nbre_neurone);
  info_neurone_pour_pando=creer_reseau_para_supp_pando(nbre_neurone); // pour les infos supplementaire liés à pando.

  for (i = 0; i < nbre_couche; i++)
  {
    if (fgets(ligne, 99, f) == NULL)
    {
      EXIT_ON_ERROR("End of file reached while reading res file (couche %i)", i);
    }
    sscanf(ligne, "%i\n", &nbre_neurones_couche[i]);
    dprints("couche %i, val = %i\n", i, nbre_neurones_couche[i]);
  }

  if ((int) fread(neurone, sizeof(type_neurone), nbre_neurone, f) != nbre_neurone)
  {
    EXIT_ON_ERROR("fread => Incorrect number of items read in res file (neurone data)");
  }

  /*for (i = 0; i < nbre_neurone; i++)
   {
   printf("neuron %i : seuil = %f, s = %f, s1 = %f, s2 = %f, flag = %i, d = %f, last_activation = %f, cste = %f, groupe = %i, nbre_voie = %i, nbre_coeff = %i, max = %c, posx = %f, posy = %f, posz = %f, coeff = %p, nsor = %p\n\n", i, neurone[i].seuil, neurone[i].s, neurone[i].s1, neurone[i].s2, neurone[i].flag, neurone[i].d, neurone[i].last_activation, neurone[i].cste, neurone[i].groupe, neurone[i].nbre_voie, neurone[i].nbre_coeff, neurone[i].max, neurone[i].posx, neurone[i].posy, neurone[i].posz, (void *)neurone[i].coeff, (void *)neurone[i].nsor);
   }*/

  dprints("fin de lecture des neurones \n");

  for (i = 0; i < nbre_neurone; i++)
  {
    /* la lecture des coeff suppose que la lecture des structures des neurones */
    /* donne une info sur le fait que le neurone avait un lien ou pas          */
    info_neurone_pour_pando[i].have_to_send_link=0;

    pt = neurone[i].coeff;
    if (pt != NULL)
    {
      nombre = 0;
      position = ftell(f);
      nb_lu = compte_one_weight(f, &statut);
      nombre = 1;
      while (statut != statut_fin && nb_lu == 1)
      {
        nb_lu = compte_one_weight(f, &statut);
        nombre++;
      }

      pt = NULL; //securite
      pt = MANY_ALLOCATIONS(nombre, type_coeff);
      neurone[i].coeff = pt;
      neurone[i].nbre_coeff=nombre;

      for (j = 0; j < nombre; j++)
      {
        pt[j].s = NULL;
      }

      //     neurone[i].coeff = &(pt[0]);
      fseek(f, position, SEEK_SET);

      nb_lu = 0;
      nb_lu = load_one_weight(f, pt, &statut); //init du coeff initial

      u = 0;
      pt1 = pt;
      while (statut != statut_fin && nb_lu == 1)
      { /* lit tous les autres coefficients     */
        pt1 = &(pt[u + 1]);
        (pt[u]).s = pt1;

        nb_lu = load_one_weight(f, pt1, &statut);
        u++;
        //pt = pt1;
      }
    }
  }

  dprints("fin de lecture des poids\n");
  fclose(f);
  printf("%i neurons read. \n\n", nbre_neurone);
}

/*--------------------------------------------------------------*/
/*            SAUVEGARDE DU RESEAU DANS UN FICHIER              */
/*--------------------------------------------------------------*/

void ecriture_reseau_ascii(char *local_freseau, char *local_fscript)
/* local_freseau: nom du fichier reseau en sortie   */
/* local_fscript: nom du fichier script associe      */
{
  int i, j;
  type_coeff *pt;
  FILE *f;

  f = fopen(local_freseau, "w");
  if (f == NULL)
  {
    printf("\n Erreur lors de l'ecriture du fichier contenant le reseau \n");
    exit(0);
  }
  fprintf(f, "Reseau de neurone\n");
  fprintf(f, "Copyright Philippe GAUSSIER oct 1991\n");
  fprintf(f, "Version %d\n\n", RESEAU_VERSION);
  fprintf(f, "script associe : %s\n", local_fscript);
  fprintf(f, "%d\n", nbre_couche);
  fprintf(f, "%d\n", nbre_groupe);
  fprintf(f, "%d\n", nbre_neurone);
  fprintf(f, "%d\n", nbre_entree);
  fprintf(f, "%d\n", nbre_sortie);

  /*ecrit le nbre de neurones par couches */
  for (i = 0; i < nbre_couche; i++)
    fprintf(f, "%d\n", nbre_neurones_couche[i]);

  /*ecrit type de chaque groupe           */
  for (i = 0; i < nbre_groupe; i++)
  {
    fprintf(f, "%d\n", no_type_groupe[i]);
    fprintf(f, "nom du groupe : %s\n", nom_groupe[i]);
  }

  for (i = 0; i < nbre_neurone; i++)
  {
    j = 0; /* nbre d'entrees                  */
    pt = neurone[i].coeff;
    while (pt != NULL)
    { /* compte le nombre d'entrees      */
      j++;
      pt = pt->s;
    }
    fprintf(f, "%d\n", neurone[i].groupe);
    if (no_type_groupe[neurone[i].groupe] == No_Fonction_Algo)
    {
      for (j = i + 1; j < nbre_neurone; j++)
        if (neurone[i].groupe != neurone[j].groupe)
        {
          fprintf(f, "taille = %d \n", j - i);
          i = j - 1;
          break;
        }
      if (j == nbre_neurone)
      {
        fprintf(f, "taille = %d \n", j - i);
        i = j - 1;
      }
    }
    else
    {
      fprintf(f, "%d\n", j);
      fprintf(f, "%f\n", neurone[i].seuil);

      if (no_type_groupe[neurone[i].groupe] == No_Winner_Colonne || no_type_groupe[neurone[i].groupe] == No_PTM) fprintf(f, "%f\n", neurone[i].d);

      fprintf(f, "%f %f %f\n", neurone[i].posx, neurone[i].posy, neurone[i].posz);
      pt = (neurone[i]).coeff;
      while (pt != NULL)
      {
        pt = pt->s; /* inscrit coeff suivant           */
      }
    }
  }
  fclose(f);
}

void sauve_un_poids(int fd, type_coeff *pt)
{
  int taille_int, taille_float;

  if (pt == NULL) /* on ne devrait pas etre ici si pt==NULL */
  {
    printf("ERROR %s, le pt vers coeff est NULL \n", __FUNCTION__);
    exit(1);
  }
  taille_int = sizeof(int);
  taille_float = sizeof(float);

  if (write(fd, &(pt->val), taille_float) < 0) PRINT_SYSTEM_ERROR();
  dprints("%x->val = %f, ", pt, pt->val);
  if (write(fd, &(pt->entree), taille_int) < 0) PRINT_SYSTEM_ERROR();
  dprints("entree = %i, ", pt->entree);
  if (write(fd, &(pt->type), taille_int) < 0) PRINT_SYSTEM_ERROR();
  dprints("type = %i, ", pt->type);
  if (write(fd, &(pt->evolution), taille_int) < 0) PRINT_SYSTEM_ERROR();
  dprints("evolution = %i, ", pt->evolution);
  if (write(fd, &(pt->proba), taille_float) < 0) PRINT_SYSTEM_ERROR();
  dprints("proba = %f, ", pt->proba);
  if (write(fd, &(pt->gpe_liaison), taille_int) < 0) PRINT_SYSTEM_ERROR();
  dprints("gpe_liaison = %i, ", pt->gpe_liaison);
  if (write(fd, &(pt->Nbre_E), taille_float) < 0) PRINT_SYSTEM_ERROR();
  dprints("Nbre_E = %f, ", pt->Nbre_E);
  if (write(fd, &(pt->Nbre_S), taille_float) < 0) PRINT_SYSTEM_ERROR();
  dprints("Nbre_S = %f, ", pt->Nbre_S);
  if (write(fd, &(pt->Nbre_ES), taille_float) < 0) PRINT_SYSTEM_ERROR();
  dprints("Nbre_ES = %f\n", pt->Nbre_ES);
}

/*
 void sauve_corps_neurones(int fd, int no_groupe)
 {

 }
 */

void sauve_poids_un_neurone(int fd, int input_grp, type_coeff *coeff)
{
  int taille_char, nb_ecrits = 0;
  char statut_continuer = 1;
  char statut_fin = 0;
  type_coeff *pt;

  taille_char = sizeof(char);
  pt = coeff;
  while (pt != NULL)
  { /* ecrit tous les coefficients     */
    dprints("input_grp = %i, liaison.depart = %i\n", input_grp, liaison[pt->gpe_liaison].depart);
    if (input_grp < 0 || input_grp == liaison[pt->gpe_liaison].depart)
    {
      if (nb_ecrits > 0)
      {
        dprints("status continuer\n");
        if (write(fd, &statut_continuer, taille_char) < 0) PRINT_SYSTEM_ERROR();
      }
      sauve_un_poids(fd, pt);
      nb_ecrits++;
      dprints("%i links written\n", nb_ecrits);

    }
    pt = pt->s; /* inscrit coeff suivant           */

    if (pt == NULL)
    {
      dprints("status fin\n");
      if (write(fd, &statut_fin, taille_char) <= 0) PRINT_WARNING("Nothing written");
    }
  }
}

#define BUFFER_SIZE 512
void sauve_voie_liaison(int fd, int gpe_entree, int gpe_sortie)
{
  int deb, nbre, nbre2, increment;
  int i, n;
  type_coeff *coeff;
  char buffer[BUFFER_SIZE];
  type_groupe *mon_groupe;

  snprintf(buffer, BUFFER_SIZE - 1, "<link>\n input=%s\n output=%s\n (%i,%i)\n", def_groupe[gpe_entree].no_name, def_groupe[gpe_sortie].no_name, gpe_entree, gpe_sortie);
  if (write(fd, buffer, strlen(buffer)) < 0) PRINT_SYSTEM_ERROR();

  mon_groupe = &def_groupe[gpe_entree];
  snprintf(buffer, BUFFER_SIZE - 1, "<Input_Group>\n");
  if (write(fd, buffer, strlen(buffer)) < 0) PRINT_SYSTEM_ERROR();
  snprintf(buffer, BUFFER_SIZE - 1, "sizex = %d, sizey= %d \n", mon_groupe->taillex, mon_groupe->tailley);
  if (write(fd, buffer, strlen(buffer)) < 0) PRINT_SYSTEM_ERROR();
  deb = mon_groupe->premier_ele;
  nbre = mon_groupe->nbre;
  nbre2 = mon_groupe->taillex * mon_groupe->tailley;
  increment = nbre / nbre2;
  snprintf(buffer, BUFFER_SIZE - 1, "beg=%d, nbr=%d, incr=%d\n", deb, nbre, increment);
  if (write(fd, buffer, strlen(buffer)) < 0) PRINT_SYSTEM_ERROR();
  snprintf(buffer, BUFFER_SIZE - 1, "<\\Input_Group>\n");
  if (write(fd, buffer, strlen(buffer)) < 0) PRINT_SYSTEM_ERROR();

  mon_groupe = &def_groupe[gpe_sortie];
  snprintf(buffer, BUFFER_SIZE - 1, "<Output_Group>\n");
  if (write(fd, buffer, strlen(buffer)) < 0) PRINT_SYSTEM_ERROR();
  snprintf(buffer, BUFFER_SIZE - 1, "sizex = %d, sizey= %d \n", mon_groupe->taillex, mon_groupe->tailley);
  if (write(fd, buffer, strlen(buffer)) < 0) PRINT_SYSTEM_ERROR();
  deb = mon_groupe->premier_ele;
  nbre = mon_groupe->nbre;
  nbre2 = mon_groupe->taillex * mon_groupe->tailley;
  increment = nbre / nbre2;
  snprintf(buffer, BUFFER_SIZE - 1, "beg=%d, nbr=%d, incr=%d\n", deb, nbre, increment);
  if (write(fd, buffer, strlen(buffer)) < 0) PRINT_SYSTEM_ERROR();
  snprintf(buffer, BUFFER_SIZE - 1, "<\\Output_Group>\n");
  if (write(fd, buffer, strlen(buffer)) < 0) PRINT_SYSTEM_ERROR();

  /*   printf("deb= %d, increment = %d,  nbre=%d\n",deb,increment,nbre); */
  /* si macro neurone alors chaque micro neurone ne corrrespond qu'a une seule voie*/
  /* dans le cas contraire il faudrait mieux tout sauvegarder ou trier un par un? */
  printf("sauvegarde des poids pour les neurones %d a %d\n", deb, deb + nbre);
  for (n = deb; n < deb + nbre; n = n + increment) /*macro */
  {
    printf("progression: %3d%%\n", (int) (100 * (n - deb) / nbre));
    fflush(stdout);
    for (i = n; i < n + increment; i++) /*micro*/
    {
      printf("neurone %d en traitement\n", i);
      fflush(stdout);
      coeff = neurone[i].coeff;
      /* ecriture de seuil, flag, d, cste dans cet ordre*/
      if (write(fd, &neurone[i].seuil, sizeof(float)) < 0) PRINT_SYSTEM_ERROR();
      if (write(fd, &neurone[i].flag, sizeof(float)) < 0) PRINT_SYSTEM_ERROR();
      if (write(fd, &neurone[i].d, sizeof(float)) < 0) PRINT_SYSTEM_ERROR();
      if (write(fd, &neurone[i].cste, sizeof(float)) < 0) PRINT_SYSTEM_ERROR();
      dprints("neuron written, seuil = %f, flag = %f, d = %f, cste = %f\n", neurone[i].seuil, neurone[i].flag, neurone[i].d, neurone[i].cste);

      sauve_poids_un_neurone(fd, gpe_entree, coeff);
    }
  }
  printf("progression: 100%% fin\n");
  snprintf(buffer, BUFFER_SIZE - 1, "<\\link>\n");
  if (write(fd, buffer, strlen(buffer)) < 0) PRINT_SYSTEM_ERROR();
}

/* lit une voie de liaison en ecrasant les anciennes valeurs des poids */
/* reste a faire: test sur taille entree et taille sortie + sizex sizey qui peuvent aussi changer */
int lecture_voie_liaison(char *link_name, char *input_group_name, char *output_group_name)
{
  int output_beg, output_nbr, output_nbr2, output_inc; /* current output Group */
  int input_beg, input_inc;
  int file_output_deb, file_output_nbr, file_output_inc;
  int file_output_sizex, file_output_sizey;
  int output_sizex, output_sizey;
  int file_input_beg, file_input_nbr, file_input_inc;
  int file_input_sizex, file_input_sizey;
  char file_input_group_name[TAILLE_CHAINE], file_output_group_name[TAILLE_CHAINE];
  int x, y, i, n;
  int input_grp, output_grp;
  int file_input_grp, file_output_grp;

  type_coeff *coeff = NULL;
  type_groupe *pt_output;
  type_noeud_comment * first_comment = NULL;
  static char ligne[TAILLE_CHAINE];
  char * ret = NULL;
  FILE *f;
  int v, res;

  input_grp = find_no_associated_to_symbolic_name(input_group_name, NULL);
  output_grp = find_no_associated_to_symbolic_name(output_group_name, NULL);
  if (input_grp < 0 || output_grp < 0)
  {
    printf("ERROR %s, cannot load the link, one group does not exist \n", __FUNCTION__);
    return 0;
  }

  input_beg = def_groupe[input_grp].premier_ele;
  input_inc = def_groupe[input_grp].nbre / (def_groupe[input_grp].taillex * def_groupe[input_grp].tailley);

  pt_output = &def_groupe[output_grp]; /* current output group */
  output_beg = pt_output->premier_ele;
  output_sizex = pt_output->taillex;
  output_sizey = pt_output->tailley;
  output_nbr = pt_output->nbre;
  output_nbr2 = pt_output->taillex * pt_output->tailley;
  output_inc = output_nbr / output_nbr2;
  printf("output_nbre=%i,output_nbr2=%i,output_inc=%i --- %i %i\n", output_nbr, output_nbr2, output_inc, pt_output->taillex, pt_output->tailley);
  printf("\nLecture de la voie de liaision %s \n", link_name);
  f = fopen(link_name, "r");
  if (f == NULL)
  {
    printf("\n ERROR Function %s while opening the binary network %s \n", __FUNCTION__, link_name);
    return 0;
  }

  /*les commentaires lus sont pour l'instant perdus */
  first_comment = read_line_with_comment(f, first_comment, ligne);
  if (recherche_champs(ligne, "<Version") != NULL)
  {
    /* printf("ligne = %s \n",ligne);*/
    sscanf(ligne, "<Version %d />\n", &v);
    /*  printf("Reseau de neurone : Version = %d \n", v);*/
    if (v != RESEAU_VERSION)
    {
      printf("ERREUR : La version du reseau de neurone n'est pas celle du programme\n");
      printf("         Programme version %d \n", RESEAU_VERSION);
      printf(" version actuelle %d\n", v);
      return 0;
    }
    first_comment = read_line_with_comment(f, first_comment, ligne); /*<script ... >*/
  }

  /* printf("ligne 2 = %s \n",ligne);*/
  /*  sscanf(ligne,"<link>");*/
  first_comment = read_line_with_comment(f, first_comment, ligne); /* <link> */
  /*printf("ligne 3 = %s \n",ligne);*/

  res = fscanf(f, " input=%s\n", file_input_group_name);
  if (res != 1)
  {
    printf("error while reading for input= (%s)\n", __FUNCTION__);
    printf("1 res = %d \n", res);
    return 0; /* FAIL to load the link */
  }

  res = fscanf(f, " output=%s\n", file_output_group_name);
  if (res != 1)
  {
    printf("error while reading for output= (%s)\n", __FUNCTION__);
    printf("2 res = %d \n", res);
    return 0; /* FAIL to load the link */
  }
  res = fscanf(f, " (%i,%i)\n", &file_input_grp, &file_output_grp);
  if (res != 2)
  {
    printf("error while reading for (x,x) (%s)\n", __FUNCTION__);
    printf("3 res = %d \n", res);
    return 0; /* FAIL to load the link */
  }

  if (strcmp(file_input_group_name, input_group_name) != 0 || strcmp(file_output_group_name, output_group_name) != 0)
  {
    printf("Be careful: file_input_name = %s, file_output_name = %s different from current IO group names\n", file_input_group_name, file_output_group_name);
  }
  file_input_grp = find_no_associated_to_symbolic_name(input_group_name, NULL);
  file_output_grp = find_no_associated_to_symbolic_name(output_group_name, NULL);
  /*printf("no input grp = %d , no output grp = %d \n",input_grp, output_grp);*/

  /*res=fscanf(f,"<Input_Group>\n"); */
  first_comment = read_line_with_comment(f, first_comment, ligne); /* <Input_Group> */
  /*printf("ligne = %s \n",ligne);*/

  res = fscanf(f, "sizex = %d, sizey= %d \n", &file_input_sizex, &file_input_sizey);
  if (res != 2)
  {
    printf("error while reading for sizex= x, sizey = y (%s)\n", __FUNCTION__);
    printf("5 res = %d \n", res);
    return 0; /* FAIL to load the link */
  }
  /*printf("sizex = %d, sizey= %d \n",file_input_sizex, file_input_sizey);*/

  res = fscanf(f, "beg=%d, nbr=%d, incr=%d\n", &file_input_beg, &file_input_nbr, &file_input_inc);
  if (res != 3)
  {
    printf("error while reading for beg=x, nbr=x, incr=x (%s)\n", __FUNCTION__);
    printf("6 res = %d \n", res);
    return 0; /* FAIL to load the link */
  }
  res = fscanf(f, "<\\Input_Group>\n");
  res = fscanf(f, "<Output_Group>\n");

  res = fscanf(f, "sizex = %d, sizey= %d \n", &file_output_sizex, &file_output_sizey);
  /*printf("sizex = %d, sizey= %d \n", file_output_sizex, file_output_sizey); */
  if (res != 2)
  {
    printf("error while reading for sizex = x, sizey = y (%s)\n", __FUNCTION__);
    printf("9 res = %d \n", res);
    return 0; /* FAIL to load the link */
  }

  res = fscanf(f, "beg=%d, nbr=%d, incr=%d\n", &file_output_deb, &file_output_nbr, &file_output_inc);
  if (res != 3)
  {
    printf("error while reading for beg=x, nbr=x, incr=x (%s)\n", __FUNCTION__);
    printf("10 res = %d \n", res);
    return 0; /* FAIL to load the link */
  }

  ret = fgets(ligne, TAILLE_CHAINE, f);
  if (ret == NULL || strcmp(ligne, "<\\Output_Group>\n") != 0)
  {
    printf("error while reading closing Output_Group tag\n");
    return 0; /* FAIL to load the link */
  }

  printf("current output beg= %d, inc = %d,  nbr=%d\n", output_beg, output_inc, output_nbr);
  printf("load weights  file_input_beg=%d, inc =%d, input_beg=%d ,inc=%d\n", file_input_beg, file_input_inc, input_beg, input_inc);
  /* si macro neurone alors chaque micro neurone ne corrrespond qu'a une seule voie*/
  /* dans le cas contraire il faudrait mieux tout sauvegarder ou trier un par un? */
  /*   for (n = output_beg; n < output_beg + output_nbr; n = n + output_inc) */

  if (file_output_sizex > output_sizex || file_output_sizey > output_sizey)
  {
    printf("ERROR in functon %s:  grp %s: file_output_size must be < output size x(%d<%d) y(%d<%d)\n", __FUNCTION__, output_group_name, file_output_sizex, output_sizex, file_output_sizey, output_sizey);
    return 0;
  }

  if (file_input_inc != input_inc)
  {
    PRINT_WARNING("the increment (nb of micro neurons) of the input group has changed, this could lead to some problems in restoring the connectivity of the links");
  }

  /*printf("output_nbre=%i,output_nbr2=%i,output_inc=%i --- %i %i\n",output_nbr,output_nbr2,output_inc,pt_output->taillex,pt_output->tailley);*/
  for (y = 0; y < file_output_sizey; y++) /*macro */
  {
    for (x = 0; x < file_output_sizex; x++)
    {
      /*printf("output_nbre=%i,output_nbr2=%i,output_inc=%i --- %i %i\n",output_nbr,output_nbr2,output_inc,pt_output->taillex,pt_output->tailley);*/
      n = output_beg + (x + y * output_sizex) * output_inc;
      /*printf("n=%i, x=%i, y=%i\n",n,x,y);*/
      kprints("progression: %3d%%\n", (int) (100 * (n - output_beg) / output_nbr));
      for (i = n; i < n + output_inc; i++) /*micro*/
      {
        coeff = neurone[i].coeff;
        /* lecture du seuil, du flag, de d et de cste dans cet ordre*/
        if (fread(&(neurone[i].seuil), sizeof(float), 1, f) == 0) PRINT_WARNING("Nothing read.");
        if (fread(&(neurone[i].flag), sizeof(float), 1, f) == 0) PRINT_WARNING("Nothing read.");
        if (fread(&(neurone[i].d), sizeof(float), 1, f) == 0) PRINT_WARNING("Nothing read.");
        if (fread(&(neurone[i].cste), sizeof(float), 1, f) == 0) PRINT_WARNING("Nothing read.");
        dprints("neuron read, seuil = %f, flag = %f, d = %f, cste = %f\n", neurone[i].seuil, neurone[i].flag, neurone[i].d, neurone[i].cste);
        neurone[i].coeff = coeff;
        kprints("load weights for neuron %d \n", i);

        /* Attention file_input_inc et input_inc ne sont pas utilisees, a voir si c'est utile ou si on peut l'enlever. AB*/
        load_weights_in_neuron(f, input_grp, coeff, file_input_beg, file_input_inc, input_beg, input_inc);
      }
    }
  }
  res = fscanf(f, "<\\link>\n");
  kprints("progression: 100%% fin\n");

  return 1; /* si tout va bien. il manque de tester les retours... */
}

int ecriture_voie_liaison(char *local_fscript, char *link_name, char *input_group_name, char *output_group_name)
{
  int fd;
  char buffer[BUFFER_SIZE];
  int input_grp, output_grp;

  printf("nom fichier = %s \n", link_name);

  fd = open(link_name, O_CREAT | O_WRONLY, 0666);
  if (fd == -1)
  {
    perror("Erreur ouverture fichier lres lors de la sauvegarde"); /*pas sur que fonction safe pour handler signal mais vue que je quitte derriere */
    exit(0);
  }
  /*d'apres la lecture du code: snprintf a l'air reentrante (sans static et sans var globales et sans malloc... masi si ca evolue faudra ecrire snprintf perso reentrant) */
  snprintf(buffer, BUFFER_SIZE - 1, "<Version %i /> \n<script %s />\n", RESEAU_VERSION, local_fscript);
  if (write(fd, buffer, strlen(buffer)) < 0) PRINT_SYSTEM_ERROR();

  input_grp = find_no_associated_to_symbolic_name(input_group_name, NULL);
  output_grp = find_no_associated_to_symbolic_name(output_group_name, NULL);
  if (input_grp < 0 || output_grp < 0)
  {
    printf("ERROR, cannot save the link, one group does not exist \n");
    return 0;
  }

  /*debug davibail*/
  printf("input_gpe=%d (%d) output_gpe=%d (%d)\n", input_grp, def_groupe[input_grp].nbre, output_grp, def_groupe[output_grp].nbre);
  /*fin debug*/
  sauve_voie_liaison(fd, input_grp, output_grp);
  return 1;

}

/*ATTENTION cette fct doit rester safe au sens des signaux*/
/*on espere que personne ne touche a local reseau et a local_script pendant l'interruption*/
void ecriture_reseau(char *local_freseau, char *local_fscript)
/* local_freseau: nom du fichier reseau en sortie   */
/* local_fscript: nom du fichier script associe      */
{
  int i;
  int fd;
  type_coeff *pt;
  char buffer[BUFFER_SIZE];

  fd = open(local_freseau, O_CREAT | O_WRONLY, 0666);
  if (fd == -1)
  {
    perror("Erreur ouverture fichier res lors de la sauvegarde"); /*pas sur que fonction safe pour handler signal mais vue que je quitte derriere */
    exit(0);
  }
  /*d'apres la lecture du code: snprintf a l'air reentrante (sans static et sans var globales et sans malloc... masi si ca evolue faudra ecrire snprintf perso reentrant) */
  snprintf(buffer, BUFFER_SIZE - 1, "Reseau de neurone\n"
      "Copyright Philippe GAUSSIER oct 1991\n"
      "Version %d\n\n"
      "script associe : %s\n"
      "%d\n"
      "%d\n"
      "%d\n", RESEAU_VERSION, local_fscript, nbre_couche, nbre_groupe, nbre_neurone);

  if (write(fd, buffer, strlen(buffer)) < 0) PRINT_SYSTEM_ERROR();

  for (i = 0; i < nbre_couche; i++)
  {
    snprintf(buffer, BUFFER_SIZE - 1, "%d\n", nbre_neurones_couche[i]);
    if (write(fd, buffer, strlen(buffer)) < 0) PRINT_SYSTEM_ERROR();
  }

  for (i = 0; i < nbre_neurone; i++)
    if (write(fd, &neurone[i], sizeof(type_neurone)) < 0) PRINT_SYSTEM_ERROR();

  for (i = 0; i < nbre_neurone; i++)
  {
    pt = neurone[i].coeff;
    sauve_poids_un_neurone(fd, -1, pt);
  }

  close(fd);

  /* Ajout Julien 14/03/08 : Gestion des fonctions de sauvegarde utilisateur au niveau des groupes lors de la sauvegarde du reseau */
  for (i = 0; i < nbre_groupe; i++)
  {
    if (def_groupe[i].save != NULL) def_groupe[i].save(i);
    else
    {
      printf("ecriture_reseau : Le pointeur de la fonction save pour le groupe %s est NULL\n", def_groupe[i].no_name);
      exit(0);
    }
  }
  printf("finished saving network\n");
}

/* local_freseau: nom du fichier reseau en sortie   */
/* local_fscript: nom du fichier script associe     */
/*void ecriture_reseau(char *local_freseau, char *local_fscript)
 {
 int i;
 type_coeff *pt;
 int taille_int, taille_float;
 char statut_continuer = 1;
 char statut_fin = 0;
 int taille_char;
 FILE *f;

 f = fopen(local_freseau, "w");
 if (f == NULL) {
 printf
 ("\n Erreur lors de l'ecriture du fichier contenant le reseau \n");
 exit(0);
 }
 fprintf(f, "Reseau de neurone\n");
 fprintf(f, "Copyright Philippe GAUSSIER oct 1991\n");
 fprintf(f, "Version %d\n\n", RESEAU_VERSION);
 fprintf(f, "script associe : %s\n", local_fscript);
 fprintf(f, "%d\n", nbre_couche);
 fprintf(f, "%d\n", nbre_groupe);
 fprintf(f, "%d\n", nbre_neurone);

 for (i = 0; i < nbre_couche; i++)
 fprintf(f, "%d\n", nbre_neurones_couche[i]);

 for (i = 0; i < nbre_neurone; i++)
 fwrite(&neurone[i], sizeof(type_neurone), 1, f);

 taille_int = sizeof(int);
 taille_float = sizeof(float);
 taille_char = sizeof(char);

 for (i = 0; i < nbre_neurone; i++) {
 pt = neurone[i].coeff;
 while (pt != nil) {
 fwrite(&(pt->val), taille_float, 1, f);
 fwrite(&(pt->entree), taille_int, 1, f);
 fwrite(&(pt->type), taille_int, 1, f);
 fwrite(&(pt->evolution), taille_int, 1, f);
 fwrite(&(pt->proba), taille_float, 1, f);
 fwrite(&(pt->gpe_liaison), taille_int, 1, f);
 fwrite(&(pt->Nbre_E), taille_float, 1, f);
 fwrite(&(pt->Nbre_S), taille_float, 1, f);
 fwrite(&(pt->Nbre_ES), taille_float, 1, f);
 pt = pt->s;
 if (pt == nil)
 fwrite(&statut_fin, taille_char, 1, f);
 else
 fwrite(&statut_continuer, taille_char, 1, f);
 }
 }
 fclose(f);
 }*/

/*ecriture binaire de toute la structure coeff*/
/*void ecriture_reseau2(char *local_freseau, char *local_fscript)
 {
 int i, j, fl, in, items;
 type_coeff *pt;
 FILE *f;

 fl = sizeof(float);
 in = sizeof(int);
 f = fopen(local_freseau, "w");
 if (f == NULL) {
 printf
 ("\n Erreur lors de l'ecriture du fichier contenant le reseau \n");
 exit(0);
 }
 printf("ecriture du reseau\n");
 fprintf(f, "script associe : %s\n",local_fscript);
 fprintf(f, "%d\n", nbre_couche);
 fprintf(f, "%d\n", nbre_groupe);
 fprintf(f, "%d\n", nbre_neurone);
 fprintf(f, "%d\n", nbre_entree);
 fprintf(f, "%d\n", nbre_sortie);

 for (i = 0; i < nbre_couche; i++)
 fprintf(f, "%d\n", nbre_neurones_couche[i]);


 #ifdef HP_cree_system
 for (i = 0; i < nbre_groupe; i++) {
 fprintf(f, "%d\n", (trouver_groupe(i))->type);
 fprintf(f, "nom du groupe : %s\n", (trouver_groupe(i))->nom);
 }
 #else
 {
 fprintf(f, "%d\n", no_type_groupe[i]);
 fprintf(f, "nom du groupe : %s\n", nom_groupe[i]);
 }
 #endif

 for (i = 0; i < nbre_neurone; i++) {
 j = 0;
 pt = neurone[i].coeff;
 while (pt != nil) {
 j++;
 pt = pt->s;
 }
 neurone[i].nbre_coeff = j;
 }
 printf("taille = %d \n", sizeof(neurone[0]));
 items =
 fwrite((type_neurone *) neurone, sizeof(neurone[0]), nbre_neurone,
 f);
 printf("items = %d \n", items);
 if (items != nbre_neurone)
 printf("ERREUR ecriture fichier \n");

 for (i = 0; i < nbre_neurone; i++) {
 pt = (neurone[i]).coeff;
 while (pt != nil) {
 fwrite((type_coeff *) pt, sizeof(type_coeff), 1, f);
 pt = pt->s;
 }
 }
 fclose(f);
 }*/

/*---------------------------------------------------------------*/
/*  Lecture du reseau pour simulateur_reseau2.h                  */
/*         GAUSSIER Philippe 1991                                */
/*---------------------------------------------------------------*/

void lecture_reseau2(char *local_freseau, char *local_fscript)
/* local_freseau: nom du fichier reseau de neurone    */
/* local_fscript: nom du fichier script               */
{
  int nbre, i, j, fl;
  type_coeff *pt, *pt1;
  FILE *f;

  fl = sizeof(fl);
  printf("\nLecture du reseau de neurone\n");
  f = fopen(local_freseau, "r");
  if (f == NULL)
  {
    printf("\n Erreur lors de l'ouverture du fichier contenant le reseau \n");
    exit(0);
  }
  if (fscanf(f, "script associe : %s", local_fscript) != 1) PRINT_WARNING("Wrong format.");
  READ_LINE_INT(f, &nbre_couche);
  READ_LINE_INT(f, &nbre_groupe);
  READ_LINE_INT(f, &nbre_neurone);
  READ_LINE_INT(f, &nbre_entree);
  READ_LINE_INT(f, &nbre_sortie);

  printf("reseau a %d couches\n", nbre_couche);
  printf("reseau comporte %d groupes\n", nbre_groupe);
  printf("dimention entree   : %d\n", nbre_entree);
  printf("dimention sortie   : %d\n", nbre_sortie);
  printf("nombre de neurones : %d\n\n", nbre_neurone);
  /*
   fscanf(f,"%f\n",&ca);    constantes de la sigmoide
   fscanf(f,"%f\n",&ck);
   */

  nbre_neurones_couche = creer_vecteur(nbre_couche);
  no_type_groupe = creer_vecteur(nbre_groupe);

  /* lecture du nombre de neurones par couche */
  for (i = 0; i < nbre_couche; i++)
    READ_LINE_INT(f, &nbre_neurones_couche[i]);

  /* lecture du type du groupe pour apprentissage */
  /* lecture du nom du groupe pour utilisation    */

  for (i = 0; i < nbre_groupe; i++)
  {
    READ_LINE_INT(f, &no_type_groupe[i]);
    if (fscanf(f, "nom du groupe : %s", nom_groupe[i]) != 1) PRINT_WARNING("Wrong format of name.");
    printf("groupe %d , type : %d , nom : %s \n", i, no_type_groupe[i], nom_groupe[i]);
  }

  neurone = creer_reseau(nbre_neurone);

  nbre = fread(neurone, sizeof(type_neurone), nbre_neurone, f);
  if (nbre != nbre_neurone) printf("erreur lecture reseau : %d\n", nbre);

  for (i = 0; i < nbre_neurone; i++)
  {
    printf("neurone %d , groupe %d \n", i, neurone[i].groupe);

    nbre = neurone[i].nbre_coeff;
    printf("neurone %d , nbre_coeff = %d \n", i, nbre);
    if (nbre > 0)
    { /*SINON C'EST UNE ENTREE */

      pt = MANY_ALLOCATIONS(nbre, type_coeff);
      pt1 = pt;
      for (j = 1; j < nbre; j++)
      {
        pt1->s = &(pt1[j]);
        pt1 = &(pt[j]);
      }

      // pt = creer_coeff();
      neurone[i].coeff = pt;
      //if (fread((type_coeff *) pt, sizeof(type_coeff), 1, f) == 0) PRINT_WARNING("Nothing read");
      //pt1 = pt;
      //for (j = 1; j < nbre; j++)
      //{
      //pt = creer_coeff();
      // pt1->s = pt;
      // if (fread((type_coeff *) pt, sizeof(type_coeff), 1, f) == 0) PRINT_WARNING("Nothing read");
      // pt1 = pt;
      // }
    }
    else neurone[i].coeff = NULL;
  }
  fclose(f);
  printf("Lecture du reseau terminee \n\n");
}

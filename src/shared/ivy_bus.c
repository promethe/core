/*
 Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
 promethe@ensea.fr

 Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier,
 C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,
 M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

 See more details and updates in the file AUTHORS

 This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
 This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software.
 You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license,
 users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability.
 In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software,
 that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge.
 Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured
 and, more generally, to use and operate it in the same conditions as regards security.
 The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
 */
/* cc hello.c -o hello -I/usr/local/include/Ivy -L/usr/local/lib64 -lglibivy -lpcre*/
/* mettre la librairie a la fin des arguments de user */

#define DEBUG 1

static int current_state;

#include <stdlib.h>
#include <stdio.h>
#include <getopt.h>
#include <stdarg.h>
#include <ivy.h>
#include "basic_tools.h"

#ifdef AVEUGLE
#include <ivyloop.h>
#else
#include <ivyglibloop.h>
#endif

#include "string.h"
#include "net_message_debug_dist.h"
#include "public.h"
#include "outils_script.h"
#include "prom_bus.h"
#include "oscillo_kernel.h"
#include "pandora_connect.h"
#include "enet_network.h"

#define TAILLE_CHAINE 256

char virtual_name[LOGICAL_NAME_MAX];
static sem_t ivy_semaphore;
char bus_id[BUS_ID_MAX];
char promethe_full_id[LOGICAL_NAME_MAX + BUS_ID_MAX + 2];
int pandora_is_activating = 0;
pthread_attr_t pandora_sched_attr;
struct sched_param param;
//extern int pandora_debug_mem_grp;
extern int pandora_debug_mem_grp_premier_lanc;
extern int pandora_debug_mem_grp_cloture;

type_commande liste_commande[] =
      {
        { "Hello", Hello_Callback, "provide the list of connected promethes" },
        { "help", help_Callback, "this help \n [xxx] is the optional name of a NN (script name or virtual name if the prt is defined and -d VirtualName option is used." },
        { "learn", run_online_Callback, "launch the online learning simulation" },
        { "use", run_use_only_Callback, "launch the use only simulation" },
        { "continue_learning", continue_learning_Callback, "continues the online learning simulation" },
        { "continue_using", continue_using_Callback, "continues the use only learning simulation" },
        { "save", save_NN_Callback, "save NN state" },
        { "save_link\\((.*)\\)", save_NN_link_Callback, "(name,xxx,yyy)' : save links from group xxx to yyy (file named name.lres)" },
        { "load_link\\((.*)\\)", load_NN_link_Callback, "(name,xxx,yyy)' : load links from group xxx to yyy (file named name.lres)" },
        { "step", step_Callback, "start step by step mode" },
        { "unstep", unstep_Callback, "leave step by step mode" },
        { "cancel", cancel_Callback, "cancel (put in stanby)" },
        { "autotest", autotest_Callback, "perform an autotest" },
        { "status", status_Callback, "provide name of the scripts, config and res used + No of the telnet ports to connect the promethe(s)" },
        { "quit", quit_Callback, "stop all the simulations (launch destroy_groups()) and close the different sockets)" },
        { "profile\\((.*)\\)", profile_group_callback, "(group name, n)': send profile statistics about the group (group_name) each n iterations (n=0 to remove the profiling of this group). The return is: stat(name:function,number of samples,min,max,mean,sigma2)" },
        { "distant_debug\\((.*)\\)", distant_debug_callback, "Send debug info." },
        { "pandora\\((.*),(.*),(.*)\\)", pandora_callback, "Send pandora command." },
        { "available_variable\\((.*),(.*)\\)", available_variable_callback, "Indicate the disponibility of the variable" },
        { "waiting_variable\\((.*),(.*)\\)", waiting_variable_callback, "Indicate that promethe is waiting for the variable" },
      /* {"enet_server\\((.*)\\)", enet_server_callback, "Indicate the port where  promethe has started an enet server"},*/
        { "\0", NULL, "\0" } };

/* Pas de macro car necessite plusieurs appels de fonctions a la suite ce qui pose des probleme avec if and else. De toute facon le tmeps est negligeable et permet de separer prom_bus de IvyBus (pas besoin d'inclure IvyBus.h, prom_bus.h suffit)*/
void prom_bus_send_message(const char *format, ...)
{
  char *buffer;
  va_list arguments;

  va_start(arguments, format);
  if (vasprintf(&buffer, format, arguments) < 0) EXIT_ON_ERROR("Fail to allow the buffer"); /* Alloue le buffer de la bonne taille */
  sem_wait(&ivy_semaphore);
  IvySendMsg("%s:%s", bus_id, buffer);
  sem_post(&ivy_semaphore);
  free(buffer);
}

void prom_bus_change_state(int state)
{
  prom_bus_send_message("status(%d)\n", state);
  current_state = state;
}

char *prom_bus_get_name()
{
  return virtual_name;
}

/* callback associated to "Hello" messages */
void Hello_Callback(IvyClientPtr app, void *data, int argc, char **argv)
{
  (void) app;
  (void) data;
  (void) argc;
  (void) argv;

  prom_bus_send_message("here(%d,%d,%d,%d)\n", current_state, io_server_kernel.port, io_server_console.port, io_server_debug.port);
}

void help_Callback(IvyClientPtr app, void *data, int argc, char **argv)
{
  int i;
  (void) app;
  (void) data;
  (void) argc;
  (void) argv;

  prom_bus_send_message("Name of promethe:%s responding to 'help':\n By default all the commands are sent to all promethes of the network. Otherwise, use: '<command> <name of promethe>'.\n", virtual_name);
  for (i = 0; liste_commande[i].name[0] != 0; i++)
  {
    prom_bus_send_message("'%s' : %s\n", liste_commande[i].name, liste_commande[i].help);
  }
}

void status_Callback(IvyClientPtr app, void *data, int argc, char **argv)
{
  (void) app;
  (void) data;
  (void) argc;
  (void) argv;

  prom_bus_send_message("%s status:\n", virtual_name);
  prom_bus_send_message("script = %s \n", fscript);
  prom_bus_send_message("res = %s \n", freseau);
  prom_bus_send_message("config = %s \n", fconfig);
  prom_bus_send_message("port client_kernel = %d  \n", io_server_kernel.port);
  prom_bus_send_message("port client_debug = %d  \n", io_server_debug.port);
  prom_bus_send_message("port client_console_user = %d  \n", io_server_console.port);
}

extern int autotest();
void autotest_Callback(IvyClientPtr app, void *data, int argc, char **argv)
{
  int res;
  (void) app;
  (void) data;
  (void) argc;
  (void) argv;

  res = autotest();
  if (res == 1) prom_bus_send_message("autotest %s = %d [OK] \n", virtual_name, res);
  else prom_bus_send_message("autotest %s = %d [PROBLEM!!!!] \n", virtual_name, res);
  printf("callback pour autotest active res=%d\n", res);
}

void run_online_Callback(IvyClientPtr app, void *data, int argc, char **argv)
{
  (void) app;
  (void) data;
  (void) argc;
  (void) argv;
  prom_bus_send_message("running %s\n", fscript);
#ifndef AVEUGLE
  gdk_threads_enter();
#endif
  set_learn_and_use_mode(NULL, NULL);
#ifndef AVEUGLE
  gdk_threads_leave();
#endif
  printf("callback pour run active \n");
}

void run_use_only_Callback(IvyClientPtr app, void *data, int argc, char **argv)
{
  (void) app;
  (void) data;
  (void) argc;
  (void) argv;

  prom_bus_send_message("running in use only mode %s\n", fscript);
#ifndef AVEUGLE
  gdk_threads_enter();
#endif
  set_use_only_mode(NULL, NULL);
#ifndef AVEUGLE
  gdk_threads_leave();
#endif
  printf("callback pour run in use only mode active \n");
}

void continue_learning_Callback(IvyClientPtr app, void *data, int argc, char **argv)
{
  (void) app;
  (void) data;
  (void) argc;
  (void) argv;

  prom_bus_send_message("running %s (continue learning)\n", fscript);
#ifndef AVEUGLE
  gdk_threads_enter();
#endif
  set_continue_learning_mode(NULL, NULL);
#ifndef AVEUGLE
  gdk_threads_leave();
#endif
  printf("callback pour continue_learning active \n");
}

void continue_using_Callback(IvyClientPtr app, void *data, int argc, char **argv)
{
  (void) app;
  (void) data;
  (void) argc;
  (void) argv;

  prom_bus_send_message("running %s (continue using)\n", fscript);
#ifndef AVEUGLE
  gdk_threads_enter();
#endif
  set_continue_only_mode(NULL, NULL);
#ifndef AVEUGLE
  gdk_threads_leave();
#endif
  printf("callback pour continue_using active \n");
}

void step_Callback(IvyClientPtr app, void *data, int argc, char **argv)
{
  (void) app;
  (void) data;
  (void) argc;
  (void) argv;

  prom_bus_send_message("%s entering step by step mode\n", fscript);
  step_by_step_pressed(NULL, NULL);
  printf("callback pour step by step active \n");
}

void unstep_Callback(IvyClientPtr app, void *data, int argc, char **argv)
{
  (void) app;
  (void) data;
  (void) argc;
  (void) argv;

  prom_bus_send_message("%s leaving step by step mode\n", fscript);
  continue_pressed(NULL, NULL);
  printf("callback pour unstep active \n");
}

void save_NN_Callback(IvyClientPtr app, void *data, int argc, char **argv)
{
  (void) app;
  (void) data;
  (void) argc;
  (void) argv;

  ecriture_reseaub(NULL, NULL);
  prom_bus_send_message("saved %s\n", fscript);
  printf("callback pour save_NN active \n");
}

void save_NN_link_Callback(IvyClientPtr app, void *data, int argc, char **argv)
{
  char link_file_name[MAXBUF], input_group_name[MAXBUF], output_group_name[MAXBUF];
  int success;
  (void) app;
  (void) data;

  if (argc != 1)
  {
    prom_bus_send_message("ERROR cannot read save_link arguments (argc= %i !=1) for script %s\n", argc, fscript);
    return;
  }
  prom_bus_send_message("save link for script %s with arg %s\n", fscript, argv[0]);
  printf("callback pour save_NN_link active \n");

  if (sscanf(argv[0], "%[^,],%[^,],%[^)]", link_file_name, input_group_name, output_group_name) != 3) PRINT_WARNING("Wrong format: %s", argv[0]);

  strcat(link_file_name, ".lres");
  printf("%s :%s ( %s , %s )\n", fscript, link_file_name, input_group_name, output_group_name);
  success = ecriture_voie_liaison(fscript, link_file_name, input_group_name, output_group_name);

  if (success == 1) prom_bus_send_message("save link for script %s with arg completed %s\n", fscript, argv[0]);
  else prom_bus_send_message("FAILED to save link for script %s with arg %s\n", fscript, argv[0]);
}

void load_NN_link_Callback(IvyClientPtr app, void *data, int argc, char **argv)
{
  char link_file_name[MAXBUF], input_group_name[MAXBUF], output_group_name[MAXBUF];
  int success;
  (void) app;
  (void) data;

  if (argc != 1)
  {
    prom_bus_send_message("ERROR cannot read save_link arguments (argc= %i !=1) for script %s\n", argc, fscript);
    return;
  }
  prom_bus_send_message("load link for script %s with arg %s\n", fscript, argv[0]);
  printf("callback pour load_NN_link active \n");

  if (sscanf(argv[0], "%[^,],%[^,],%[^)]", link_file_name, input_group_name, output_group_name) != 3) PRINT_WARNING("Wrong format: %s", argv[0]);

  strcat(link_file_name, ".lres");

  printf("%s :%s ( %s , %s )\n", fscript, link_file_name, input_group_name, output_group_name);
  success = lecture_voie_liaison(link_file_name, input_group_name, output_group_name);

  if (success == 1) prom_bus_send_message("SUCCESS load link for script %s with arg %s %s %s\n", fscript, link_file_name, input_group_name, output_group_name);
  else prom_bus_send_message("FAILED to load link for script %s with arg %s %s %s\n", fscript, link_file_name, input_group_name, output_group_name);
}

/* callback associated to "Bye" messages */
void quit_Callback(IvyClientPtr app, void *data, int argc, char **argv)
{
  (void) app;
  (void) data;
  (void) argc;
  (void) argv;

  IvyStop();
  kprints("Arret demande par ivy_bus ByeCallbac \n");
  exit(0); /* promethe_quit() execute avant exit grace a atexit */

}

/*** Begin profile ***/
void cancel_Callback(IvyClientPtr app, void *data, int argc, char **argv)
{
  (void) app;
  (void) data;
  (void) argc;
  (void) argv;

  cancel_pressed(NULL, NULL);
  prom_bus_send_message("cancelling %s\n", fscript);
  printf("callback pour cancel simul active \n");
}

void profile_group_callback(IvyClientPtr app, void *data, int argc, char **argv)
{
  char group_name[SIZE_NO_NAME];
  int index_of_group, number_of_iterations;
  (void) app;
  (void) data;
  (void) argc;

  sscanf(argv[0], "%[^,],%d", group_name, &number_of_iterations);
  if (strcmp("*", group_name) == 0)
  {
    for (index_of_group = 0; index_of_group < nbre_groupe; index_of_group++)
    {
      if (def_groupe[index_of_group].type == No_Fonction_Algo || def_groupe[index_of_group].type == No_Fonction_Algo_mvt)
      {
        if (number_of_iterations == 0) destroy_statistical_debug_group(index_of_group);
        else new_statistical_debug_group(index_of_group, number_of_iterations);
      }
    }
  }
  else
  {
    index_of_group = find_no_associated_to_symbolic_name(group_name, NULL);
    if (index_of_group < 0) prom_bus_send_message("Name of group: '%s' unknown.");
    else
    {
      if (number_of_iterations == 0) destroy_statistical_debug_group(index_of_group);
      else new_statistical_debug_group(index_of_group, number_of_iterations);
    }
  }
}

void distant_debug_callback(IvyClientPtr app, void *data, int argc, char **argv)
{
  (void) data;
  (void) argc;

  switch (atoi(argv[0]))
  {
  case DISTANT_DEBUG_START:
    dprints("Connect distant debug from: %s.", IvyGetApplicationName(app));
    init_enet_token_oscillo_kernel(IvyGetApplicationHost(app), 1234);
    break;
  }

}

/**
 *
 * Detecte la connection de Japet avec le protocole IVY et appele la fonction pandora_connect
 *
 * @see pandora_connect()
 */
void pandora_callback(IvyClientPtr app, void *data, int argc, char **argv)
{

  int gpe, activated, neuro;
  pthread_t thread_pando_connect;
  // char* ivyget=NULL;
  arg_pandora *arguments = NULL;
  int fifo_max_prio;
  (void) data;
  (void) argc;

  // if (pandora_activated==0 && pandora_is_activating==0)
  // {
  //        MANY_REALLOCATIONS(&arguments, 1);
  // arguments->compression=atoi(argv[1]);
  // arguments->port=atoi(argv[2]);
  // pandora_is_activating=1;
//          dprints("Connect distant pandora from: %s at address %s at port.\n", IvyGetApplicationName(app), IvyGetApplicationHost(app));
//          arguments->ivyget = MANY_ALLOCATIONS(256, char);
//          strncpy(arguments->ivyget, IvyGetApplicationHost(app), 256);
//
//
//          pthread_attr_init(&pandora_sched_attr);
//          pthread_attr_setinheritsched(&pandora_sched_attr, PTHREAD_EXPLICIT_SCHED);
//          pthread_attr_setschedpolicy(&pandora_sched_attr, SCHED_BATCH);
//          fifo_max_prio = sched_get_priority_max(SCHED_BATCH);
//         // fifo_min_prio = sched_get_priority_min(SCHED_BATCH);
//          param.sched_priority = fifo_max_prio;
//          pthread_attr_setschedparam(&pandora_sched_attr, &param);
//          pthread_create(&thread_pando_connect,&pandora_sched_attr,pandora_connect,(void*)arguments);
//          pthread_setschedparam(thread_pando_connect, SCHED_BATCH, &param);
  //pandora_connect(ivyget);
//  }else
//  {
  switch (atoi(argv[0]))
  {
  case PANDORA_START:
    if (pandora_is_activating != 1)
    {
      if (pandora_activated != 0) pandora_disconnect();

      if (arguments != NULL) free(arguments->ivyget);
      MANY_REALLOCATIONS(&arguments, 1);
      arguments->compression = atoi(argv[1]);
      arguments->port=atoi(argv[2]);
      pandora_is_activating = 1;
      dprints("Connect distant pandora from: %s at address %s.\n", IvyGetApplicationName(app), IvyGetApplicationHost(app));
      arguments->ivyget = MANY_ALLOCATIONS(256, char);
      strncpy(arguments->ivyget, IvyGetApplicationHost(app), 256);

      pthread_attr_init(&pandora_sched_attr);
      pthread_attr_setinheritsched(&pandora_sched_attr, PTHREAD_EXPLICIT_SCHED);
      pthread_attr_setschedpolicy(&pandora_sched_attr, SCHED_BATCH);
      fifo_max_prio = sched_get_priority_max(SCHED_BATCH);
      //fifo_min_prio = sched_get_priority_min(SCHED_BATCH);
      param.sched_priority = fifo_max_prio;
      pthread_attr_setschedparam(&pandora_sched_attr, &param);
      pthread_create(&thread_pando_connect, &pandora_sched_attr, pandora_connect, (void*) arguments);
      pthread_setschedparam(thread_pando_connect, SCHED_BATCH, &param);
    }
    else
    {
      PRINT_WARNING("Erreur probable d'ordre d'allocation dans ivy_bus.c vis à vis du demarrage de pandora\n");
    }
    // pandora_connect(ivyget);
    //pandora_connect( IvyGetApplicationHost(app), 1235));
    break;
  case PANDORA_STOP:
    if (pandora_activated == 1)
    {
      dprints("Disconnect distant pandora from: %s at address %s.\n", IvyGetApplicationName(app), IvyGetApplicationHost(app));
      pandora_disconnect();
    }
    break;
  case PANDORA_SEND_NEURONS_START:
    if (pandora_activated == 1)
    {
      gpe = atoi(argv[1]);
      def_groupe[gpe].debug |= FLAG_PANDORA_DEBUG_NEURONS;
      send_neurons_to_pandora(gpe);
    }
    break;
  case PANDORA_SEND_NEURONS_STOP:
    if (pandora_activated == 1)
    {
      gpe = atoi(argv[1]);
      def_groupe[gpe].debug &= ~FLAG_PANDORA_DEBUG_NEURONS;
    }
    break;
  case PANDORA_SEND_NEURONS_ONE:
    if (pandora_activated == 1)
    {
      gpe = atoi(argv[1]);
      activated = def_groupe[gpe].debug & FLAG_PANDORA_DEBUG_NEURONS;
      if (!activated) def_groupe[gpe].debug |= FLAG_PANDORA_DEBUG_NEURONS;
      send_neurons_to_pandora(gpe);
      if (!activated) def_groupe[gpe].debug &= ~FLAG_PANDORA_DEBUG_NEURONS;
    }
    break;
  case PANDORA_SEND_EXT_START:
    if (pandora_activated == 1)
    {
      gpe = atoi(argv[1]);
      def_groupe[gpe].debug |= FLAG_PANDORA_DEBUG_EXT;
      send_neurons_to_pandora(gpe);
    }
    break;
  case PANDORA_SEND_EXT_STOP:
    if (pandora_activated == 1)
    {
      gpe = atoi(argv[1]);
      def_groupe[gpe].debug &= ~FLAG_PANDORA_DEBUG_EXT;
    }
    break;
  case PANDORA_SEND_EXT_ONE:
    if (pandora_activated == 1)
    {
      gpe = atoi(argv[1]);
      activated = def_groupe[gpe].debug & FLAG_PANDORA_DEBUG_EXT;
      if (!activated) def_groupe[gpe].debug |= FLAG_PANDORA_DEBUG_EXT;
      send_neurons_to_pandora(gpe);
      if (!activated) def_groupe[gpe].debug &= ~FLAG_PANDORA_DEBUG_EXT;
    }
    break;
  case PANDORA_SEND_PHASES_INFO_START:
    if (pandora_activated == 1)
    {
      pandora_send_phases_info = 1;
    }
    break;
  case PANDORA_SEND_PHASES_INFO_STOP:
    if (pandora_activated == 1)
    {
      pandora_send_phases_info = 0;
    }
    break;
  case PANDORA_SEND_NEURO_LINKS_START:
    if (pandora_activated == 1)
    {
      neuro = atoi(argv[1]);
      gpe = neurone[neuro].groupe;

      if(info_neurone_pour_pando[neuro].have_to_send_link == 0)
      {
        if(para_pando_group[gpe].nbre_neuronne_to_send_links == 0)
        {
          def_groupe[gpe].debug |= FLAG_PANDORA_DEBUG_NEURO_LINKS; //flag general pour le groupe
          printf("activation flag general du groupe \n");
        }
        info_neurone_pour_pando[neuro].have_to_send_link = 1;
        para_pando_group[gpe].nbre_neuronne_to_send_links++;

        printf("reception d'une demande de liens pour le neurone %d, le nbre de neurone à afficher du groupe %d passe a %d \n",neuro,gpe,para_pando_group[gpe].nbre_neuronne_to_send_links);
      }


    }
    break;

  case PANDORA_SEND_NEURO_LINKS_STOP:
    if (pandora_activated == 1)
    {
      neuro = atoi(argv[1]);
      gpe = neurone[neuro].groupe;

      if(info_neurone_pour_pando[neuro].have_to_send_link==1)
      {
        if(para_pando_group[gpe].nbre_neuronne_to_send_links>0)
        {
          para_pando_group[gpe].nbre_neuronne_to_send_links--;
        }

        if (para_pando_group[gpe].nbre_neuronne_to_send_links==0)
        {
          def_groupe[gpe].debug &= ~FLAG_PANDORA_DEBUG_NEURO_LINKS;
          printf("DESactivation flag general du groupe \n");
        }

        info_neurone_pour_pando[neuro].have_to_send_link=0;
        printf("reception d'une demande dARRET DE liens pour le neurone %d, le nbre de neurone à afficher du groupe %d passe a %d \n",neuro,gpe,para_pando_group[gpe].nbre_neuronne_to_send_links);
      }
     // para_pando_group[gpe].neuronne_to_send_links = -1;

    }
    break;

  case PANDORA_SEND_OK_DEBUG_GRP_MEM:
    if (pandora_activated == 1)
    {
      pandora_debug_mem_grp_premier_lanc = 1;
    }
    break;

  case PANDORA_SEND_STOP_DEBUG_GRP_MEM:
    if (pandora_activated == 1)
    {
      pandora_debug_mem_grp_cloture = 1;
    }
    break;
  }
}

/*** End profile ***/

void bind_ivy_callbacks(type_commande liste_commande[], char *prom_name, void *user_data)
{

  int i;
  static char message[MAXBUF];

  for (i = 0; liste_commande[i].name[0] != (char) 0; i++)
  {
    sprintf(message, "^%s:%s$", bus_id, liste_commande[i].name);
    IvyBindMsg(liste_commande[i].function, user_data, message);

    sprintf(message, "^%s:%s %s$", bus_id, liste_commande[i].name, prom_name); /*utiliser IvySendDirectMsg ? */
    IvyBindMsg(liste_commande[i].function, user_data, message);
  }
}

void available_variable_callback(IvyClientPtr app, void *data, int argc, char **argv)
{
  (void) data;
  (void) argc;

  net_var_available(argv[0], IvyGetApplicationName(app), IvyGetApplicationHost(app), atoi(argv[1]));
}

void waiting_variable_callback(IvyClientPtr app, void *data, int argc, char **argv)
{
  (void) data;
  (void) argc;

  net_var_waiting(argv[0], IvyGetApplicationName(app), IvyGetApplicationHost(app), atoi(argv[1]));
}
/*
 void enet_server_callback(IvyClientPtr app, void *data, int argc, char **argv)
 {
 (void) data;
 (void) argc;

 enet_server_waiting(IvyGetApplicationName(app), IvyGetApplicationHost(app) , atoi(argv[0]));
 }
 */
void ApplicationCallback(IvyClientPtr app, void *user_data, IvyApplicationEvent event)
{

  switch (event)
  {
  case IvyApplicationConnected:
    Hello_Callback(app, user_data, 0, NULL);
    break;
  case IvyApplicationDisconnected:
    break;

  default:
    break;
  }
}

void main_loop_ivy(int argc, char *argv[])
{
  /* handling of -b option */
  const char* bus = 0;
  //char *promethe_full_id = NULL;
  char computer_name[LOGICAL_NAME_MAX];
  long int my_pid;
  /*  char c;*/
  int i;

#ifdef AVEUGLE
  int res;
  pthread_t un_thread;
#endif
  (void) argc;

  if (argv[No_arg_bus_ivy][0] != '\0')
  {
    bus = argv[No_arg_bus_ivy];
  }
  else
  {
    bus = getenv("IVYBUS");
  }

  if (argv[No_arg_VirtualNetwork][0] != (char) 0) strncpy(virtual_name, argv[No_arg_VirtualNetwork], LOGICAL_NAME_MAX); //on cherche un -n : si pas de -n on en invente un
  else
  {
    gethostname(computer_name, LOGICAL_NAME_MAX);
    my_pid = getpid();
    snprintf(virtual_name, LOGICAL_NAME_MAX, "%s_%ld", computer_name, my_pid);
  }

  if (argv[No_arg_StartMode][0] != '\0') sscanf(argv[No_arg_StartMode], "%d", &i);

  sem_init(&ivy_semaphore, 0, 1); /* semaphore necessaire pour faire des appels thread safes... Prom_bus_send_message n'est pas suffisant ??? */
  // promethe_full_id = MANY_ALLOCATIONS(strlen(bus_id)+strlen(virtual_name)+2, char);
  sprintf(promethe_full_id, "%s:%s", bus_id, virtual_name);
  IvyInit(promethe_full_id, NULL, ApplicationCallback, NULL, NULL, NULL);
  IvyStart(bus);
  bind_ivy_callbacks(liste_commande, virtual_name, NULL); /* Permet de passer un pointeur sur une structure perso (user_data) */

#ifdef AVEUGLE  /* et si pas gtk ...*/

  /* ivy main loop version AVEUGLE*/
  res = pthread_create(&un_thread, NULL, (void*(*)(void *)) IvyMainLoop, NULL);
  res = pthread_detach(un_thread);

  if (res != 0) kprints("Echec detach IvyMainLoop\n");
#endif

}

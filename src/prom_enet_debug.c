/*
Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
promethe@ensea.fr

Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...

See more details and updates in the file AUTHORS 

This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
and, more generally, to use and operate it in the same conditions as regards security. 
The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/
/*
 *      prom_enet_debug.c
 *
 *      philippe gaussier 2009/2010
 *
 */

/* #define DEBUG 1 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <enet/enet.h>

#include <sys/timeb.h>
#include <signal.h>
#include <math.h>
#include <bits/local_lim.h>

#include "public.h"
#include "net_message_debug_dist.h"
#include "prom_enet_debug.h"
#include "oscillo_kernel.h"

long secsFirstCall = 0;

int verbose_debug = 0;

type_connexion_udp network_debug_link;

inline float convert_time(struct timeval my_time)
{
	float f_time=0;
	f_time = (float) ((long int)my_time.tv_sec - secsFirstCall) + (float) my_time.tv_usec / (1000.0 * 1000.0);
	return (f_time);
}

int no_message = 0;

int enet_manager(int wait_time)
{
	char ip[HOST_NAME_MAX];
	ENetEvent event;
	ENetPacket *packet=NULL;
	size_t packetSize=0;
	void *data=NULL;
	int res = 1;

	res = enet_host_service(network_debug_link.host, &event, wait_time);
	if (res < 0)
	  {
	  PRINT_WARNING("Problem managing enet.");
	  return res;
	  }

	switch (event.type)
	{
	case ENET_EVENT_TYPE_CONNECT:
		enet_address_get_host_ip(&event.peer->address, ip, HOST_NAME_MAX);
		kprints("A new client connected from ip %s:%i.\n", ip, event.peer->address.port);
		event.peer->data = NULL;

		sem_wait(&(network_debug_link.lock));
		packetSize = sizeof(char) * LOGICAL_NAME_MAX + sizeof(type_com_groupe) * nbre_groupe;
		data = malloc(packetSize);
		if (data==NULL)
		{
		  PRINT_WARNING("allocation failed");
		  return -1;
		}
		memcpy(data, virtual_name, LOGICAL_NAME_MAX);
		init_com_def_groupe((type_com_groupe*) &((char*) data)[LOGICAL_NAME_MAX]); /*To be clenaned but the idea is to put the name, then the data */
		/*	init_com_def_groupe(data);*/
		packet = enet_packet_create(data, packetSize, ENET_PACKET_FLAG_RELIABLE & ENET_PACKET_FLAG_NO_ALLOCATE);
		if (packet == NULL)
		{
		  PRINT_WARNING("The packet has not been created.");
		  free(data);
		  return -1;
		}
		else if (enet_peer_send(network_debug_link.peer, ENET_DEF_GROUP_CHANNEL, packet) != 0)
		{
		  PRINT_WARNING("The packet has not been sent.");
		  free(data);
		  return -1;
		}
		sem_post(&(network_debug_link.lock)); /* on relibere le jeton pour les autres */
		if (enet_manager(0) < 0)
		{
		  PRINT_WARNING("Probleme sending description info");
		  free(data);
		  return -1;
		}

		oscillo_dist_activated = 1;
		free(data);
		break;

	case ENET_EVENT_TYPE_RECEIVE:
		if (event.channelID == ENET_COMMAND_CHANNEL)
		{
			switch (*(int*) event.peer->data)
			{
			case ENET_COMMAND_STOP_OSCILLO:
				oscillo_dist_activated = 0;
				break;
			case ENET_COMMAND_START_OSCILLO:
				oscillo_dist_activated = 1;
				break;
			}
		}
		break;

	case ENET_EVENT_TYPE_DISCONNECT:
		kprints("%s disconected.\n", (char*) event.peer->data);
		/* Reset the peer's client information. */
		event.peer->data = NULL;
		break;

	case ENET_EVENT_TYPE_NONE:
		break;
	}
	return res;
}
/*************************************************************/
/* Attention: peut etre appeler depuis une section critique pour init */

/* ou poour l'envoie des tags de l'oscillo kernel                     */
void send_debug_data(void *data, size_t packetSize, int channel_number)
{
	ENetPacket *packet;

	sem_wait(&network_debug_link.lock); /* attend que le message precedent ait ete envoye */
	packet = enet_packet_create(data, packetSize, ENET_PACKET_FLAG_RELIABLE & ENET_PACKET_FLAG_NO_ALLOCATE);
	if (packet == NULL)
	  {
	  PRINT_WARNING("Fail to create packet on channel %i", channel_number);
	  return;
	  }
	enet_peer_send(network_debug_link.peer, channel_number, packet); /* queue the packet to be sent to the peer over channel number */
	if (enet_manager(0) < 0)
	  {
	  PRINT_WARNING("Error sending enet");
	  return;
	  }
	sem_post(&network_debug_link.lock); /* on relibere le jeton pour les autres */
}

/* init network for udp connexion using enet : used for distant oscillo_kernel */
/* ip_adr= 127.0.0.1 par ex ou nom_adr=localhost... */

void init_enet_token_oscillo_kernel(char *ip_adr, int port)
{

	if (sem_init(&network_debug_link.lock, 0, 1) != 0) EXIT_ON_SYSTEM_ERROR("Fail to init semaphore.");
	/* attend que le message precedent ait ete envoye */

	/* network initialization: */
	if (enet_initialize() != 0) EXIT_ON_ERROR("An error occurred while initializing ENet.\nAn error occurred while initializing ENet.\n");

	atexit(enet_deinitialize); /* PG: A verifier si pas de conflit au niveau global de promethe */
	enet_time_set(0);

	network_debug_link.address.port = port; /* bind connection to this port */
	network_debug_link.packet = NULL;
	network_debug_link.waitTime = event_waitTime; /* number of milliseconds to wait for network events, corresponding to time between frames  */
	network_debug_link.sendCounter = 0;

	network_debug_link.host = enet_host_create(NULL, 1, ENET_NUMBER_OF_CHANNELS, ENET_UNLIMITED_BANDWITH, ENET_UNLIMITED_BANDWITH);

	if (!network_debug_link.host) EXIT_ON_ERROR("An error occurred while trying to create an ENet client host.\n");

	if (enet_address_set_host(&(network_debug_link.address), ip_adr) != 0) EXIT_ON_ERROR("Error with address %s", ip_adr);
	network_debug_link.address.port = port;
	network_debug_link.peer = enet_host_connect(network_debug_link.host, &network_debug_link.address, ENET_NUMBER_OF_CHANNELS, 0);

	if (enet_manager(5000)<0)
	  {
	  PRINT_WARNING("problem with enet_manager");
	  return;
	  }

}

void quit_debug_client(type_connexion_udp *connexion)
{
	int event_return;

	if (connexion->data != NULL)
	{
		free(connexion->data);
		connexion->data = NULL;
	}

	if (connexion->peer)
	{
		enet_peer_disconnect(connexion->peer, 0);
		event_return = enet_host_service(connexion->host, &connexion->event, connexion->waitTime);
		if (event_return < 0) printf("error in event_host_service\n");
		enet_peer_reset(connexion->peer);
	}

	if (connexion->host != NULL)
		{
			enet_host_destroy(connexion->host);
			enet_deinitialize();
		}
}

void quit_enet_token_oscillo_kernel()
{
	quit_debug_client(&network_debug_link);
}

void send_token_oscillo_kernel(int gpe, int phase, long temps)
{
	type_nn_message data_network; /*PG: pas malin la structure a ete aussi alloue par malloc dans connexion */

	data_network.type_trame = GROUP_DEBUG;
	no_message++;
	data_network.val_nn_message_id = NN_message_magic_card;
	data_network.no_message = no_message;
	data_network.time_stamp = temps;
	data_network.gpe = gpe;
	data_network.type_message = phase;

	send_debug_data(&data_network, sizeof(type_nn_message), ENET_GROUP_EVENT_CHANNEL);
}

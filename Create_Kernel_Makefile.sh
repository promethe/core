#!/bin/bash
################################################################################
# Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
#promethe@ensea.fr
#
# Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
# C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
# M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...
#
# See more details and updates in the file AUTHORS 
#
# This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
# This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
# You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info". 
# As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
# users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
# In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
# that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
# Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
# and, more generally, to use and operate it in the same conditions as regards security. 
# The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
################################################################################
####################################################
#script permettant de generer un Makefile pour compiler la libkernel
#v2.0 Maillard M. & Baccon J.C.
#v3.0 Hirel J.
###################################################
 
####################################################
#definition de $CFLAGS $FLAGS_OPTIM $FLAGS_DEBUG
####################################################
source ../scripts/COMPILE_FLAG

####################################################
#Definition des chemins d'acces, options de compile etc...
####################################################

# Nom du programme
PROG_NAME="libkernel"

# Initialisation des libs, includes et flags
SOURCES=(shared/a_mac_semaphore.c shared/iostream_over_network.c shared/script.c shared/lenautil.c shared/outils.c shared/lec_config.c shared/gestion_threads.c shared/gestion_ES.c shared/gestion_diagnostic.c shared/promethe.c shared/rttoken.c shared/gestion_sequ.c shared/ivy_bus.c prom_jpeg_compressor.c prom_jpeg_uncompressor.c)

LIBS="$GTKLIB"
INCLUDES="$GTKINCLUDES -I$SIMULATOR_PATH -I$SIMULATOR_PATH/shared/include/ -I$PWD/include -I$SIMULATOR_PATH/shared/include/Ivy -I. -I$HOME/.local/include -I$PWD/include/network"

#CFLAGS definis dans le COMPILE_FLAGS
#option sequenceur autonome
#Decommenter la ligne pour utiliser le sequenceur autonome
#CFLAGS="$CFLAGS -DSEQ_AUTONOME"

ARGS=(`getopt --options h --long help,enable-blind,enable-threads,enable-debug,disable-enet -- "$@"`)
if [ $? -ne 0 ]
then 
	echo "Use $0 -h to get some help." > /dev/stderr
	exit 2;
fi

ENET=1
DEBUG=0
BLIND=0
THREADS=0

for i in ${ARGS[@]}
do
	case $i in
		-h|--help) cat README.$(basename $0); exit 0;;
		--enable-blind) BLIND=1;; 
		--enable-threads)  THREADS=1;;
    	--enable-debug) DEBUG=1;; 
	esac
done




# Gestion des parametres passes au Create_Makefile

SOURCES=(${SOURCES[@]} prom_enet_debug.c prom_send_to_pandora.c enet_network.c);
INCLUDES="$INCLUDES  -I$SIMULATOR_PATH/enet/include";


if (( $BLIND ))
then echo "compile enable-blind..."
    CFLAGS="$CFLAGS -DAVEUGLE"
	PROG_NAME="${PROG_NAME}_blind"
    INCLUDES="$INCLUDES -I$PWD/include/blind"
else echo "compile gui ..."
    SOURCES=(${SOURCES[@]} windows/callbacks.c windows/interface.c windows/graphic_config.c windows/gestion_debug.c)
    INCLUDES="$INCLUDES -I$PWD/include/windows"
fi


echo "Oscillo_kernel: enabled"
CFLAGS="$CFLAGS -DOSCILLO_KERNEL"
SOURCES=(${SOURCES[@]} oscillo_kernel.c)



if (( $THREADS ))
then
	CFLAGS="$CFLAGS -DUSE_THREADS -DUSE_THREADS_2"	
	PROG_NAME="${PROG_NAME}_threads"
fi 

if (( $DEBUG ))
then echo "compile enable-debug..."
    CFLAGS="$CFLAGS $FLAGS_DEBUG"
    PROG_NAME="${PROG_NAME}_debug"    
else echo "compile disable-debug..."
    CFLAGS="$CFLAGS $FLAGS_OPTIM"
fi


#Version finale des libs, includes et flags
SOURCES=${SOURCES[@]/#/src/} #On ajoute src/ avant chaque fichier source
SOURCES="$SOURCES  src/network/*.c"
FINALINCLUDES="$INCLUDES"
FINALLIBS="$LIBS -lrt" #lrt pour clock_gettime etc
FINALCFLAGS="$CFLAGS -DUSE_IVY -DUSE_ENET=$ENET"

#Les repertoires de destination des fichiers compiles
OBJDIR="$OBJPATH/$PROG_NAME"
LIBDIR="$SIMULATOR_PATH/lib/$SYSTEM/kernel"

#Gestion des fichiers a compiler
OBJECTS=""

####################################################
#Creation du Makefile
####################################################

MAKEFILE="Makefile.$PROG_NAME"

#ecrasement du Makefile precedent

#regle par defaut
echo "default: $PROG_NAME" > $MAKEFILE
echo "" >> $MAKEFILE

echo -e "include ../scripts/variables.mk\n" >> $MAKEFILE

echo "$OBJDIR:">> $MAKEFILE
echo -e "\tmkdir -p \$@" >> $MAKEFILE


# creer les regles
#pour chaque  .o
for i in $SOURCES
do
  echo "processing '$i'"
  FICHIER=`basename $i .c`
  CHEMIN=`echo $i | sed -e s@$FICHIER.c@@`
  echo "$OBJDIR/$FICHIER.o:$i | $OBJDIR " >> $MAKEFILE
  echo -e "\t@echo \"[processing $i...]\"">>$MAKEFILE
echo -e "\t@(cd $CHEMIN; $CC $FINALCFLAGS $FINALINCLUDES -c -o $OBJDIR/$FICHIER.o $FICHIER.c -DSVN_REVISION='\$(shell svnversion -n .)' )" >> $MAKEFILE
  echo "" >> $MAKEFILE
  OBJECTS="$OBJECTS $OBJDIR/$FICHIER.o"
done

#pour l'edition de liens et le lien sur le binaire
echo "$PROG_NAME: $OBJECTS" >> $MAKEFILE
echo -e "\t@echo \"[making Promethe kernel library...]\"" >> $MAKEFILE
echo -e "\t@mkdir -p $LIBDIR" >> $MAKEFILE
echo -e "\t@$AR -rcv $LIBDIR/$PROG_NAME.a $OBJECTS" >> $MAKEFILE
echo "" >> $MAKEFILE


#regles additionnelles
echo "clean:" >> $MAKEFILE
echo -e "\trm -f  $OBJDIR/*.o" >> $MAKEFILE
echo "" >> $MAKEFILE

echo "reset:clean" >> $MAKEFILE
echo -e "\trm -f  $OBJDIR/*.o $LIBDIR/$PROG_NAME.a" >> $MAKEFILE
echo "" >> $MAKEFILE
